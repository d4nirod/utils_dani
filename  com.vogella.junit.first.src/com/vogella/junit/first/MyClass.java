package com.vogella.junit.first;

public class MyClass {
	public int multiply(int x, int y) {
		// the following is just an example
		if (x > 999) {
			throw new IllegalArgumentException("X should be less than 1000");
		}
		return x * y;
	}

	public float divide(int x, int y){
		if(y == 0) throw new IllegalArgumentException("Divisor cannot be equal to 0");
		return (float)x / y;
	}
}
