package common;


public class CloneTest {

	
	public class MyClass {
		String a = "originalA";
		String b = "original B";
	}
	
	MyClass myclazz = null;
	public CloneTest(){
		myclazz = new MyClass();
	}
	
	public static void main(String[] args) {
		
		CloneTest mytest = new CloneTest();
		
		System.out.println("Valores originales: "+mytest.myclazz.a + ", "+mytest.myclazz.b);
		
		try {
			CloneTest myclone = (CloneTest) mytest.clone();
		} catch (CloneNotSupportedException e) {

			e.printStackTrace();
		}

	}

}
