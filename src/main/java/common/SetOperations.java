package common;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class SetOperations {

/*
 *  s1.containsAll(s2) * returns true if s2 is a subset of s1. (s2 is a subset of s1 if set s1 contains all of the
 *  					 elements in s2.)
 *  s1.addAll(s2) 	   * transforms s1 into the union of s1 and s2. (The union of two sets is the set containing all
 *  					 of the elements contained in either set.)
 *  s1.retainAll(s2)   * transforms s1 into the intersection of s1 and s2. (The intersection of two sets is the
 *  					 set containing only the elements common to both sets.)
 *  s1.removeAll(s2)   * transforms s1 into the (asymmetric) set difference of s1 and s2. (For example, the
 *  					 set difference of s1 minus s2 is the set containing all of the elements found in s1
 * 						 but not in s2.)
 */


	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		Set old = new HashSet<Integer>(Arrays.asList(new Integer[]{1,2,3,4,5,6}));
		Set nnew = new HashSet<Integer>(Arrays.asList(new Integer[]{2,4,6,8,10}));
		//all the elements in nnew but not in old
		Set toAdd = new HashSet(nnew);
		toAdd.removeAll(old);
		System.out.print("toAdd: { ");
		printSet(toAdd);
		System.out.println("}");

		//all the elements in old but not in nnew
		Set toDel = new HashSet(old);
		toDel.removeAll(nnew);
		System.out.print("toDel: { ");
		printSet(toDel);
		System.out.println("}");

	}

	public static void printSet(Set s){
		for(Object n:s)
			System.out.print((Integer)n + " ");
	}

}
