package common;


import java.io.Console;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

public class YetAnotherInputTask extends Task{
	private String outputproperty;

	private String message;

	private Boolean password;

	public void setPassword(String password) {
		this.password = Boolean.parseBoolean(password);
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setOutputproperty(String outputproperty) {
		this.outputproperty = outputproperty;
	}

	@Override
	public void execute() throws BuildException {
		Console cons = System.console();
		String text = null;
		if (cons != null) {
			if (password.booleanValue()) {
				char[] passwd = cons.readPassword("[yainput] %s", message);
				text = new String(passwd);
			} else {
				text = cons.readLine("[yainput] %s", message);
			}
			getProject().setProperty(outputproperty, text);
		}
	}

}
