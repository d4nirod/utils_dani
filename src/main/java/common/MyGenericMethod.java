package common;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collection;


public class MyGenericMethod {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Integer[] ai = new Integer[] {1,2,3,4,5,6};
		List li = Arrays.asList(new Integer[]{7,8,9,0});
		List merged = (List)merge(ai, li);

		String[] as = new String[] {"a", "b", "c"};
		List ls = Arrays.asList(new String[]{"d", "e", "f"});
		List merged2 = (List)merge(as, ls);

		System.out.print("merged: { ");
		printCol(merged);
		System.out.println("}");
		System.out.print("merged2: { ");
		printCol(merged2);
		System.out.println("}");

	}

	static void printCol(Collection c){
		for(Object o : c)
			System.out.print(o+ " ");
	}

	static <T> Collection<T> merge(T[]a, Collection<T> b){

		List<T> res = new ArrayList<T>();

		for( T o : a)
			res.add(o);

		res.addAll(b);

		return res;
	}

}
