package common;


import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Vector;

public class LinkedHashMapTest {

	public static void main(String[] args) {
		LinkedHashMap<String, String> lHashMap = new LinkedHashMap<String, String>();
		lHashMap.put("1", "One");
		lHashMap.put("2", "Two");
		lHashMap.put("3", "Three");
						
		for(String str : lHashMap.keySet()){ 
			System.out.println(lHashMap.get(str));
		} 
		System.out.println("#####################");
		Collection<String> values = lHashMap.values();
		for(String str : values){ 
			System.out.println(str);
		}				
	}
}