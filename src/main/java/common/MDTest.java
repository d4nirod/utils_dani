package common;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.binary.Base64;


public class MDTest {

	/**
	 * @param args
	 * @throws NoSuchAlgorithmException
	 */
	public static String doHash(String msg) throws NoSuchAlgorithmException{
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.reset();
		md.update(msg.getBytes());
		Base64 codec = new Base64();
		return new String(codec.encode(md.digest()));
	}




	public static void main(String[] args) {

		String msg = "El perro de parra no tiene rabo porque ram*n ramirez se lo ha cortado";
		String hash = null;
		try {
			hash = MDTest.doHash(msg);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		System.out.println("Mensaje: "+msg);
		System.out.println("Hash: "+ hash);

	}

}
