package common;

import java.util.Currency;
import java.util.Locale;

public class CurrencySymbol {
    public static void main(String[] args) {
        Currency currency = Currency.getInstance(Locale.JAPAN);
        System.out.println("Currency.getSymbol() = " + currency.getSymbol());

        currency = Currency.getInstance(Locale.UK);
        System.out.println("Currency.getSymbol() = " + currency.getSymbol());

        currency = Currency.getInstance(Locale.US);
        System.out.println("Currency.getSymbol() = " + currency.getSymbol());

        currency = Currency.getInstance(new Locale("in", "ID"));
        System.out.println("Currency.getSymbol() = " + currency.getSymbol());
        
        System.out.println("==========================================");
        currency = Currency.getInstance("EUR");
        System.out.println("Currency.getSymbol() = " + currency.getSymbol());
        currency = Currency.getInstance("USD");
        System.out.println("Currency.getSymbol() = " + currency.getSymbol());
        
    }
}
