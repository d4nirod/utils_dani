package common;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;


public class FileUtils {
	
	public static byte[] readBytes(File file) throws IOException {

	    if ( file.length() > Integer.MAX_VALUE ) {
	        throw new IOException("File too large to read in array. Size > Integer.MAX_VALUE. "+file);
	    }

	    byte []buffer = new byte[(int) file.length()];
	    InputStream ios = null;
	    try {
	        ios = new FileInputStream(file);
	        if ( ios.read(buffer) == -1 ) throw new IOException("EOF reached while trying to read the whole file");
	                
	    } finally { 
	        try { if(ios != null) ios.close(); } catch (Exception e) {}
	    }

	    return buffer;
	}
	
	public static void writeStringAsBytes(String str, File file, String charsetName) throws IOException{
		OutputStreamWriter out = null; 
		try {
			 out = new OutputStreamWriter(new FileOutputStream(file), charsetName);
			 out.write(str);
			
		}finally { 
	        try { if(out != null)out.close(); } catch (IOException e) {}
		}
	}
	
	public static void writeBytesAsString(byte[] bytes, File file, String charsetName) throws IOException{
		FileUtils.writeString(new String(bytes, charsetName), file);				
	}
	
	public static void writeString(String s, File file) throws IOException{
		FileWriter out = null; 
		try {
			out = new FileWriter(file);
			out.write(s);
			
		} finally { 
	        try { if(out != null)out.close(); } catch (IOException e) {}
	    }
	}
}
