package common;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ParseMod100Renta2010 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length<1){
		      System.err.println("usage: ParseMod100Renta2010 filename");
		      System.exit(-1);
		}  
		byte[] srcbytes = new byte[0];
		System.out.println("Reading file: "+args[0]);
		try {
			srcbytes = FileUtils.readBytes(new File(args[0]));						
		} catch (Exception e) {			
			e.printStackTrace();
			System.exit(-1);
		}
		System.out.println("Read "+srcbytes.length+" bytes.");
		String srcstr = "";
		try {
			srcstr = new String(srcbytes, "ISO-8859-15");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String REGEX = "(<T10001>.*?)</T100020100A0000>";
		System.out.println("Extracting using regex "+REGEX+"\n**************************");
		Pattern MY_PATTERN = Pattern.compile(REGEX, Pattern.DOTALL);
		Matcher m = MY_PATTERN.matcher(srcstr);
		String substr = "";
		while (m.find()) {
			substr = m.group(1);		    
		}
		System.out.println(substr);
		System.out.println("-------------------------");
		System.out.println(substr.replaceAll("\\r\\n", ""));

		
	}
}
