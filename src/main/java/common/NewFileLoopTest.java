package common;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.apache.commons.lang3.StringUtils;

public class NewFileLoopTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		StringBuffer strb = new StringBuffer("c:/tmp/CAdES/declaraciones-web/tmp/12763342K_2011_100_004916_ok");
		nextAvailableFilename2(strb);
		OutputStreamWriter out = null;
		try {
			out = new OutputStreamWriter(new FileOutputStream(strb.toString()));
			out.write("testin 1, 2, 3");
			out.close();
			
		} catch (IOException e) { 
			e.printStackTrace();			
		}finally{ try {if(out!=null)out.close();}catch (IOException e1){}}

	}
	public static StringBuffer nextAvailableFilename2(StringBuffer strb){
		int i = 0;
		
		StringBuffer aux = new StringBuffer(strb.toString()).append('_').append(
				StringUtils.leftPad(Integer.toString(i), 2, '0')).append(".html");
	    
	    File f = new File(aux.toString());
	    while (f.exists()) {
	        i++;
	        aux = new StringBuffer(strb.toString()).append('_').append(
					StringUtils.leftPad(Integer.toString(i), 2, '0')).append(".html");
	        f = new File(aux.toString());
	    }
//	    f.createNewFile();
		return aux;
	}
	public static StringBuffer nextAvailableFilename(StringBuffer strb){
		int i = 0;
		String aux = strb.toString();
		for (; i < 100; i++) {
			String f = new StringBuffer(aux).append('_').append(
						StringUtils.leftPad(String.valueOf(i), 2, '0')).append(".html").toString();
			File file = new File(f);
			if (!file.exists())
				break;
		}
		//NewFileLoopTest.fill(String.valueOf(i), '0', 2, true)
		strb.append('_').append(StringUtils.leftPad(String.valueOf(i), 2, '0')).append(".html");
		return strb;
	}
	
	  /**
	   * Rellena un string src con un caracter f hasta llegar a longitud len
	   * por la izda o la dcha left
	   */
	  public static String fill(String src,char f,int len,boolean left) {
	    StringBuffer strb = new StringBuffer();
	    if (left) { //rellenar por la izda
	      int ls = (src==null)?0:src.length();
	      for (int i=0;i<len-ls;i++) strb.append(f);
	      strb.append((ls>len)?src.substring(0,len):src);
	    } else {
	      int ls = (src==null)?0:src.length();
	      strb.append((ls>len)?src.substring(0,len):src);
	      for (int i=0;i<len-ls;i++) strb.append(f);
	    }
	    return strb.toString();
	  }

}
