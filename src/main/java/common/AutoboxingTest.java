package common;

public class AutoboxingTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Integer i = null; 
		int j = i;  //NullPointerException (I presume autoboxing is trying to do i.intValue() 
		System.out.println(j); 
		
	}

}
