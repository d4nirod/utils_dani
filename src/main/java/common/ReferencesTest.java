package common;

public class ReferencesTest {

	public static void main(String[] args) {
		Object a = new Object();
		Object b = a;
		System.out.println("a=" + a + ", b=" + b + ", a==b: " + (a == b));
		a = new Object();
		System.out.println("a=" + a + ", b=" + b + ", a==b: " + (a == b));
		
	}

}
