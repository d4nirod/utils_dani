package common;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.Vector;


public class MultiThreadTest implements Runnable {

	public void run() {
		
		for(int i=0; i< REPEATS; i++){
			int theVal = this.getVal();
			if(!uniqueGenerated.add(new Integer(theVal))){
				System.out.println("\t************Duplicado: "+theVal);
				synchronized(LOCK){
					dupeCount++;
				}
			}
//			Thread.currentThread();
			Thread.yield();
		}
	}
	
	
	
	public synchronized int getVal(){
		int currentVal = val;
		Random rnd = new Random();
//		try {
//			Thread.currentThread().sleep(rnd.nextInt(10)*10);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
		val = val+1;
		System.out.println(Thread.currentThread().getName()+ ": " + System.currentTimeMillis() + ": " +currentVal);
		return  currentVal;
	}
	
	private static int val = 0;

	/**
	 * @param args
	 */
	
	private final static int MAX_THREADS = 100;
	private final static int REPEATS = 10;
	
	private static Set<Integer> uniqueGenerated = Collections.synchronizedSet(new HashSet<Integer>(MAX_THREADS*REPEATS));
	private static int dupeCount = 0;
	private final static  Object LOCK = new Object();
	
	public static void main(String[] args) {
						
		Vector<Thread> threads = new Vector<Thread>(MAX_THREADS);
		for(int i=0; i<MAX_THREADS; i++){
			threads.add(new Thread(new MultiThreadTest()));
			threads.get(i).setName("Thread #"+i);
		}
//		try {
//			Thread.currentThread().sleep(1);
//		} catch (InterruptedException e1) {
//			e1.printStackTrace();
//		}
		
		//mezclamos para cambiar aleatoriamente el orden
		Collections.shuffle(threads);
		Random rnd = new Random();
		for(Thread t : threads){
			//esperamos un corto tiempo aleatorio para empezar
			try {
//				Thread.currentThread();
				Thread.sleep(rnd.nextInt(20)*10);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			t.start();
		}
		//mezclamos otra vez
		Collections.shuffle(threads);
		//esperamos a que todos los threads acaben
		for(Thread t : threads){			
			if(t.isAlive()){
				try {
					t.join();
				} catch (InterruptedException e) {					
					System.out.println(t.getName() + ": "+e.getMessage());					
				}
			}
		}
		
		System.out.println("Numero duplicados: "+dupeCount);

	}

}
