package common;

public class FinallyReturnTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
     System.out.println(test());
	}

	private static boolean test() {
		try {
		   return true;
		   
		 } finally {   	 
		   return false;
		   
		 }
	}

}
