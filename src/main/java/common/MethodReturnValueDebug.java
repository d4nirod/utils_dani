package common;

import org.apache.commons.lang3.RandomStringUtils;

public class MethodReturnValueDebug {

	public static void main(String[] args) {
		System.out.println(MethodReturnValueDebug.randomString());
	}
	
	public static String randomString() {
		return RandomStringUtils.randomAscii(10);
	}

}
