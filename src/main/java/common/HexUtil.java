package common;

public class HexUtil {
	/**
	 * Fast convert a byte array to a hex string with possible leading zero.
	 *
	 * @param b
	 *            array of bytes to convert to string
	 * @return hex representation, two chars per byte.
	 */
	public static String toHexString(byte[] b) {
		StringBuffer sb = new StringBuffer(b.length * 2);
		for (int i = 0; i < b.length; i++) {
			// look up high nibble char
			sb.append(hexChar[(b[i] & 0xf0) >>> 4]);

			// look up low nibble char
			sb.append(hexChar[b[i] & 0x0f]);
		}
		return sb.toString();
	}

	/**
	 * table to convert a nibble to a hex char.
	 */
	public final static char[] hexChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

//	/**
//	 * Fast convert a byte array to a hex string with possible leading zero.
//	 *
//	 * @param b
//	 *            array of bytes to convert to string
//	 * @return hex representation, two chars per byte.
//	 */
//	public byte[] toByteArray(String hexString) {
//		String hexVal = "0123456789ABCDEF";
//		byte[] out = new byte[hexString.length() / 2];
//
//		int n = hexString.length();
//
//		for (int i = 0; i < n; i += 2) {
//			// make a bit representation in an int of the hex value
//			int hn = hexVal.indexOf(hexString.charAt(i));
//			int ln = hexVal.indexOf(hexString.charAt(i + 1));
//
//			// now just shift the high order nibble and add them together
//			out[i / 2] = (byte) ((hn << 4) | ln);
//		}
//
//		return out;
//	}

	/**
	 * precomputed translate table for chars 0..'f'
	 */
	private static byte[] correspondingNibble = new byte['f' + 1];

	// -------------------------- PUBLIC STATIC METHODS --------------------------

	/**
	 * Convert a hex string to an unsigned byte array. Permits upper or lower case hex.
	 *
	 * @param s
	 *            String must have even number of characters. and be formed only of digits 0-9 A-F or a-f. No spaces, minus or plus signs.
	 *
	 * @return corresponding unsigned byte array.
	 */
	public static byte[] fromHexString(String s) {
		int stringLength = s.length();
		if ((stringLength & 0x1) != 0) {
			throw new IllegalArgumentException("fromHexString requires an even number of hex characters");
		}
		byte[] bytes = new byte[stringLength / 2];

		for (int i = 0, j = 0; i < stringLength; i += 2, j++) {
			int high = charToNibble(s.charAt(i));
			int low = charToNibble(s.charAt(i + 1));
			// You can store either unsigned 0..255 or signed -128..127 bytes in a byte type.
			bytes[j] = (byte) ((high << 4) | low);
		}
		return bytes;
	}

	// -------------------------- STATIC METHODS --------------------------

	static {
		// only 0..9 A..F a..f have meaning. rest are errors.
		for (int i = 0; i <= 'f'; i++) {
			correspondingNibble[i] = -1;
		}
		for (int i = '0'; i <= '9'; i++) {
			correspondingNibble[i] = (byte) (i - '0');
		}
		for (int i = 'A'; i <= 'F'; i++) {
			correspondingNibble[i] = (byte) (i - 'A' + 10);
		}
		for (int i = 'a'; i <= 'f'; i++) {
			correspondingNibble[i] = (byte) (i - 'a' + 10);
		}
	}

	/**
	 * convert a single char to corresponding nibble using a precalculated array.
	 *
	 * @param c
	 *            char to convert. must be 0-9 a-f A-F, no spaces, plus or minus signs.
	 *
	 * @return corresponding integer 0..15
	 * @throws IllegalArgumentException
	 *             on invalid c.
	 */
	private static int charToNibble(char c) {
		if (c > 'f') {
			throw new IllegalArgumentException("Invalid hex character: " + c);
		}
		int nibble = correspondingNibble[c];
		if (nibble < 0) {
			throw new IllegalArgumentException("Invalid hex character: " + c);
		}
		return nibble;
	}

	/**
	 * code not used, for explanation only. convert a single char to corresponding nibble. Slow version, easier to understand.
	 *
	 * @param c
	 *            char to convert. must be 0-9 a-f A-F, no spaces, plus or minus signs.
	 *
	 * @return corresponding integer
	 */
	private static int slowCharToNibble(char c) {
		if ('0' <= c && c <= '9') {
			return c - '0';
		} else if ('a' <= c && c <= 'f') {
			return c - 'a' + 0xa;
		} else if ('A' <= c && c <= 'F') {
			return c - 'A' + 0xa;
		} else {
			throw new IllegalArgumentException("Invalid hex character: " + c);
		}
	}

}
