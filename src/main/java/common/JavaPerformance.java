package common;

//JavaPerformance.java
import java.util.Random;

public class JavaPerformance {

	private final static long iterations = 1000000000;

	private static Base createClassRandomly(final int numberOfClasses) {
		if (numberOfClasses < 2 || numberOfClasses > 10) {
			throw new Error("Only 10 different classes (A, ..., J) are implemented. ");
		}
		JavaPerformance o = new JavaPerformance();
		Random r = new Random();
		int randomPositiveNumber = Math.abs(r.nextInt(numberOfClasses));

		switch (randomPositiveNumber) {
		case 0:
			return o.new A();
		case 1:
			return o.new B();
		case 2:
			return o.new C();
		case 3:
			return o.new D();
		case 4:
			return o.new E();
		case 5:
			return o.new F();
		case 6:
			return o.new G();
		case 7:
			return o.new H();
		case 8:
			return o.new I();
		case 9:
			return o.new J();
		default:
			throw new Error("Only 10 different classes (A, ..., J) are implemented. ");
		}
	}

	public static long measureINSTANCHOH(final int numberOfClasses) {
		long start = System.currentTimeMillis();

		for (long i = 0; i < iterations; i++) {
			Base base = createClassRandomly(numberOfClasses);

			if (base instanceof A) {
				((A) base).doA();
			} else if (base instanceof B) {
				((B) base).doB();
			} else if (base instanceof C) {
				((C) base).doC();
			} else if (base instanceof D) {
				((D) base).doD();
			} else if (base instanceof E) {
				((E) base).doE();
			} else if (base instanceof F) {
				((F) base).doF();
			} else if (base instanceof G) {
				((G) base).doG();
			} else if (base instanceof H) {
				((H) base).doH();
			} else if (base instanceof I) {
				((I) base).doI();
			} else if (base instanceof J) {
				((J) base).doJ();
			}
		}
		return (System.currentTimeMillis() - start);
	}

	public static long measureOO(final int numberOfClasses) {
		long start = System.currentTimeMillis();

		for (long i = 0; i < iterations; i++) {
			Base base = createClassRandomly(numberOfClasses);
			base.doSomething();
		}
		return (System.currentTimeMillis() - start);
	}

	public static long measureTYPH(final int numberOfClasses) {
		long start = System.currentTimeMillis();

		for (long i = 0; i < iterations; i++) {
			Base base = createClassRandomly(numberOfClasses);

			switch (base.type) {
			case A:
				((A) base).doA();
				break;
			case B:
				((B) base).doB();
				break;
			case C:
				((C) base).doC();
				break;
			case D:
				((D) base).doD();
				break;
			case E:
				((E) base).doE();
				break;
			case F:
				((F) base).doF();
				break;
			case G:
				((G) base).doG();
				break;
			case H:
				((H) base).doH();
				break;
			case I:
				((I) base).doI();
				break;
			case J:
				((J) base).doJ();
				break;
			default:
				break;
			}
		}
		return (System.currentTimeMillis() - start);
	}

	private static long measureHHTCLASS(final int numberOfClasses) {
		long start = System.currentTimeMillis();

		for (long i = 0; i < iterations; i++) {
			Base base = createClassRandomly(numberOfClasses);

			if (base.getClass() == A.class) {
				((A) base).doA();
			} else if (base.getClass() == B.class) {
				((B) base).doB();
			} else if (base.getClass() == C.class) {
				((C) base).doC();
			} else if (base.getClass() == D.class) {
				((D) base).doD();
			} else if (base.getClass() == E.class) {
				((E) base).doE();
			} else if (base.getClass() == F.class) {
				((F) base).doF();
			} else if (base.getClass() == G.class) {
				((G) base).doG();
			} else if (base.getClass() == H.class) {
				((H) base).doH();
			} else if (base.getClass() == I.class) {
				((I) base).doI();
			} else if (base.getClass() == J.class) {
				((J) base).doJ();
			}
		}
		return (System.currentTimeMillis() - start);
	}

	public static void main(String[] args) {
		final boolean prettyOutput = true;
		if (prettyOutput) {
			System.out.printf("%-12s%-12s%-12s%-12s%-12s\n", "# Classes", "INSTANCHOH", "OO", "TYPH", "HHTCLASS");
			System.out.println("--------------------------------------------------------");
		} else {
			System.out.println("# Classes; INSTANCHOH; OO; TYPH; HHTCLASS");
		}

		for (int differentClasses = 2; differentClasses <= 10; differentClasses++) {
			final long timeINSTANCHOH = measureINSTANCHOH(differentClasses);
			final long timeOO = measureOO(differentClasses);
			final long timeTYPH = measureTYPH(differentClasses);
			final long timeHHTCLASS = measureHHTCLASS(differentClasses);
			if (prettyOutput) {
				System.out.printf("%-12d%-12d%-12d%-12d%-12d\n", differentClasses, timeINSTANCHOH, timeOO, timeTYPH,
						timeHHTCLASS);
			} else {
				System.out.printf("%d; %d; %d; %d; %d\n", differentClasses, timeINSTANCHOH, timeOO, timeTYPH,
						timeHHTCLASS);
			}
		}
	}

	// Type.java
	public enum Type {
		A, B, C, D, E, F, G, H, I, J
	};

	// Base.java
	public abstract class Base {
		int i = 1;
		final Type type;

		public Base(Type type) {
			this.type = type;
		}

		public abstract void doSomething();
	}

	// A.java
	public class A extends Base {

		public A() {
			super(Type.A);
		}

		public boolean isA() {
			return true;
		}

		public void doA() {
			i = i * -1;
		}

		@Override
		public void doSomething() {
			i = i * -1;
		}
	}

	// H.java
	public class B extends Base {

		public B() {
			super(Type.B);
		}

		public boolean isB() {
			return true;
		}

		public void doB() {
			i = i * -1;
		}

		@Override
		public void doSomething() {
			i = i * -1;
		}
	}

	public class C extends Base {

		public C() {
			super(Type.C);
		}

		public boolean isC() {
			return true;
		}

		public void doC() {
			i = i * -1;
		}

		@Override
		public void doSomething() {
			i = i * -1;
		}
	}

	public class D extends Base {

		public D() {
			super(Type.D);
		}

		public boolean isD() {
			return true;
		}

		public void doD() {
			i = i * -1;
		}

		@Override
		public void doSomething() {
			i = i * -1;
		}
	}

	public class E extends Base {

		public E() {
			super(Type.E);
		}

		public boolean isE() {
			return true;
		}

		public void doE() {
			i = i * -1;
		}

		@Override
		public void doSomething() {
			i = i * -1;
		}
	}

	// H.java
	public class F extends Base {

		public F() {
			super(Type.F);
		}

		public boolean isF() {
			return true;
		}

		public void doF() {
			i = i * -1;
		}

		@Override
		public void doSomething() {
			i = i * -1;
		}
	}

	// H.java
	public class G extends Base {

		public G() {
			super(Type.G);
		}

		public boolean isG() {
			return true;
		}

		public void doG() {
			i = i * -1;
		}

		@Override
		public void doSomething() {
			i = i * -1;
		}
	}

	// H.java
	public class H extends Base {

		public H() {
			super(Type.H);
		}

		public boolean isH() {
			return true;
		}

		public void doH() {
			i = i * -1;
		}

		@Override
		public void doSomething() {
			i = i * -1;
		}
	}

	// I.java
	public class I extends Base {

		public I() {
			super(Type.I);
		}

		public boolean isI() {
			return true;
		}

		public void doI() {
			i = i * -1;
		}

		@Override
		public void doSomething() {
			i = i * -1;
		}
	}

	// H.java
	public class J extends Base {

		public J() {
			super(Type.J);
		}

		public boolean isJ() {
			return true;
		}

		public void doJ() {
			i = i * -1;
		}

		@Override
		public void doSomething() {
			i = i * -1;
		}
	}

	// classes C to J follow the same logic.
}