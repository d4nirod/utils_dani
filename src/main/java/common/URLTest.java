package common;
import java.net.MalformedURLException;
import java.net.URL;


public class URLTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			URL a = new URL("http", "example.com", 80, "/pages/page1.html");
			System.out.println(a.toString());
			URL b = new URL("http", "example.com", 80, "pages/page1.html");
			System.out.println(b.toString());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}


	}

}
