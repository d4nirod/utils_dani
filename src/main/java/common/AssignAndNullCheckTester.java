/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package common;

import java.util.HashMap;
import java.util.Map;

public class AssignAndNullCheckTester {

	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		Map <String, Object> data = new HashMap<>();
		
		Map<String, Object> commentElement = new HashMap<>();
		commentElement.put("author", "jones");
		commentElement.put("text", "blah");
		
		data.put("comment", commentElement);
		System.out.println("Comment element exists and is not null");
		Map <String, Object> commentMap = null;
		if (data.containsKey("comment") && (commentMap = (Map<String, Object>) data.get("comment")) != null) {
			System.out.println("commentMap not null: " + (commentMap != null));
		} else {
			System.out.println("commentMap is null: "  + (commentMap == null));
		}
		
		System.out.println("Comment element exists but is null");
		data.put("comment", null);
		commentMap = null;
		if (data.containsKey("comment") && (commentMap = (Map<String, Object>) data.get("comment")) != null) {
			System.out.println("commentMap not null: " + (commentMap != null));
		} else {
			System.out.println("commentMap is null: "  + (commentMap == null));
		}
		
		System.out.println("Comment element does not exist");
		data.remove("comment");
		commentMap = null;
		if (data.containsKey("comment") && (commentMap = (Map<String, Object>) data.get("comment")) != null) {
			System.out.println("commentMap not null: " + (commentMap != null));
		} else {
			System.out.println("commentMap is null: "  + (commentMap == null));
		}
		
		
		
	}

}
