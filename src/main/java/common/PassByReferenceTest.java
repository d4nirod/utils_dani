package common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PassByReferenceTest {

	public static void main(String[] args) {
		List<Integer> origList = new ArrayList<>(Arrays.asList(0, 1, 2)); 
		addToList(origList);
		System.out.println(Arrays.toString(origList.toArray()));
		
		origList = addToList2(origList);
		System.out.println(Arrays.toString(origList.toArray()));

		addToList3(origList);
		System.out.println(Arrays.toString(origList.toArray()));
	}
	
	public static void addToList(List<Integer> list) {
		list.add(3);
	}
	
	public static List<Integer> addToList2(List<Integer> list) {
		List<Integer> result = new ArrayList<>(list);
		
		result.add(4);
		return result;
	}
	
	public static void addToList3(List<Integer> list) {
		list = addToList2(list);
	}

}
