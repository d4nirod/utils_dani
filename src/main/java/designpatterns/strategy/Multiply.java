package designpatterns.strategy;

/** Implements the algorithm using the strategy interface */
public class Multiply implements Strategy {

	@Override
	public int execute(int a, int b) {
		System.out.println("Called Subtract's execute()");
		return a - b; // Do a subtraction with a and b
	}

}
