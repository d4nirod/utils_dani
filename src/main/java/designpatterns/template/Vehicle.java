package designpatterns.template;

/**
 * The Template Method design pattern defines the workflow for achieving a specific operation.
 * It allows the subclasses to modify certain steps without changing the workflow*s structure.
 */
abstract public class Vehicle {
	// set to protected so that subclass can access
	protected boolean status;

	abstract void start();

	abstract void run();

	abstract void stop();

	public void testYourVehicle() {
		start();
		if (this.status) {
			run();
			stop();
		}
	}
}
