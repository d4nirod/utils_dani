package designpatterns.template;

public class Truck extends Vehicle {

	@Override
	void start() {
		this.status = true;
	}

	@Override
	void run() {
		System.out.println("      _____          ");
		System.out.println(" ____//__]|          ");
		System.out.println("(o _ |   -|_________ ");
		System.out.println(" `(_)-------(_)(_)--'");
//		System.out.println("Run slowly!");
	}

	@Override
	void stop() {
		System.out.println("Truck stop!");

	}
}
