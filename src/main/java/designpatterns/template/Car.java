package designpatterns.template;

public class Car extends Vehicle {

	@Override
	void start() {
		this.status = true;
	}

	@Override
	void run() {
		System.out.println("      _______    ");
		System.out.println(" ____//__][__\\___");
		System.out.println("(o _ |  -|   _  o|");
		System.out.println(" `(_)-------(_)--'");
//		System.out.println("Run fast!");
	}

	@Override
	void stop() {
		System.out.println("Car stop!");
	}
}
