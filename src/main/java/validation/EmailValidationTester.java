package validation;

import java.util.Arrays;
import java.util.List;

import javax.mail.internet.InternetAddress;

public class EmailValidationTester {

	public static void main(String[] args) {
		List<String> emails = Arrays
				.asList(new String[] { null, "", "blah@bl", "dude@bla.london", "someone@[10.10.1.5]", "user@localhost",
						"dud@t.c", "dude@t", "dude@", "dude@blah..london", "dude@adf,com", "dude@adfa@asdfa.com", "\"John Doe\" <jon.doe@tes.com>", "Joe Bloggs<joe.bloggs@test.com>" });
		for (String email : emails) {
			boolean valid = false;
			try {
				InternetAddress emailAddr = new InternetAddress(email, true);
				emailAddr.validate();
				valid = true;
				System.out.println(String.format("[%s]: Personal: '%s', Address: '%s' - Valid: %s", email, emailAddr.getPersonal(), emailAddr.getAddress(), valid));
			} catch (Exception ex) {
				System.out.println("'" + email + "' : " + valid);
			}
		}

	}

}
