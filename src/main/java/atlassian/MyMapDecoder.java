package atlassian;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.PatternSyntaxException;

public class MyMapDecoder implements MapDecoder
{
    public Map<String, String> decode(String s)  {
    	Map<String, String> maap = new HashMap<String, String>();
    	if(s==null) return null;
    	else if(s.trim().equals("")) return maap ;
    	try {
	    	String[] params =  s.split("&");
	    	if(params==null || params.length==0)
	    		throw new IllegalArgumentException() ;
	    	
	    	for(int i=0; i<params.length; i++){
	    		if(params[i].equals("="))
	    			throw new IllegalArgumentException();
	    		else if(params[i].startsWith("="))
	    			maap.put(null, params[i].substring(1));
	    		else if(params[i].endsWith("="))
	    			maap.put(params[i].substring(0, params[i].length()-1), null);
	    		
	    		else{
		    		String[] pair = params[i].split("=");
		    		maap.put(pair[0], pair[1]);
	    		}
	
	    	}
	        return maap;
    	}catch (PatternSyntaxException e){
    		throw new IllegalArgumentException() ; 
    	}
    }
}