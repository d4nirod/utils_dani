package atlassian;

public class Test0 {

	private static int value = 0;

	public static int getValue()
	{
	    if (value == 0)
	    {
	        value = calculateValueOnce();
	    }
	    return value;
	}

	private static int calculateValueOnce() {
		return 99;
	}

}
