package mq;

import java.nio.file.Files;
import java.nio.file.Paths;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;

/**
 * Hello world!
 */
public class TestSender {

	private static final String PUSH_MESSAGES_ANDROID = "push-messaging.messages.android";
	private static final String PUSH_MESSAGES_FAKEAPNS = "push-messages-fakeapns";

	public static void main(String[] args) throws Exception {
		int repeats = 1;
		for (int i = 0; i < repeats; i++) {
			thread(new PushMessageProducer(PUSH_MESSAGES_ANDROID), false);
//			thread(new PushMessageProducer(PUSH_MESSAGES_FAKEAPNS), false);
		}

//		Thread.sleep(1000);
//
//		thread(new PushMessageProducer(PUSH_MESSAGES_FAKEAPNS), false);
//		thread(new PushMessageProducer(PUSH_MESSAGES_ANDROID), false);
//		
//		Thread.sleep(1000);
//
//		thread(new PushMessageProducer(PUSH_MESSAGES_ANDROID), false);
//		thread(new PushMessageProducer(PUSH_MESSAGES_FAKEAPNS), false);
//		
//		Thread.sleep(1000);
//		thread(new PushMessageProducer(PUSH_MESSAGES_FAKEAPNS), false);
//		thread(new PushMessageProducer(PUSH_MESSAGES_ANDROID), false);

	}

	public static void thread(Runnable runnable, boolean daemon) {
		Thread brokerThread = new Thread(runnable);
		brokerThread.setDaemon(daemon);
		brokerThread.start();
	}

	public static class PushMessageProducer implements Runnable {
		private static PooledConnectionFactory connectionFactory = null;
		private static Object lock = new Object();
		
		private String queue;
		
		static {
			synchronized (lock) {
				if (connectionFactory == null) {
					connectionFactory = new PooledConnectionFactory();
					connectionFactory.setConnectionFactory(new ActiveMQConnectionFactory("tcp://localhost:61616"));
					connectionFactory.setMaxConnections(5);
				}
			}
		}

		public PushMessageProducer(String queue) {
			this.queue = queue;
		}
		
		public void run() {
			try {
				// Create a ConnectionFactory

				// Create a Connection
				Connection connection = connectionFactory.createConnection();
				connection.start();

				// Create a Session
				Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

				// Create the destination (Topic or Queue)
				Destination destination = session.createQueue(queue);

				// Create a MessageProducer from the Session to the Topic or Queue
				MessageProducer producer = session.createProducer(destination);
				producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

				// Create a messages
//				String text = "{\n" + 
//						"  \"token\": \"eIEdkB0wfKw:APA91bEHpP0qfZvYfQ_kOAY6eB90x8N1nmBTo78rIad5_fC6F1znxBqnkcC81T0HzrukhWDOFer9lo3h3FmNPxIWlgxeTxvmpQzLIxv6mNc-YnLRiGK75y5YWkNkK0rw1Sxx_KGl8CXF\",\n" + 
//						"  \"notification\":{\n" + 
//						"    \"title\": \"" + Thread.currentThread().getName() + " : " + this.hashCode() + "\",\n" + 
//						"    \"body\": \"Consectetur adipiscing elit.Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"\n" + 
//						"  }\n" + 
//						"}";
				
//				TextMessage message = session.createTextMessage(text);
				BytesMessage message = session.createBytesMessage();
				byte[] bFile = Files.readAllBytes(Paths.get("superdry6.png"));
				message.writeBytes(bFile);
				
				
				// Tell the producer to send the message
				System.out.println("Sent message: "+ message.hashCode() + " : " + Thread.currentThread().getName());
				producer.send(message);

				// Clean up
				session.close();
				connection.close();
			}
			catch (Exception e) {
				System.out.println("Caught: " + e);
				e.printStackTrace();
			}
		}
	}

//	public static class HelloWorldConsumer implements Runnable, ExceptionListener {
//		public void run() {
//			try {
//
//				// Create a ConnectionFactory
//				ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("vm://localhost");
//
//				// Create a Connection
//				Connection connection = connectionFactory.createConnection();
//				connection.start();
//
//				connection.setExceptionListener(this);
//
//				// Create a Session
//				Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//
//				// Create the destination (Topic or Queue)
//				Destination destination = session.createQueue("TEST.FOO");
//
//				// Create a MessageConsumer from the Session to the Topic or Queue
//				MessageConsumer consumer = session.createConsumer(destination);
//
//				// Wait for a message
//				Message message = consumer.receive(1000);
//
//				if (message instanceof TextMessage) {
//					TextMessage textMessage = (TextMessage) message;
//					String text = textMessage.getText();
//					System.out.println("Received: " + text);
//				} else {
//					System.out.println("Received: " + message);
//				}
//
//				consumer.close();
//				session.close();
//				connection.close();
//			} catch (Exception e) {
//				System.out.println("Caught: " + e);
//				e.printStackTrace();
//			}
//		}
//
//		public synchronized void onException(JMSException ex) {
//			System.out.println("JMS Exception occured.  Shutting down client.");
//		}
//	}
}