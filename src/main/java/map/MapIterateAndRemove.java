/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package map;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public class MapIterateAndRemove {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Map<Integer, String> map = new HashMap<>();
		map.put(1, "one");
		map.put(2, "two");
		map.put(3, "three");
		map.put(4, "four");
		map.put(5, "five");
		map.put(6, "six");
		
		Set<Integer> toRemove = new HashSet<>(Arrays.asList(2, 5));
		printMap(map);
		
		System.out.println("\nProcessing map entries. Removing: " + StringUtils.join(toRemove, ", "));
//		map.keySet().removeIf(e -> (e == 2)); // <-- remove here
		Iterator<Entry<Integer, String>> it = map.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Integer, String> entry = it.next();
			if (toRemove.contains(entry.getKey())) {
				it.remove();
				System.out.println("\t- Removing key: " + entry.getKey());
				continue;
			}
			System.out.println("\t+ Processing key: " + entry.getKey());
			
		}

		System.out.println("\nAfter removing element");

		printMap(map);

	}

	protected static void printMap(Map<Integer, String> map) {
		map.forEach((key, value) -> {
			System.out.println("Key: " + key + "\t" + " Value: " + value);
		});
	}

}
