package numbers;

import java.util.ArrayList;
import java.util.List;

public class TestModuloOperator {

	public static void main(String[] args) {
		int divisor = 100;
		List<Integer> integers = new ArrayList<>();
		for (int i = 0; i < 300; i+=9) {
			integers.add(Integer.valueOf(i));
		}
		
		integers.stream().mapToInt(i->i).forEach(i -> {
			int quotient = i/divisor;
			int reminder = i % divisor;
			int floorMod = Math.floorMod(i, divisor);
			int floorDiv = Math.floorDiv(i, divisor);
			System.out.println(String.format(
					"quotient: %d/%d = %d, reminder %d %% %d = %d, floorMod(%d, %d) = %d, floorDiv(%d, %d) = %d,  iterations: %d ", 
					i, divisor, quotient, 
					i, divisor, reminder, 
					i, divisor, floorMod, 
					i, divisor, floorDiv,
					quotient + (int) Math.ceil((double)reminder/(double)divisor)
					));
			
		});

		for (double d = 0.0d; d < 2.1d; d += 0.1d) {
			System.out.println(d + ", round: " + (int)Math.round(d) + ", ceil: " + (int)Math.ceil(d));
		}
	}

}
