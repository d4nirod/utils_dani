package numbers;

public class Redondeo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int items = 11;
		int its_p_page =  3;

		// with out library
		int pages = items/its_p_page + (items % its_p_page > 0 ? 1 : 0);
		System.out.println(pages);
		
		// with library
		int pages2 = (int)Math.ceil((double)items / (double)its_p_page);
		System.out.println(pages2);

		//another one
		int pages3 = (items + its_p_page - 1) / its_p_page;
		System.out.println(pages3);
		
	}

}
