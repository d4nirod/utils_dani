package numbers;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class BigDecimalTest {
	public static void main(String[] args) {

		BigDecimal b = new BigDecimal("+9999999999999999999999999.99");
		System.out.println("toString: "+b.toString());
		String plain = b.toPlainString();
		System.out.print("toPlainString: "+plain);
		String[] parts = plain.split("\\.");
		System.out.println(" [Whole: "+parts[0]+"] [fractional: "+parts[1]+"]");

		System.out.println("toEngineeringString: "+b.toEngineeringString());

//		String s = "9999999999999";

		//esto es de los ficheros de Hacienda, 13 posiciones en total, las 2 ultimas decimales
		// 0000000043272 son 432,72 EUR
		String s = "0000000043272";
		long l = Long.parseLong(s);
		double d = l / 100.0;
		System.out.printf("%s: String>long>double: %s\n", s, String.valueOf(d));

		BigDecimal bd = new BigDecimal(s+"E-2");
		System.out.printf("new BigDecimal(\"%sE-2\") toString: %s\n", s, bd.toString());
		NumberFormat nf =  NumberFormat.getCurrencyInstance(new Locale("es", "ES"));
		System.out.printf("new BigDecimal(\"%sE-2\") NumberFormat currency : %s\n", s,
				           nf.format(bd.setScale(2, RoundingMode.HALF_EVEN)));

		bd = new BigDecimal(s).movePointLeft(2);
		System.out.printf("new BigDecimal(\"%s\").movePointLeft(2) toString: %s\n", s, bd.toString());
		bd = new BigDecimal(new BigInteger(s), 2);
		System.out.printf("new BigDecimal(new BigInteger(\"%s\"), 2) toString: %s\n", s, bd.toString());

		System.out.println("No decimal separator: "+ b.multiply(new BigDecimal(100)));




	}

}
