package numbers;

import java.math.BigDecimal;
import static java.lang.System.out;

/**
 * Simple example of problems associated with using BigDecimal constructor
 * accepting a double.
 *
 * http://marxsoftware.blogspot.com/
 */
public class DoubleToBigDecimal
{
   private final static String NEW_LINE = System.getProperty("line.separator");

   public static void main(final String[] arguments)
   {
      //
      // Demonstrate BigDecimal from double
      //
      final double primitiveDouble = 0.1;
      final BigDecimal bdPrimDoubleCtor = new BigDecimal(primitiveDouble);
      final BigDecimal bdPrimDoubleValOf = BigDecimal.valueOf(primitiveDouble);
      final Double referenceDouble = Double.valueOf(0.1);
      final BigDecimal bdRefDoubleCtor = new BigDecimal(referenceDouble);
      final BigDecimal bdRefDoubleValOf = BigDecimal.valueOf(referenceDouble);

      out.println("Primitive Double: " + primitiveDouble);
      out.println("Reference Double: " + referenceDouble);
      out.println("Primitive BigDecimal/Double via Double Ctor: " + bdPrimDoubleCtor);
      out.println("Reference BigDecimal/Double via Double Ctor: " + bdRefDoubleCtor);
      out.println("Primitive BigDecimal/Double via ValueOf: " + bdPrimDoubleValOf);
      out.println("Reference BigDecimal/Double via ValueOf: " + bdRefDoubleValOf);

      out.println(NEW_LINE);

      //
      // Demonstrate BigDecimal from float
      //
      final float primitiveFloat = 0.1f;
      final BigDecimal bdPrimFloatCtor = new BigDecimal(primitiveFloat);
      final BigDecimal bdPrimFloatValOf = BigDecimal.valueOf(primitiveFloat);
      final Float referenceFloat = Float.valueOf(0.1f);
      final BigDecimal bdRefFloatCtor = new BigDecimal(referenceFloat);
      final BigDecimal bdRefFloatValOf = BigDecimal.valueOf(referenceFloat);

      out.println("Primitive Float: " + primitiveFloat);
      out.println("Reference Float: " + referenceFloat);
      out.println("Primitive BigDecimal/Float via Double Ctor: " + bdPrimFloatCtor);
      out.println("Reference BigDecimal/Float via Double Ctor: " + bdRefFloatCtor);
      out.println("Primitive BigDecimal/Float via ValueOf: " + bdPrimFloatValOf);
      out.println("Reference BigDecimal/Float via ValueOf: " + bdRefFloatValOf);

      out.println(NEW_LINE);

      //
      // More evidence of issues casting from float to double.
      //
      final double primitiveDoubleFromFloat = 0.1f;
      final Double referenceDoubleFromFloat = new Double(0.1f);
      final double primitiveDoubleFromFloatDoubleValue = new Float(0.1f).doubleValue();

      out.println("Primitive Double from Float: " + primitiveDoubleFromFloat);
      out.println("Reference Double from Float: " + referenceDoubleFromFloat);
      out.println("Primitive Double from FloatDoubleValue: " + primitiveDoubleFromFloatDoubleValue);

      //
      // Using String to maintain precision from float to BigDecimal
      //
      final String floatString = String.valueOf(new Float(0.1f));
      final BigDecimal bdFromFloatViaString = new BigDecimal(floatString);
      out.println("BigDecimal from Float via String.valueOf(): " + bdFromFloatViaString);
   }
}
