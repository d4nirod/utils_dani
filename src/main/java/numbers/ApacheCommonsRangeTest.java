package numbers;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.Range;

public class ApacheCommonsRangeTest {

	public static void main(String[] args) {
		Range<BigDecimal> tramo1 = Range.between(new BigDecimal(3000.00), new BigDecimal(6000.00));
		Range<BigDecimal> tramo2 = Range.between(new BigDecimal(6000.01), new BigDecimal(12000.00));
		Range<BigDecimal> tramo3 = Range.between(new BigDecimal(12000.01), new BigDecimal(30000.00));

		List<BigDecimal> importes = Arrays.asList(new BigDecimal[]{new BigDecimal(3457.23), new BigDecimal(6000.03), new BigDecimal(12000), new BigDecimal(21000.17)});
		for(BigDecimal d : importes){
			System.out.print(d.floatValue()+ " : ");
			if(tramo1.contains(d)) System.out.println("tramo1");
			else if(tramo2.contains(d)) System.out.println("tramo2");
			else if(tramo3.contains(d)) System.out.println("tramo3");
			else System.out.println("ninguno");
		}
	}

}
