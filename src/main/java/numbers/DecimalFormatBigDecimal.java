package numbers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

public class DecimalFormatBigDecimal {

	public static void main(String[] args) {
//		DecimalFormat df = new DecimalFormat("#,##0.00");
//		seg*n locale
//		NumberFormat fmt = NumberFormat.getCurrencyInstance();
		//forzar el formato espa*ol:
		NumberFormat fmt = NumberFormat.getCurrencyInstance(new Locale("es","ES"));

		BigDecimal bd = new BigDecimal(123456789.12);
		System.out.println("Locale formated: "+fmt.format(bd));
		System.out.println("toPlainString: "+ bd.toPlainString());
		BigDecimal scaled = bd.setScale(2, RoundingMode.HALF_EVEN);
		System.out.println("original toPlainString: "+ bd.toPlainString());
		System.out.println("scaled toPlainString: "+ scaled.toPlainString());
		System.out.println("scaled toPlainString no decimal point: "+ StringUtils.remove(scaled.toPlainString(), '.'));




	}

}
