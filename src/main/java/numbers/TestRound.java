package numbers;
import java.math.*;

public class TestRound {

	/**
	 * @param args
	 */
	public static void main(String args[]){
	    double d = 3.5537;
	    BigDecimal bd = new BigDecimal(d);
	    RoundingMode rm;
	    bd = bd.setScale(2, RoundingMode.HALF_EVEN);
	    System.out.println(d + " : " + bd.toPlainString());

	    // output is 3.15
	    System.out.println(d + " : " + round(d, 0));
	    // output is 3.154
	    System.out.println(d + " : " + round(d, 1));
	  }

	  public static double round(double d, int decimalPlace){
	    // see the Javadoc about why we use a String in the constructor
	    // http://java.sun.com/j2se/1.5.0/docs/api/java/math/BigDecimal.html#BigDecimal(double)
	    BigDecimal bd = new BigDecimal(Double.toString(d));
	    bd = bd.setScale(decimalPlace, RoundingMode.HALF_EVEN);
	    return bd.doubleValue();
	  }
}



