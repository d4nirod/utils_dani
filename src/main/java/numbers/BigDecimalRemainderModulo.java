package numbers;

import java.math.BigDecimal;

public class BigDecimalRemainderModulo {

	public static void main(String[] args) {
		BigDecimal number = new BigDecimal(-5);
		BigDecimal divisor = new BigDecimal("0.3");
		BigDecimal result = number.remainder(divisor);
		System.out.println(String.format(
				"remainder(%s/%s) = %s", number.toPlainString(), divisor.toPlainString(), result.toPlainString()));

	}

}
