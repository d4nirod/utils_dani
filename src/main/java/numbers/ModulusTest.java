package numbers;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ModulusTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String[] arr = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60"};
		String[] sub = new String[15];
		int cnt=1;
		int subIdx = 0;
		for(int i=0; i<arr.length; i++){
			//a*adimos
			sub[subIdx++] = arr[i];

			//miramos si el siguiente ser* multiplo de 15
			int mod = (i+1)%15;
			if(mod==0 || i==(arr.length-1)){

				System.out.printf("Subarray %d: [",cnt++);
				//enviamos - imprimimos
				for(int j=0; j<sub.length; j++){
					System.out.print(sub[j]+ " ");
				}
				System.out.println("]");
				subIdx = 0;
				sub = new String[15];
			}
		}
		System.out.println("**********************************************");
		cnt = 0;

		List<String> arrl =  Arrays.asList(arr);
		List<String> subl;
		int len = arrl.size();
		int mod = len%15;
		int div = len/15;

		if(len <= 15 ){
			subl = arrl;
		}else {
			for(int i=0; (i+15)<len; i+=15){
				subl = arrl.subList(i, i+15);
				//enviamos - imprimimos
				printSublist(subl, cnt++ );
			}
			if(mod!=0){
				subl = arrl.subList(len-mod, len);
				printSublist(subl, cnt++ );
			}
			System.out.println("**********************************************");
			cnt = 0;
			
			int from=0;
			for(int i=0; i<div; i++){
				subl = arrl.subList(from, from+15);
				printSublist(subl, cnt++ );
				from+=15;
			}
			if(mod!=0){
				subl = arrl.subList(len-mod, len);
				printSublist(subl, cnt++ );
			}

		}




	}
	static void printSublist(List l, int cnt){
		System.out.printf("Sublist %d: [",cnt);
		for(int j=0; j<l.size(); j++){
			System.out.print(l.get(j)+ " ");
		}
		System.out.println("]");
	}
}
