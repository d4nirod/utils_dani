package cryptotest;

import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;

import common.HexUtil;



public class DesMacTest {
	/**
	 * @param args
	 * Cifrado DES - FIPS-81 http://www.itl.nist.gov/fipspubs/fip81.htm
	 * DES-MAC - FIPS-113 http://csrc.nist.gov/publications/fips/fips113/fips113.html
	 */
    public static void main (String[] args) throws Exception {

        String algorithm = "DES/CBC/NoPadding";   //BC soporta DES/CBC/ZeroBytePadding

//		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()); //"BC"

        Provider[] providers = Security.getProviders();
        for (int p = 0; p < providers.length; p++)
        System.out.println(providers[p]);

		Cipher cipher = Cipher.getInstance(algorithm);
//        Cipher cipher = Cipher.getInstance(algorithm);

        System.out.println("Algorithm: "+cipher.getAlgorithm());
//		System.out.println(cipher.);

        byte[] plainText = "7654321 Now is the time for ".getBytes();
        byte[] keyBytes =  HexUtil.fromHexString("0123456789abcdef");

        //KeyParameter key = new KeyParameter(keyBytes);

        // Get a HmacMD5 object and update it with the plaintext
//        Mac mac = Mac.getInstance(algorithm, "BC");

        DESKeySpec keySpec = new DESKeySpec(keyBytes);
        SecretKey key = SecretKeyFactory.getInstance("DES").generateSecret(keySpec);
//        SecretKey key = SecretKeyFactory.getInstance("DES").generateSecret(keySpec);

        // Get a HmacSHA1 object and update it with the plaintext
        // Mac mac = Mac.getInstance("HmacSHA1");
        Mac mac = Mac.getInstance("DES");        //NO SOPORTA DES HAY QUE UTILIZAR BC PROVIDER
        mac.init(key);
        mac.update(plainText);
        byte[] result = mac.doFinal();
        System.out.println("Key hex      : "+ common.HexUtil.toHexString(keyBytes));
        System.out.println("Plaintext hex: "+common.HexUtil.toHexString(plainText));
        System.out.println( "\n" + mac.getProvider().getInfo() );
        System.out.println( "\nMAC: "+ common.HexUtil.toHexString(result) );
    }
}
