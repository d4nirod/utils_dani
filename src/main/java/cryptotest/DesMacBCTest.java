package cryptotest;

import java.io.UnsupportedEncodingException;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.engines.DESEngine;
import org.bouncycastle.crypto.macs.CBCBlockCipherMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.encoders.Hex;



/* Calcular la clave MAC utilizando DES seg*n ANSI X9.9-1
 *
 */
public class DesMacBCTest {

	/**
	 * @param args
	 * Cifrado DES - FIPS-81 http://www.itl.nist.gov/fipspubs/fip81.htm
	 * DES-MAC - FIPS-113 http://csrc.nist.gov/publications/fips/fips113/fips113.html
	 */
	public static void main(String[] args) {

		try {
			//Clave verificacion     FIPS-113: "0123456789abcdef"
			//plaintext verificacion FIPS-113: "7654321 Now is the time for "
			//MAC a obtener 		 FIPS-113: "f1d30f68"
			byte[] keyBytes  =  Hex.decode("0123456789abcdef");
			byte[] plaintextBytes = "7654321 Now is the time for ".getBytes("UTF8");

			//Clave pruebas Canarias: 				"12308EA039ACD836"
			//texto comprobacion pruebas Canarias:  "F0F0F0F0F0F0F0F0"
			//["00000000" (ocho ceros en c*digo EBCDIC en hex: "F0F0F0F0F0F0F0F0")]
			//MAC a obtener:						"8E1B98BE"

//			byte[] keyBytes  =  Hex.decode("12308ea039acd836");
//			//byte[] plaintextBytes = "00000000".getBytes("Cp1047"); //EBCDIC
//			byte[] plaintextBytes = Hex.decode("f0f0f0f0f0f0f0f0");


			KeyParameter        key = new KeyParameter(keyBytes);
	        BlockCipher         cipher = new DESEngine();
	        Mac                 mac = new CBCBlockCipherMac(cipher);

	        mac.init(key);

	        mac.update(plaintextBytes, 0, plaintextBytes.length);

	        byte[]  out = new byte[4];

	        mac.doFinal(out, 0);

	        System.out.println("Key hex      : "+new String(Hex.encode(keyBytes), "UTF-8"));
	        System.out.println("Plaintext hex: "+new String(Hex.encode(plaintextBytes), "UTF-8"));

	        String macStr = new String(Hex.encode(out), "UTF-8");
	        System.out.println("MAC hex      : "+macStr);

		} catch (UnsupportedEncodingException e) {
		
			e.printStackTrace();
		};
	}
}
