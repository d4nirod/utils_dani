package fileupload;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.output.ByteArrayOutputStream;

public class FileUploadTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		InputStream is = new ByteArrayInputStream("la cadena".getBytes());
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			long nbytes = Streams.copy(is, os, true);
			System.out.println("read bytes: "+nbytes+" os content:"+os.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
