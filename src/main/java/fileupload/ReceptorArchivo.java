package fileupload;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.FileUploadBase.FileSizeLimitExceededException;
import org.apache.commons.fileupload.FileUploadBase.FileUploadIOException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;

public class ReceptorArchivo {

	public static final String DEFAULT_CHARSET_IBM850 = "IBM850";
	public static final int DEFAULT_MAX_REGISTROS = 99999;
	public static final int UNLIMITED_MAX_REGISTROS = -1;
	

	public static final String CHARSET_ISO_8859_15 = "ISO-8859-15";
	public static final long DEFAULT_MAX_BYTES = 25 * 1024 * 1024; // 25MB
	public static final long UNLIMITED_MAX_BYTES = -1L;
	
	public static final String FILETYPES_MURO = ".+\\.(zip|rar|7z|pdf|gif|jpe?g|png|tiff?|bmp|txt|csv|html?|rtf|docx?|xlsx?|pptx?|odt?s?p?)$";

	private String fileName;
	private String fullFileName;
	private String contentType;
	private Map<String, String> params;
	private byte[] fileData;
	private BufferedReader in;
	private String logString;
	private String charset;
	private final long maxBytes;
	private final int maxRegistros;
	private final Pattern fileTypesPattern;

	public ReceptorArchivo() {
		this(DEFAULT_CHARSET_IBM850, DEFAULT_MAX_BYTES, DEFAULT_MAX_REGISTROS, null);
	}

	/**
	 * Constructor indica charset distinto al por defecto IBM850
	 * tama*o m*ximo de fichero a subir en bytes ser* por defecto ReceptorArchivo.DEFAULT_MAX_BYTES (26214400 bytes (25MB))
	 * n*mero m*ximo de registros (lineas de texto) en fichero ser* por defecto  ReceptorArchivo.DEFAULT_MAX_REGISTROS (99999)
	 * @param charset encoding a utilizar
	 */
	public ReceptorArchivo(String charset) {
		this(charset, DEFAULT_MAX_BYTES, DEFAULT_MAX_REGISTROS, null);
	}
	
	/**
	 * Constructor indica charset distinto al por defecto IBM850
	 * tama*o m*ximo de fichero a subir en bytes ser* por defecto ReceptorArchivo.DEFAULT_MAX_BYTES (26214400 bytes (25MB))
	 * n*mero m*ximo de registros (lineas de texto) en fichero ser* por defecto  ReceptorArchivo.DEFAULT_MAX_REGISTROS (99999)
	 * @param charset encoding a utilizar
	 * @param fileTypesRegex expresion regular para el filtrar el tipo de archivo permitido
	 */
	public ReceptorArchivo(String charset, String fileTypesRegex) {
		this(charset, DEFAULT_MAX_BYTES, DEFAULT_MAX_REGISTROS, fileTypesRegex);
	}

	/**
	 * Constructor indica charset distinto al por defecto IBM850 y nmero mximo de registros distinto al por defecto.
	 * 
	 * @param charset
	 * @param maxBytes tama*o m*ximo de fichero a subir en bytes. ReceptorArchivo.UNLIMITED_MAX_BYTES (-1) ilimitado             
	 * @param maxRegistros n*mero m*ximo de registros (lineas de texto) en fichero. ReceptorArchivo.UNLIMITED_MAX_REGISTROS (-1) es ilimitado.            
	 * @param fileTypesRegex expresion regular para el filtrar el tipo de archivo permitido
	 */
	public ReceptorArchivo(String charset, long maxBytes, int maxRegistros, String fileTypesRegex) {
		this.charset = charset;
		this.maxBytes = maxBytes;
		this.maxRegistros = maxRegistros;
		if(StringUtils.isNotBlank(fileTypesRegex))
			this.fileTypesPattern = Pattern.compile(fileTypesRegex, Pattern.CASE_INSENSITIVE);
		else
			this.fileTypesPattern = null;
		params = new HashMap<String, String>(10);
		in = null;
		setLogString("");
	}

	public long procesaArchivo(HttpServletRequest request) throws IOException {
		request.setCharacterEncoding(charset);
		InputStream is = null;
		ByteArrayOutputStream os = null;
		try {
			if (!ServletFileUpload.isMultipartContent(request))
				throw new FileUploadBase.InvalidContentTypeException();
			ServletFileUpload upHandler = new ServletFileUpload();
			if (this.maxBytes > ReceptorArchivo.DEFAULT_MAX_BYTES)
				upHandler.setFileSizeMax(this.maxBytes);
			FileItemIterator it = upHandler.getItemIterator(request);
			StringBuffer logMsg = new StringBuffer();
			long bytesRead = 0L;
			while (it.hasNext()) {
				FileItemStream item = it.next();
				String name = item.getFieldName();
				is = item.openStream();
				if (item.isFormField()) {
					String content = Streams.asString(is);
					logMsg.append(name).append("=").append(StringUtils.substring(content, 0, 20)).append(" ");
					params.put(name, content);
				} else {
					this.contentType = item.getContentType();
					fullFileName = item.getName();
					fileName = FilenameUtils.getName(item.getName());
					if(this.fileTypesPattern!=null && !fileTypesPattern.matcher(fileName).matches())
						throw new FileUploadIOException(new InvalidFileTypeException("Tipo de fichero no admitido."));

					logMsg.append("| ").append(name).append(" : fileName=").append(fileName).append(" | ");
					is = ByteArrayOutputStream.toBufferedInputStream(is);
					os = new ByteArrayOutputStream();
					bytesRead = Streams.copy(is, os, false);
					fileData = os.toByteArray();
					if (maxRegistros > ReceptorArchivo.UNLIMITED_MAX_REGISTROS) {
						int regs = countDataLines();
						if (regs > maxRegistros) {
							throw new FileUploadIOException(new FileSizeLimitExceededException("El fichero excede el n*mero m*ximo de registros permitidos", regs, maxRegistros));
						}
					}
				}
			}
			this.setLogString(logMsg.toString());
			return bytesRead;

		} catch (IOException ioe) {
			if (ioe.getCause() instanceof FileSizeLimitExceededException) {
				FileSizeLimitExceededException e = (FileSizeLimitExceededException) ioe.getCause();
				if (e.getPermittedSize() == this.maxBytes)
					ioe = new FileUploadIOException(new FileSizeLimitExceededException("El fichero excede el tama*o (bytes) m*ximo permitido", e.getActualSize(), e.getPermittedSize()));
			}
			throw ioe;
			
		} catch (FileUploadException fue) {
			throw new FileUploadIOException(fue);
			
		}finally {
			if(is!=null) try{  is.close();  }catch(Exception e){}
			if(os!=null) try{  os.close();  }catch(Exception e){}
		}
	}

	public byte[] getFileData() {
		return this.fileData;
	}

	/**
	 * Cuenta el numero de lineas en los datos del fichero
	 * 
	 * @return numero de lineas
	 * @throws IOException
	 */
	private int countDataLines() throws IOException {
		if (this.fileData.length <= 0)
			return 0;
		LineNumberReader lnr = new LineNumberReader(new StringReader(new String(this.fileData, charset)));
		lnr.skip(Long.MAX_VALUE);
		return lnr.getLineNumber();
	}

	/**
	 * Lee el fichero por lineas
	 * 
	 * @param max
	 * @return Lista de strings con las lineas del fichero
	 * @throws IOException
	 */
	public ArrayList<String> leerLineas(int max) throws IOException {
		if (in == null)
			in = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(fileData), charset));
		ArrayList<String> regs = new ArrayList<String>(max);
		String line = null;
		while ((regs.size() < max) && (line = in.readLine()) != null)
			if (!line.trim().equals(""))
				regs.add(line);
		return regs;
	}

	public String getFileName() {
		return fileName;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public String getParam(String name) {
		return params.get(name);
	}

	public Set<String> getParamNames() {
		return params.keySet();
	}

	public void setFullFileName(String fullFileName) {
		this.fullFileName = fullFileName;
	}

	public String getFullFileName() {
		return fullFileName;
	}

	public String getLogString() {
		return logString;
	}

	public void setLogString(String logString) {
		this.logString = logString;
	}

	public int getMaxRegistros() {
		return maxRegistros;
	}

	public long getMaxBytes() {
		return maxBytes;
	}

	public String getContentType() {
		return contentType;
	}
}
