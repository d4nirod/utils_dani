package fileupload;

import org.apache.commons.fileupload.FileUploadException;

public class InvalidFileTypeException extends FileUploadException {

	private static final long serialVersionUID = -6400920580748208297L;

	public InvalidFileTypeException(String msg) {
		super(msg);
	}
}
