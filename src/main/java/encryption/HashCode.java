package encryption;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;

public class HashCode {

	public static String MD5Digest(String s) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		byte[] messageDigest = getDigest(s);
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < messageDigest.length; i++) {
			String aux = Integer.toHexString(0xFF & messageDigest[i]);
			if (aux.length() == 1)
				hexString.append('0');
			hexString.append(aux);
		}
		return hexString.toString().toLowerCase();
	}
	
	public static String MD5DigestB64(String s) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		byte[] messageDigest = getDigest(s);
		return new String(Base64.encodeBase64(messageDigest));

	}

	private static byte[] getDigest(String s) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		byte[] str = s.getBytes("ISO-8859-1");
		MessageDigest algorithm = MessageDigest.getInstance("MD5");
		algorithm.reset();
		algorithm.update(str);
		byte messageDigest[] = algorithm.digest();
		return messageDigest;
	}
}
