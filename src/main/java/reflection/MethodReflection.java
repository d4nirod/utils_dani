package reflection;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class MethodReflection {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			List<TestClass> lista = new ArrayList<TestClass>();
			lista.add(new TestClass("pepito", false, 1));
			lista.add(new TestClass("juanito", true, 2));
			lista.add(new TestClass("miguelito", false, 3));
			List<Method> l = new ArrayList<Method>();
			if (lista.size() > 0) {
				Object o = lista.get(0);
				Class<? extends Object> clzz = o.getClass();
				Field[] fields = clzz.getDeclaredFields();
				System.out.print("\t");				
				for (int k = 0; k < fields.length; k++) {
					String attrname = fields[k].getName();
					if ("serialVersionUID".equals(attrname))
						continue;					
					String methodMain = attrname.substring(0, 1).toUpperCase() + attrname.substring(1);
					
					Method m;
					try {
						m = clzz.getMethod("get"+methodMain, new Class[0]);
					} catch (NoSuchMethodException e) {
						if(fields[k].getGenericType().equals(boolean.class))
							m = clzz.getMethod("is"+methodMain, new Class[0]);
						else continue;
					}
					
					if (m==null || m.getParameterTypes().length != 0)
						continue;
					l.add(m);						
					System.out.print(attrname+"\t");										
				}
				System.out.println();
			}
			for (int i = 0; i < lista.size(); i++) {
				Object o = lista.get(i);
				for (Method m : l) {
					try {
						Object value = m.invoke(o, new Object[0]);
						if (value != null) {
							if (m.getReturnType().isArray()) {
								for (Object item : (Object[]) value) {
									System.out.print(item.toString()+"\t");
								}
							} else
								System.out.print(value.toString()+"\t");
						} else
							System.out.println("NULL\t");
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				System.out.println();
			}
		} catch (Exception e) {
		}
	}
}

