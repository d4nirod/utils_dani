package reflection;

public class TestClass {
	
	public TestClass(String stringField, boolean boolField, int intField) {
		super();
		this.stringField = stringField;
		this.boolField = boolField;
		this.intField = intField;
	}
	public String stringField;
	public boolean boolField;
	public int intField;
	
	public String getStringField() {
		return stringField;
	}
	public void setStringField(String stringField) {
		this.stringField = stringField;
	}
	public boolean isBoolField() {
		return boolField;
	}
	public void setBoolField(boolean boolField) {
		this.boolField = boolField;
	}
	public int getIntField() {
		return intField;
	}
	public void setIntField(int intField) {
		this.intField = intField;
	}
}
