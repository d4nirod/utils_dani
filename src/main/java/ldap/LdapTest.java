/*
 *
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 *
 */

package ldap;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class LdapTest {
	private static Logger log = LogManager.getLogger();

	private static DirContext getContext() throws NamingException {
		Properties env = getConfig();
		
		return new InitialDirContext(env);
	}

	private static Properties getConfig() {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, "ldap://localhost:10389");
		env.put(Context.SECURITY_AUTHENTICATION, "none");
		env.put("com.sun.jndi.ldap.connect.timeout", "5000");
		env.put("com.sun.jndi.ldap.read.timeout", "5000");
		// Enable connection pooling
		env.put("com.sun.jndi.ldap.connect.pool", "true");
		env.put("com.sun.jndi.ldap.connect.pool.maxsize","2");
		env.put("com.sun.jndi.ldap.connect.pool.timeout", "5000");
		
		env.put("java.naming.ldap.attributes.binary", "jpegPhoto");
		return env;
	}

	public static void main(String[] args)  {
		System.setProperty("com.sun.jndi.ldap.connect.pool.debug", "fine");
		
		log.debug("Using ldap config: {}", getConfig());
		
		try {
			// ldapsearch -LLL -x -H ldap://lookup.apple.com -b ou=People,o=Apple
			// mail=darya@apple.com applePhotoPreferred-jpeg givenName appleLastName
			// dn: appledsId=1276796949,ou=People,o=apple

			LdapTest test = new LdapTest();
			
//			byte[] image1 = test.readFile("push-message-icon.jpg");
//			byte[] image2 = test.readFile("sauron-eye-small.jpeg");
//			byte[] image3 = test.readFile("suicide-emoji.jpg");
//			byte[] image4 = test.readFile("byte-array.jpg");
	
//			test.createUser("00001", "Joe",   "Blogs", "joe.bloggs@apple.com",  new String(Base64.getEncoder().encode(image1)));
//			test.createUser("00002", "John",  "Smith", "john.smith@apple.com",  new String(Base64.getEncoder().encode(image2)));
//			test.createUser("00003", "Fred", "Public", "fred.public@apple.com", new String(Base64.getEncoder().encode(image3)));
			
//			test.createUser("00004", "Zid",  "Tester", "sid@t.com", new String(Base64.getEncoder().encode(image2)));
//			test.createUser("00005", "Frank", "Private", "frank.private@t.com", new String(Base64.getEncoder().encode(image3)));
//			test.createUser("00006", "Zimon", "Boss", "simont@t.com", new String(Base64.getEncoder().encode(image4)));
	
//			System.out.println("==============================================================\n");
	
//			NamingEnumeration<SearchResult> answer = test.findByEmail("john.smith@apple.com");
//			test.printSearchEnumeration(answer);
			
			String[] attributesToReturn = { "givenName", "surname", "sn", "mail", "jpegPhoto", "dn" };
			
			CompletableFuture<Void> future1 = CompletableFuture.supplyAsync(() -> { 
				try {
					return test.findByEmail2("john.smith@apple.com", attributesToReturn);
				} catch (NamingException e) {
					 throw new CompletionException(e);
				}
			}).thenAccept(r -> {
				try {
					test.printSearchEnumeration2(r, attributesToReturn);
					r.close();
				} catch (NamingException e) {
					 throw new CompletionException(e);
				}
			});
			 
//			.thenAccept(r -> test.printSearchEnumeration2(r, attributesToReturn));
			
//			CompletableFuture.supplyAsync(() -> {
//				try {
//					NamingEnumeration<SearchResult> answer2 = test.findByEmail2("john.smith@apple.com", attributesToReturn);
//					test.printSearchEnumeration2(answer2, attributesToReturn);
//					answer2.close();
//				} catch (NamingException e) {
//					log.error("Error", e);
//					System.exit(1);
//				}
//				return null;
//			});
//			CompletableFuture.supplyAsync(() -> {
//				try {
//					NamingEnumeration<SearchResult> answer2 = test.findByEmail2("joe.bloggs@apple.com", attributesToReturn);
//					test.printSearchEnumeration2(answer2, attributesToReturn);
//					answer2.close();
//				} catch (NamingException e) {
//					log.error("Error", e);
//					System.exit(1);
//				}
//				return null;
//			});
//			
//			CompletableFuture.supplyAsync(() -> {
//				try {
//					NamingEnumeration<SearchResult> answer2 = test.findByEmail2("fred.public@apple.com", attributesToReturn);
//					test.printSearchEnumeration2(answer2, attributesToReturn);
//					answer2.close();
//				} catch (UnsupportedEncodingException | NamingException e) {
//					log.error("Error", e);
//					System.exit(1);
//				}
//				return null;
//			});
//			
//			CompletableFuture.supplyAsync(() -> {
//				try {
//					NamingEnumeration<SearchResult> answer2 = test.findByEmail2("simont@t.com", attributesToReturn);
//					test.printSearchEnumeration2(answer2, attributesToReturn);
//					answer2.close();
//				} catch (UnsupportedEncodingException | NamingException e) {
//					log.error("Error", e);
//					System.exit(1);
//				}
//				return null;
//
//			});
//	
//			LdapTest.deleteUser("00001");
//			LdapTest.deleteUser("00002");
//			LdapTest.deleteUser("00003");
			CompletableFuture.allOf(future1).get();
		} catch (Exception e) {
			log.error("Error", e);
			System.exit(1);
		}
	}
	
	private NamingEnumeration<SearchResult> findByEmail2(String email, String[] attributesToReturn) throws NamingException {
		String filter = String.format("(mail=%s)", email);
		SearchControls ctls = new SearchControls();
		ctls.setTimeLimit(1000);
		ctls.setCountLimit(1);
		
		// Specify the ids of the attributes to return
		
		ctls.setReturningAttributes(attributesToReturn);
		ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		System.out.println(String.format("Seaching for attribute: %s, value: %s", "mail", email));
		
		// Search for objects using the filter
		DirContext context = getContext();
		
		NamingEnumeration<SearchResult> result = context.search("ou=People,o=Apple,dc=partition1,dc=com", filter, ctls);
		
		// Close the context when we're done
//		context.close();
		return result;
	}

	private void printSearchEnumeration2(NamingEnumeration<SearchResult> enumeration, String[] attributesToReturn) throws NamingException {
		while(enumeration.hasMoreElements()) {
			SearchResult searchResult = enumeration.nextElement();
			System.out.println("Search result: " + searchResult.getNameInNamespace());
			Attributes attributes = searchResult.getAttributes();
			for (String attrId : attributesToReturn) {
				Object value = attributes.get(attrId) != null ? attributes.get(attrId).get() : null;
				if (value == null) {
					continue;
				}	
				List<String> values = new ArrayList<>();
				System.out.print("\tAttribute: " + attrId);
				String valueStr = null;
				if (value instanceof String) {
					valueStr = (String) value;
				} else if (value instanceof byte[]) {
					valueStr = new String(Base64.getEncoder().encode((byte[]) value), StandardCharsets.UTF_8);
				} else {
					// log warning
				}
				values.add(valueStr);
				System.out.println("\tValues: " + StringUtils.joinWith(", ", values));
			}
		}
	}

	private byte[] readFile(String filename) throws URISyntaxException, IOException {
		Path path = Paths.get(getClass().getClassLoader().getResource(filename).toURI());
		return Files.readAllBytes(path);
	}
	
	private NamingEnumeration<SearchResult> findByEmail(String email) throws NamingException {
		// Specify the attributes to match
		// Ask for objects with the surname ("sn") attribute
		// with the value "Smith"
		// and the "mail" attribute.
		// ignore case
		
		Map<String, Object> filterArgs = new HashMap<>();
		String searchAttrId = "mail";
		
		Attributes matchAttrs = new BasicAttributes(true);
	//				matchAttrs.put(new BasicAttribute(searchAttrId, searchAttrValue));
	
		filterArgs.forEach(matchAttrs::put);
	
		System.out.println(String.format("Seaching for attribute: %s, value: %s", searchAttrId, email));
	
		// Search for objects that have those matching attributes
	    DirContext ctx = getContext();
	
	    // Create the default search controls
	//			    SearchControls ctls = new SearchControls();
	    // SearchControls.setTimeLimit()
	
	    // Specify the ids of the attributes to return
	    String[] attrIDs = {"givenName", "sn", "mail", "jpegPhoto", "dn", "applePhotoPreferred-jpeg", "appleLastName"};
	//				NamingEnumeration<SearchResult> answer = ctx.search("ou=People,o=Apple,dc=partition1,dc=com", matchAttrs, attrIDs);
		NamingEnumeration<SearchResult> result = ctx.search("ou=People,o=Apple,dc=partition1,dc=com", matchAttrs, attrIDs);
		// Close the context when we're done
		ctx.close();
		return result;
	}

	private void printSearchEnumeration(NamingEnumeration<SearchResult> enumeration) throws NamingException, UnsupportedEncodingException {
		while(enumeration.hasMoreElements()) {
			SearchResult searchResult = enumeration.next();
			System.out.println("Search result: " + searchResult.getNameInNamespace());
			Attributes attributes = searchResult.getAttributes();
			NamingEnumeration<? extends Attribute> allAttrs = attributes.getAll();

			while (allAttrs.hasMoreElements()) {
				Attribute attr = allAttrs.next();
				System.out.print("\tAttribute: " + attr.getID());
				// print each value
				NamingEnumeration<?> e = attr.getAll();
				List<String> values = new ArrayList<>();
				while (e.hasMore()) {
					Object value = e.next();
					String valueStr = null;
					
					if (value instanceof String) {
						valueStr = (String) value;
					} else if (value instanceof byte[]) {
						valueStr = new String(Base64.getEncoder().encode((byte[]) value), StandardCharsets.UTF_8);
					} else {
						// log warning
					}
					values.add(valueStr);
				}
				System.out.println("\tValues: " + StringUtils.joinWith(", ", values));
			}
		}

	}
	
	//----------------- USER CRUD METHODS -------------------------//
	
	private static void createUser(String employeeNumber, String givenName, String surname, String mail, String image) throws NamingException, UnsupportedEncodingException {
		DirContext context = getContext();
		String name = "employeeNumber=" + employeeNumber + ",ou=People,o=Apple,dc=partition1,dc=com";
		createLDAPObject(context, name);
	
		updateAttribute(context, name, "cn", givenName);
		createAttribute(context, name, "givenName", givenName);
		updateAttribute(context, name, "sn", surname);
		createAttribute(context, name, "displayName", givenName + " " + surname);
		createAttribute(context, name, "mail", mail);
	    byte[] decodedImage = Base64.getDecoder().decode(image.getBytes(StandardCharsets.ISO_8859_1));
		createAttribute(context, name, "jpegPhoto", decodedImage);
	
		System.out.println("=== Created user:");
		viewAttribute(context, name, "givenName");
		viewAttribute(context, name, "sn");
		viewAttribute(context, name, "displayName");
		viewAttribute(context, name, "mail");
		viewAttribute(context, name, "jpegPhoto");
	}
	private static void deleteUser(String employeeNumber) throws NamingException {
		String name = "employeeNumber=" + employeeNumber + ",ou=People,o=Apple,dc=partition1,dc=com";
		removeLDAPObject(getContext(), name);
	}
	private static void removeLDAPObject(DirContext context, String name) throws NamingException {
		context.destroySubcontext(name);
	}
	private static void createLDAPObject(DirContext context, String name) throws NamingException {
			Attributes attributes = new BasicAttributes();
	
			Attribute attribute = new BasicAttribute("objectClass");
			attribute.add("inetOrgPerson");
	
			Attribute sn = new BasicAttribute("sn");
			sn.add("..");
			attributes.put(sn);
	
			Attribute cn = new BasicAttribute("cn");
			cn.add("..");
			attributes.put(cn);
	
	//		attributes.put("telephoneNumber", "123456");
	
			attributes.put(attribute);
			context.createSubcontext(name, attributes);
		}
	private static void removeAttribute(DirContext context, String name, String attrName) throws NamingException {
		Attribute attribute = new BasicAttribute(attrName);
		ModificationItem[] item = new ModificationItem[1];
		item[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, attribute);
		context.modifyAttributes(name, item);
	}
	private static void createAttribute(DirContext context, String name, String attrName, Object attrValue)
			throws NamingException {
		Attribute attribute = new BasicAttribute(attrName, attrValue);
		ModificationItem[] item = new ModificationItem[1];
		item[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE, attribute);
		context.modifyAttributes(name, item);
	}
	private static void updateAttribute(DirContext context, String name, String attrName, Object attrValue)
			throws NamingException {
		Attribute attribute = new BasicAttribute(attrName, attrValue);
		ModificationItem[] item = new ModificationItem[1];
		item[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, attribute);
		context.modifyAttributes(name, item);
	}
	private static void viewAttribute(DirContext context, String name, String attrName) throws NamingException {
		Attributes attrs = context.getAttributes(name);
		Object value = attrs.get(attrName).get();
		String valueStr = value instanceof String ? (String) value : new String(Base64.getEncoder().encode((byte[]) value), StandardCharsets.UTF_8);
		System.out.println(attrName + ":" + valueStr);
	}
	

}
