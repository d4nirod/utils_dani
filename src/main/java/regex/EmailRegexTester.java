package regex;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class EmailRegexTester {

	public static void main(String[] args) {
		Pattern regex = Pattern.compile("^" + Pattern.quote("null-user.") + "[A-Za-z0-9]{8}" + Pattern.quote("@cirkus.com") + "$", Pattern.CASE_INSENSITIVE);
		
		List<String> emails = Arrays.asList("null-user.KL2tn3OV@cirkus.com", "null-user.VUqqUKgR@cirkus.com", "null-user.Hr91notW@cirkus.com");
		emails.forEach(e -> System.out.println(e + " : " + regex.matcher(e).matches()));

	}

}
