package regex;

public class NegateRegex {

	/**
	 * No funciona. negative look ahead
	 * @param args
	 */
	public static void main(String[] args) {
		String s[] = {"a", " ", "", "11", "4", "1", "2", "3"};
		String rgx = "^[1-3]$";
		for(int i=0; i<s.length; i++)
			System.out.println("'"+s[i] + "' matches '"+rgx+"' : "+s[i].matches(rgx));
		System.out.println("------------------------------------");
		//Negate
		rgx = "(?![1-3]).{1}";
		for(int i=0; i<s.length; i++)
			System.out.println("'"+s[i] + "' does NOT match '"+rgx+"' : "+s[i].matches(rgx));
		
		
	}

}
