package regex;

import java.util.regex.Pattern;

public class NifTelEmailRegexTester {

	public static Pattern[] PATTERNS = new Pattern[] { Pattern.compile("^\\d{9,}$"), Pattern.compile("^.*@.*$"), Pattern.compile("^[A-Z]?[- ]?\\d{7,9}[- ]?[A-Z]$")};

	public static void main(String[] args) {
		String strings[] = new String[] {"123456789Z", "123456789-Z", "X12345678Z", "asdf@asdf.com", "", "606202020", "34971455889"};

		for(int i=0; i<strings.length; i++){
			String pattern = "none";
			for(int j=0; j< PATTERNS.length; j++){
				if(PATTERNS[j].matcher(strings[i]).matches()){
					pattern = PATTERNS[j].pattern();
					break;
				}
			}
			System.out.println("["+strings[i]+"] matches: "+pattern);
		}
	}

}
