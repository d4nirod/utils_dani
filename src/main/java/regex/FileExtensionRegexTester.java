package regex;

import java.util.Arrays;
import java.util.List;

public class FileExtensionRegexTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> lst = Arrays.asList("adfas.bat", "adfasd.exe", "asdfasdf.asdfasd.pdf", "ad  sf asd.doc", "asdfasd.xlsx", "asdfas.txt", "asdfasdf.jpg", "adsfasdf.png", ".txt");
		
		for(String s : lst)
			System.out.printf("'%s'\t\t\t: %s\n", s, s.matches(".+\\.(zip|rar|7z|pdf|gif|jpe?g|png|tiff?|bmp|txt|csv|html?|rtf|docx?|xlsx?|pptx?|odt?s?p?)$"));
//			System.out.printf("'%s'\t\t\t: %s\n", s, s.matches(".+\\.(pdf|jpg|txt)$"));
	}

}
