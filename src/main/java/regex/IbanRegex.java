package regex;

import java.util.Arrays;
import java.util.List;

public class IbanRegex {

	public static void main(String[] args) {
		List<String> l = Arrays.asList(
				new String[]{	"GR16 0110 1250 0000 0001 2300 695",
								"GB29 NWBK 6016 1331 9268 19",
								"SA03 8000 0000 6080 1016 7519",
								"CH93 0076 2011 6238 5295 7",
								"IBAN ES76 2077 0024 0031 0257 5766",
								"ES7620770024003102575766"
							});
		for(String s : l)
			System.out.printf("%s : %s\n", s, s.replaceAll(" ", "").toUpperCase().matches("^(:?IBAN){0,1}\\p{Alpha}{2}\\d{2}\\p{Alnum}{1,30}$"));

	}

}
