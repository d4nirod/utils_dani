package regex;

import java.util.List;
import java.util.Arrays;
public class IntegerRegexTester {

	public IntegerRegexTester() {
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> lst = Arrays.asList("", "-123","4r", "a", " ", "1", " 1234",  "12345678", "1234567");

		for(String s : lst)
			System.out.printf("'%s'\t\t\t: %s\n", s, s.matches("^\\d{1,7}$"));

	}

}
