/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package regex;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class InlineFileTagsReplaceRegexTester {
	
	private static final String HTML_REPLACEMENT = String.format("<img src='%s' class='cw-img-icon' height='16' width='16'/>$3 ", "/assets/email/Paperclip@2x.png");

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// \!\[[^\]]*\]\((5d3ed2de2bfd306876126e77|5d3ed29b2bfd306876126df1|5d3ed6f02bfd3068761272ae) \"([^"]*)"\)
		// \\!\\[[^\\]]*\\]\\((5d3ed2de2bfd306876126e77|5d3ed29b2bfd306876126df1|5d3ed6f02bfd3068761272ae) \\"([^"]*)"\\)
		
		// \!\[[^\]]*\]\((5d3ed2de2bfd306876126e77|5d3ed29b2bfd306876126df1|5d3ed6f02bfd3068761272ae) \"([^\"]*)\"\)
		// " \\!\\[[^\\]]*\\]\\((5d3ed2de2bfd306876126e77|5d3ed29b2bfd306876126df1|5d3ed6f02bfd3068761272ae) \\"([^\\"]*)\\"\\) "
		
		Pattern matchPattern = Pattern.compile(
				".*?\\!\\[[^\\]]*\\]\\((?:5d3ed2de2bfd306876126e77|5d3ed29b2bfd306876126df1|5d3ed6f02bfd3068761272ae|5d3ed6f02bfd3068761272b1|5d3ed6f02bfd3068761272b2) \\\"([^\\\"]*)\\\"\\).*?",
				Pattern.DOTALL);

		List<String> l = Arrays.asList(
			"![theImage.jpg](5d3ed2de2bfd306876126e77 \"theImage.jpg\")",
			"prependedbytext![theImage.jpg](5d3ed2de2bfd306876126e77 \"theImage.jpg\")",
			"![some image.jpg](5d3ed29b2bfd306876126df1 \"some image.jpg\")followed by text",
			"@John Doe A multiline message\n![image-one.png](5d3ed6f02bfd3068761272ae \"image-one.png\") with an inline pic.",
			"@John Doe A multiline message\nprependedstuck![image-one.png](5d3ed6f02bfd3068761272ae \"image-one.png\")stuck with an inline pic.",
			"@John Doe A multiline message\n![image-one.png](5d3ed6f02bfd3068761272ae \"image-one.png\")stuck with an inline pic.",
			"@John Doe Another multiline message\n![image-two.png](5d3ed6f02bfd3068761272b1 \"image-two.png\") blah." + 
					"\nLorem ipsum another image ![image-three.png](5d3ed6f02bfd3068761272b2 \"image-three.png\")bleh."
			);
		
		l.forEach(s -> System.out.println(s + " : Matches: " + matchPattern.matcher(s).matches() + "\n"));
		
		System.out.println();
		System.out.println("****** REPLACEMENT");

		Pattern replacePattern = Pattern.compile(
				"\\!\\[[^\\]]*\\]\\((?:5d3ed2de2bfd306876126e77|5d3ed29b2bfd306876126df1|5d3ed6f02bfd3068761272ae|5d3ed6f02bfd3068761272b1|5d3ed6f02bfd3068761272b2|5d3ed6f02bfd3068761272b2) \\\"([^\\\"]*)\\\"\\) ?.*?",
				Pattern.DOTALL);
		l.forEach(s -> {
			Matcher matcher = replacePattern.matcher(s);
//			StringBuffer sb = new StringBuffer(s.length());
//			while (matcher.find()) {
//				String fileName = matcher.group(1);
////				matcher.appendReplacement(sb, Matcher.quoteReplacement(fileName));
//				matcher.appendReplacement(sb, fileName);
//			}
//			matcher.appendTail(sb);

			StringBuffer sb = new StringBuffer();
	        boolean result = matcher.find();
			if (result) {
	            do {
//	            	StringBuffer sb = new StringBuffer();
	            	String fileName = matcher.group(1);
	                matcher.appendReplacement(sb, fileName);
	                result = matcher.find();
	            } while (result);
	            matcher.appendTail(sb);
	           // return sb.toString();
	            System.out.println(s + " ->\n\t" + sb.toString() + "\n");
	        }

		});
		
		System.out.println();
		System.out.println("****** REPLACEMENT II");
		l.forEach(s -> System.out.println(s + " ->\n\t" + replacePattern.matcher(s).replaceAll("$1 ") + "\n"));
		
		System.out.println();
		System.out.println("****** REPLACEMENT III");
		int[] codepoint = {0x1F4CE};
		String clipEmoji = new String(codepoint, 0, codepoint.length);
		l.forEach(s -> System.out.println(s + " ->\n\t" + replacePattern.matcher(s).replaceAll(clipEmoji + "($1) ") + "\n"));
		
//		Pattern replacePatternHtml = Pattern.compile(
//				"\\!\\[[^\\]]*\\]\\((?:5d3ed2de2bfd306876126e77|5d3ed6f02bfd3068761272b1|5d3ed6f02bfd3068761272b2) &quot;([^\\\"]*)&quot;\\)",
//				Pattern.DOTALL);
		Pattern replacePatternHtml = Pattern.compile(
//				"\\!\\[[^\\]]*\\]\\((?:5d3ed2de2bfd306876126e77|5d3ed6f02bfd3068761272b1|5d3ed6f02bfd3068761272b2) &quot;([^\\\"]*)&quot;\\)",
				"((?:<br\\/>)*)(\\s*)\\!\\[[^\\]]*\\]\\((?:5d3ed2de2bfd306876126e77|5d3ed29b2bfd306876126df1|5d3ed6f02bfd3068761272ae|5d3ed6f02bfd3068761272b2|5d3ed6f02bfd3068761272b1|5d3ed6f02bfd3068761272b2) &quot;((?!&quot;).*?)&quot;\\)(\\s*)");
//		String source = "@John Doe Another multiline message\n![image-two.png](5d3ed6f02bfd3068761272b1 \"image-two.png\") blah." + 
//				"\nLorem ipsum another image ![image-three.png](5d3ed6f02bfd3068761272b2 \"image-three.png\")bleh.";
		System.out.println();
		System.out.println("****** REPLACEMENT IV");
		
//		System.out.println(source + " ->\n\t" + replacePatternHtml.matcher(source).replaceAll(clipEmoji + "($1) ") + "\n");
		l.forEach(s -> {
			String escapedSource = HtmlUtils.formatToMultiLineHtmlText(s);
			
			StringBuffer sb = new StringBuffer();
			Matcher matcher = replacePatternHtml.matcher(escapedSource);
			boolean result = matcher.find();
			if (result) {
				do {
	//            	StringBuffer sb = new StringBuffer();
					String br = matcher.group(1);
					String whitespaceBefore = matcher.group(2);
					String fileName = matcher.group(3);
					String whitespaceAfter = matcher.group(4);
	
					String prefix = null;
					StringBuffer replacement = new StringBuffer();
					if (StringUtils.isNotEmpty(br)) {
						replacement.append(br);
					} else if (StringUtils.isEmpty(whitespaceBefore) && matcher.start() != 0) {
							replacement.append(" ");
					} else {
						replacement.append(whitespaceBefore);
					}
					replacement.append("📎" + fileName);
					
					if (StringUtils.isEmpty(whitespaceAfter)) {
						replacement.append(" ");
					} else {
						replacement.append(whitespaceAfter);
					}
					
					matcher.appendReplacement(sb, replacement.toString());
					result = matcher.find();
				} while (result);
				matcher.appendTail(sb);
				// return sb.toString();
			}
			System.out.println("Source: " + s + "\n\tEscaped source: " + escapedSource + " ->\n\t[" + sb.toString() + "]\n");
		});
		

	}
	
	public static class HtmlUtils {
		private HtmlUtils() {
		}
		
		public static String formatToMultiLineHtmlText(String str) {
			if (StringUtils.isEmpty(str)) {
				return "";
			}
			str = escapeHtml(str);
			str = str.replace("\n\r", "\n");
			str = str.replace("\r\n", "\n");
			str = str.replace("\r", "\n");
			str = str.replace("\n", "<br/>");
			
			return str;
		}

		private static String escapeHtml(String s) {
		    return s.replaceAll("&", "&amp;").
		    		replaceAll(">", "&gt;").
		    		replaceAll("<", "&lt;").
		    		replaceAll("\"", "&quot;").
		    		replaceAll("'", "&apos;");
		}
	}

}
