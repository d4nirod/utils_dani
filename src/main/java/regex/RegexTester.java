package regex;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Pattern patron = Pattern.compile("(\\d{3})(\\d{2}).(\\d{4})(\\d{3})(\\d{7})");
							
			String[] claveContratoSips = {
					"50252000050100000019",
					"50252000050200000040",
					"50252000050200000043",
					"50252000050200000044",
					"50252000050200000057",
					"50252000050300000025",
					"50252002520100000191",
					"50252002520100000194",
					"50252002520100000322",
					"50252002520100000323",
					"50252002520100000598",
					"50252002520100001153",
					"50252002520100002078",
					"50252002520100002106",
					"50252002520100002252"
			};
			System.out.println("Regex pattern: "+patron.pattern());
			for(int i=0; i< claveContratoSips.length; i++){
				Matcher matcher = patron.matcher(claveContratoSips[i]);
				if(matcher.find()) {
					String apli = matcher.group(1);
					String emp = matcher.group(2);
					String cen = matcher.group(3);
					String afi = matcher.group(4);
					String con = matcher.group(5);
					
					System.out.println(apli + " "+  emp + " " + cen +" " + afi + " " +con);
				}
			}
	}
}
