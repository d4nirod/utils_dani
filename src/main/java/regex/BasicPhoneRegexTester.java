package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BasicPhoneRegexTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// \+?\d+
		Pattern patron = Pattern.compile("\\+?\\d+");
		
		String[] tels = {
				"+34971777432",
				"606101010",
				"23asdf3qwert534",
				"6062221100"			
		};
		for(int i=0; i< tels.length; i++){
			Matcher matcher = patron.matcher(tels[i]);			
			String match = "NOK";
			if(matcher.find())
				match = matcher.group(0);											
			System.out.printf("String '%s' : Match '%s'\n",  tels[i], match);
		}

	}

}
