/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package regex;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JUnitParamsMethodNameTester {
//	private static final Pattern JUNIT_PARAMS_METHOD_PATTERN = Pattern.compile("([^\\(]+)(\\([^\\)]*.*)");
	private static final Pattern JUNIT_PARAMS_METHOD_PATTERN = Pattern.compile("^([^(]+)\\(.*$");

	public static void main(String[] args) {
		List<String> strings = Arrays.asList(
				"testRemoveCommentMentionsForUser(TASK) [0]", 
				"testRemoveCommentMentionsForUser(TASK, BLA) [0]", 
				"testRemoveCommentMentionsForUser(TASK, BLA) [0] [1]", 
				"testRemoveCommentMentionsForUser"
		);
		
		for (String str : strings) {
			Matcher matcher = JUNIT_PARAMS_METHOD_PATTERN.matcher(str);
			if (matcher.find()) {
				System.out.println("Match!: " + str + ": " + matcher.group(1));
			} else {
				System.out.println("No match!: " + str);
			}

		}
		
		
	}

}
