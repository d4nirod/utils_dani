package regex;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CardRegexTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Pattern patron = Pattern.compile("(\\b(\\d{4})([ -])?(\\d{4})([ -])?(\\d{4})([ -])?(\\d{4})\\b)(?: - )?");
		String[] cardNums = {
				"5489 1500 1111 0006 - adsfasd asdfasdf",
				"5489 1500 1111 0204",
				"5489 1500 1111 0303",
				"6769 2300 1111 0401",
				"6769 2300 1111 0500",
				"6769 2300 1111 0609",
				"6769 2300 1111 0708",
				"5489 1500 1129 6813",
				"5489 1500 1129 9221",
				"5489 1500 2115 4119"
		};
		System.out.println("Regex pattern: "+patron.pattern());
		for(int i=0; i< cardNums.length; i++){
			Matcher matcher = patron.matcher(cardNums[i]);
			if(matcher.find()) {
				List<String> matches = new ArrayList<String>(8);
				int j=0;
				for(j=0; j<9; j++)
					matches.add(matcher.group(j));
				j=0;
				for(String s:matches)
					System.out.println("Group "+j++ + ": '"+s+"'");
			}
		}
	}
}
