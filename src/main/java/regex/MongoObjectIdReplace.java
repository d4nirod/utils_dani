package regex;

import org.apache.commons.lang3.StringUtils;

public class MongoObjectIdReplace {

	public static void main(String[] args) {
		String source = "{ \"_id\": ObjectId(\"58f5fb52300496eba1b53b31\"), \"className\": \"com.farmerswife.cloudwife.entities.CWTask\", \"name\": \"Standalone task\", \"createdDate\": ISODate(\"2017-04-18T11:41:06.399+0000\"), \"deleted\": false, \"doneBehavior\": \"ANY\", \"deletedInParent\": false, \"createdByUserId\": ObjectId(\"577e7f08300488c6676b33f5\"), \"assignees\": [ 	DBRef(\"cw_user\", ObjectId(\"58f72eec30044ac8d6cf3e78\")) ], \"done\": false, \"fileIds\": [ ], \"canceled\": false, \"assignedByUserIds\": [ 	ObjectId(\"577e7f08300488c6676b33f5\") ], \"favoriteIds\": [ ], \"mentionedUserIds\": [ ], \"mentionedRoleIds\": [ ], \"modifiedTime\": ISODate(\"2017-04-19T12:18:36.992+0000\") }";
			
		String objectIdPunc = "\\p{Punct}[a-f\\d]{24}\\p{Punct}";
		System.out.println(source);
		System.out.println(StringUtils.replaceAll(source, objectIdPunc, "\"XXX\""));
	}

}
