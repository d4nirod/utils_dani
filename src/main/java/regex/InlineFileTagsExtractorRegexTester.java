/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package regex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class InlineFileTagsExtractorRegexTester {
	public static void main(String[] args) {
		List<String> l = Arrays.asList(
			"![theImage.jpg](5d3ed2de2bfd306876126e77 \"theImage.jpg\")",
			"prependedbytext![theImage.jpg](5d3ed2de2bfd306876126e77 \"theImage.jpg\")",
			"![some image.jpg](5d3ed29b2bfd306876126df1 \"some image.jpg\")followed by text",
			"@John Doe A multiline message\n![image-one.png](5d3ed6f02bfd3068761272ae \"image-one.png\") with an inline pic.",
			"@John Doe A multiline message\nprependedstuck![image-one.png](5d3ed6f02bfd3068761272ae \"image-one.png\")stuck with an inline pic.",
			"@John Doe A multiline message\n![image-one.png](5d3ed6f02bfd3068761272ae \"image-one.png\")stuck with an inline pic.",
			"@John Doe Another multiline message\n![image-two.png](5d3ed6f02bfd3068761272b1 \"image-two.png\") blah." + 
					"\nLorem ipsum another image ![image-three.png](5d3ed6f02bfd3068761272b2 \"image-three.png\")bleh.",
			"![ ](5d3ed29b2bfd306876126df1  \" \")",
			"![](5d3ed29b2bfd306876126df1  \"\")"
		);
		
		String regex1 = "!\\[[^\\]]+]\\(([^)\\s]+)[^)]+\\)";
		System.out.println("********** regex 1: " + regex1);
		Pattern extractPattern = Pattern.compile(regex1, Pattern.DOTALL);
		l.forEach(s -> {
			Matcher matcher = extractPattern.matcher(s);
			List<String> matches = new ArrayList<>();
			while(matcher.find()) {
	            matches.add(matcher.group(1));
	        }
			System.out.println(s + " ->\n\t" + StringUtils.joinWith(", ", matches) + "\n");
		});
		
		String regex2 = "!\\[(?!\\s*])[^]]+]\\((\\p{Alnum}+)\\s+\\\"(?!\\s*\")[^\"]+\\\"\\)";
		
		System.out.println();
		System.out.println("********** regex 2: " + regex2);
		Pattern extractPattern2 = Pattern.compile(regex2, Pattern.DOTALL);
		l.forEach(s -> {
			Matcher matcher = extractPattern2.matcher(s);
			List<String> matches = new ArrayList<>();
			while(matcher.find()) {
				matches.add(matcher.group(1));
			}
			System.out.println(s + " ->\n\t" + StringUtils.joinWith(", ", matches) + "\n");
		});
		
	}
}
