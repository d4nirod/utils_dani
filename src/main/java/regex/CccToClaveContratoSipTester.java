package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CccToClaveContratoSipTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//(0061)(\d{4})(\d{2})(\d{6})(\d{3})
		Pattern patron = Pattern.compile("(\\d{4})(\\d{4})(\\d{2})(\\d{6})(\\d{3})(\\d)");
		//CCC: EEEEOOOODDNNNNNNNNNN
		// Entidad, Oficina, Digito control, N*mero cuenta
		String ccc = "00612222334444447778";
		Matcher matcher = patron.matcher(ccc);
		if(matcher.find()){
			String ent = matcher.group(1);
			String ofi = matcher.group(2);
			String dc  = matcher.group(3);
			String cbeg = matcher.group(4);
			String cend = matcher.group(5);
			String last = matcher.group(6);
			System.out.printf("ent: %s ofi: %s dc: %s cend: %s cbeg: %s last: %s", ent, ofi, dc, cend, cbeg, last);
		}

	}

}
