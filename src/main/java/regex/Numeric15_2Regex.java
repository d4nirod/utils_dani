package regex;

import java.util.Arrays;
import java.util.regex.Pattern;

public class Numeric15_2Regex {

	public static void main(String[] args) {
		String rgx = "^\\d{1,13}(?:[.,']\\d{0,2})?$";
		Pattern p = Pattern.compile(rgx);
		String nums[] = {
				"1",
				"12",
				"123",
				"1234",
				"12345",
				"123456",
				"1234567",
				"12345678",
				"123456789",
				"1234567890",
				"12345678901",
				"123456789012",
				"1234567890123",
				"12345678901234",
				"123456789012.1",
				"1234567890123.1",
				"123456789012.12",
				"1234567890123.12",
				"1234567890123,12",
				"1234567890123,12,234",
				"1234567890123'12"
		};

		for(String n : Arrays.asList(nums))
			System.out.println(String.format("\"%s\" : %s", n, p.matcher(n).matches()));
	}

}
