package regex;

import java.util.Arrays;
import java.util.List;

public class OneDigit123OrFiveNinesRegexTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> list = Arrays.asList("1", "2", "3", "4", "55", "a", "bb", "99999", "11", "222", "33333");
		for(String s:list){
			System.out.printf("%s : %s\n", s, s.matches("^[123]|99999$"));
		}
	}

}
