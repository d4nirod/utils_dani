package regex.aeat;

public class FicheroRespuestaAEAT {
	private String nif;
	private String ejercicio;
	private String modelo;
	private String fecha;
	private String resultado;
	private String link;
	
	public FicheroRespuestaAEAT(String nif, String ejercicio, String modelo, String fecha, String resultado, String link) {
		this.nif = nif;
		this.ejercicio = ejercicio;
		this.modelo = modelo;
		this.fecha = fecha;
		this.resultado = resultado;
		this.link = link;
	}
	public String getNif() {
		return nif;
	}
	public void setNif(String nif) {
		this.nif = nif;
	}
	public String getEjercicio() {
		return ejercicio;
	}
	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
}
