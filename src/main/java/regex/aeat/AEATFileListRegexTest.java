package regex.aeat;

import java.io.File;
import java.io.FileFilter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.filefilter.RegexFileFilter;


public class AEATFileListRegexTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AEATFileListRegexTest.getFileData();
	}
	
	public static void simpleTest(){
		List<String> list = new ArrayList<String>(
				Arrays.asList(	"c:/tmp/CAdES/declaraciones-web/tmp/12763342K_2011_100_004916_ok_00.html", 
								"c:/tmp/CAdES/declaraciones-web/tmp/12763342K_2011_100_004916_ok_01.html", 
								"c:/tmp/CAdES/declaraciones-web/tmp/12763342K_2011_100_004916_ok_02.html", 
								"c:/tmp/CAdES/declaraciones-web/tmp/12763342K_2011_100_004916_ok_03.html"));
		String pathsep = Pattern.quote(File.separator);
		String regex = "(.*)_\\d{4}_\\d{3}_(004916)_(ok)_\\d{2}.html";
		Pattern p = Pattern.compile(regex);
		for(String s: list){
			Matcher m = p.matcher(s);
			while(m.find())
				System.out.printf("%s   %s  %s\n", m.group(1), m.group(2),m.group(3));
				
		}		
	}
	
	public static void getFileData(){
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String path = "c:\\tmp\\CAdES\\declaraciones-web\\";
		File dir = new File(path);
		String usuario = "004916";
		
		String regex = String.format("([^_]*)_(\\d{4})_(\\d{3})_%s_(ok|err)_(\\d{2}).html", usuario);
		FileFilter filter = new RegexFileFilter(regex);
		File[] filelist = dir.listFiles(filter);
		
		if(filelist.length>0){
			StringBuffer urlBegin = new StringBuffer("<a href=\"XTransManager?tr=OP");
			urlBegin.append("&num=").append("3065");
			urlBegin.append("&PA=").append("5");
			urlBegin.append("&idioma=").append("0");
			
			StringBuffer urlEnd = new StringBuffer("\" target=\"_blank\">").append("Ver").append("</a>");
			
			List<FicheroRespuestaAEAT> ficherosAeat = new ArrayList<FicheroRespuestaAEAT>(filelist.length);			
			String name, nif, ejercicio, modelo, id, fecha, resultado, link;
			StringBuffer urlMid;
			Pattern p = Pattern.compile(regex);
			for(int i=0; i<filelist.length; i++) {				
				name = filelist[i].getName();
				Matcher m = p.matcher(name);
				m.find();
				nif = m.group(1);						
				ejercicio = m.group(2);
				modelo = m.group(3);
	//			usuario = m.group(X);
				resultado = m.group(4);
				id = m.group(5);
				fecha = dateFormat.format(new Date(filelist[i].lastModified()));
				urlMid = new StringBuffer(urlBegin.toString());
				urlMid.append("&nif=").append(nif).append("&ejercicio=").append(ejercicio);
				urlMid.append("&modelo=").append(modelo).append("&id=").append(id);			
				urlMid.append(urlEnd.toString());
				link = urlMid.toString();			
				ficherosAeat.add(new FicheroRespuestaAEAT(nif, ejercicio, modelo, fecha, resultado, link));
			}
			for(FicheroRespuestaAEAT f : ficherosAeat){
				System.out.printf("NIF: %s, EJR: %s, MOD: %s, FECHA: %s, RES: %s, LNK: %s\n", f.getNif(), 
						f.getEjercicio(), f.getModelo(), f.getFecha(), f.getResultado(), f.getLink());
			}
		}else{
			System.out.println("No hay ficheros.");
		}
	}

}
