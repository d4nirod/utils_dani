package regex;

public class AlfanumCharClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s = "12345678Z";
		String alumTrue = "^\\p{Alnum}{1,12}$";
		String s2 = ";:_-*+";
		String alumFalse = "^\\P{Alnum}{1,12}$";
		System.out.println(s + " matches '"+alumTrue + "' : "+s.matches(alumTrue));
		System.out.println(s2 + " matches '"+alumFalse + "' : "+s2.matches(alumFalse));

	}

}
