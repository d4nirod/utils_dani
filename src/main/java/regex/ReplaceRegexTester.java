/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package regex;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;


public class ReplaceRegexTester {
	
	private static final Pattern VARIABLE_PATTERN = Pattern.compile("\\[(\\S+?)\\]");

	public static void main(String[] args) {
		String str = "asdfasd [var1] asdfasdfasdf [var2] asdfasdfasdf [varNone1] asdfasdf [var2] sadfasdf [varNone2]";
		System.out.println(String.format("Original string: '%s'\n", str));
		
		test1(str);
		test2(str);
	}

	private static void test2(String str) {
		System.out.println("*** Tester 2");
		Matcher matcher = VARIABLE_PATTERN.matcher(str);
		int count = 0;
		StringBuffer sb = new StringBuffer(str.length());
		while (matcher.find()) {
			String key = matcher.group(1);
			Optional<String> value = getValue(key);
			
			System.out.println(String.format("[%d] Found key: '%s', value: '%s'", 
					count, key, value.isPresent() ? value.get() : "UNKNOWN"));

			if (value.isPresent()) {
				matcher.appendReplacement(sb, Matcher.quoteReplacement(value.get()));
			}
			count++;
		}
		matcher.appendTail(sb);
		System.out.println(String.format("\nProcessed string: '%s'", sb.toString()));
	}
	
	private static void test1(String str) {
		System.out.println("*** Tester 1");
		Matcher matcher = VARIABLE_PATTERN.matcher(str);
		int count = 0;
		while (matcher.find()) {
			String key = matcher.group(1);
			Optional<String> value = getValue(key);
			
			System.out.println(String.format("[%d] Found key: '%s', value: '%s'", 
					count, key, value.isPresent() ? value.get() : "UNKNOWN"));
			if (value.isPresent()) {
				str = matcher.replaceFirst(value.get());
				System.out.println(String.format("[%d] Processed string '%s'", 
						count, str));
				matcher = VARIABLE_PATTERN.matcher(str); //Reset the matcher
			}
			
			count++;
		}
		System.out.println(String.format("\nProcessed string: '%s'\n", str));
	}
	
	private static Optional<String> getValue(String varName) {
		switch (StringUtils.defaultString(varName)) {
			case "var1":
				return Optional.of("ONE");
			case "var2":
				return Optional.of("TWO");
			default:
				return Optional.empty();
		}
	}

}
