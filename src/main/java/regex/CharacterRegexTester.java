package regex;

import java.util.Arrays;
import java.util.List;

public class CharacterRegexTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> lst = Arrays.asList("", "1","A", "d", "p", " T", "TA", "PY", "P ",  "D", "P", "T");
		
		for(String s : lst)
			System.out.printf("'%s'\t\t\t: %s\n", s, s.toUpperCase().matches("^[DPT]$"));
	}

}
