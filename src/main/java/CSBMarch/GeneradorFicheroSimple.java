package CSBMarch;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class GeneradorFicheroSimple {
	public static void main(String[] args) {
		Writer out = null;
		String cabz   =	"035612345678Z             001%s%s0061000501119101110   73       \n" +
					  	"035612345678Z             002MARIA BATLE RAMON                          \n" +
					  	"035612345678Z             003DOMICILIO MARIA ORDENANTE                  \n" +
					  	"035612345678Z             004POBLACION MARIA ORDENANTE                  \n" +
					  	"035612345678Z             007MERLUZO POR CTA DE                         \n" +
					  	"035612345678Z             008MERLUZO DOMICILIO                          \n" ;
		
		String apunte = "065612345678Z BENEF7      01000000000010000720004000012235119  93       \n" + 
						"065612345678Z BENEF7      011PERICO DE LOS PALOTES                      \n" +
						"065612345678Z BENEF7      012CALLE SIN NOMBRE BENEF                     \n" +
						"065612345678Z BENEF7      01407009PALMA                                 \n" +
						"065612345678Z BENEF7      015BALEARES                                   \n" +
						"065612345678Z BENEF7      016EL CONCEPTO                                \n" +
						"065612345678Z BENEF7      017ES EL CONCEPTO                             \n" ;
		
		String pie    = "085612345678Z                %010d%010d%010d             \n";
		int tamano    = 6*1024*1024 ;
		int tsincabz  = tamano - cabz.toString().length();		
		int tapunte   = apunte.toString().length();
		int nApuntes  = (int)Math.ceil((double)tsincabz / (double)tapunte);
//		int lineas    = (nApuntes+1)*5;
		try {
			int lineas = countLines(cabz) + (countLines(apunte)*nApuntes) + countLines(pie);
			System.out.printf("Tama*o: %d, apuntes: %d, registros: %d\n", tamano, nApuntes, lineas);
			
			pie = String.format(pie, nApuntes, nApuntes, lineas);
			
			Date hoy = GregorianCalendar.getInstance().getTime();
			String fecha = new SimpleDateFormat("ddMMyy").format(hoy);
			cabz = String.format(cabz, fecha, fecha);
			
			String ts = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(hoy);
			String fname =  "c:/tmp/CSBMarch/test_"+ts+".q34";
			System.out.println("Fichero salida: "+fname);
			File file = new File(fname);		
			int cnt = 0;
			out = new BufferedWriter(new FileWriter(file));			
			out.write(cabz);
			cnt+=cabz.length();
			for(int i=0; i<nApuntes;i++){
				out.write(apunte);
				cnt+=apunte.length();
			}
			out.write(pie);
			cnt+=pie.length();
			out.close();
			System.out.printf("Escrito %d caracteres, %d bytes", cnt, file.length());
			
			
		} catch (IOException e) {
			try {if(out!=null)out.close();}catch (IOException e1){}
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public static int countLines(String s) throws IOException{
		LineNumberReader  lnr = new LineNumberReader(new StringReader(s));
		lnr.skip(Long.MAX_VALUE);
		return lnr.getLineNumber();
	}


}
