package patterns;

import java.util.Arrays;
import java.util.List;


public class Cohort {

	private List<Student> students;

	public static Cohort getInstance(Student... students) {
		return new Cohort(students);
	}

	private Cohort(Student... students) {
		this.setStudents(Arrays.asList(students));
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

}