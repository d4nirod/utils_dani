package patterns;

public class VarArgsFactory {


	public static void main(String[] args) {
		Cohort c = Cohort.getInstance(new Student("Pepito"), new Student("Juanito"), new Student("Jaimito") );
		int i=0;
		for(Student s : c.getStudents())
			System.out.println(String.format("Student %d: %s",i++, s.getName()));

	}

}
