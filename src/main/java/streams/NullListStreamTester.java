/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.ListUtils;

public class NullListStreamTester {
	
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add(null);
		String collect = ListUtils.emptyIfNull(list).stream().filter(Objects::nonNull).map(String::trim).collect(Collectors.joining(","));
		System.out.println("Collect: '" + collect + "'");
	}

}
