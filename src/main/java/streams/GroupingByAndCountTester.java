/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package streams;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GroupingByAndCountTester {
	enum Type {
		A, B, C
	}
	
	public final static Type A = Type.A;
	public final static Type B = Type.B;
	public final static Type C = Type.C;
	
	public static void main(String[] args) {
		List<Type> l = Arrays.asList(A, A, B, A, B, B);
		
		Map<Type, Long> counted = l.stream()
		.filter(Objects::nonNull).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		
		counted.entrySet().stream().forEach(e -> System.out.println(e.getKey() + ": " + e.getValue()));

	}

}
