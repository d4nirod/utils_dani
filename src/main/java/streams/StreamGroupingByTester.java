/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package streams;

import static java.util.stream.Collectors.groupingBy;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import utils.data.Contact;

public class StreamGroupingByTester {
	public static void main(String[] args) {
		List<Contact> list = Arrays.asList(
				new Contact("John Smith", "Acme"),
				new Contact("Joe Bloggs", null),
				new Contact("Jane White", "Acme"),				
				new Contact("Fred Public", "NewCo")				
		);
		// Filter out elements with null company otherwise NPE as null cannot be key
		Map<String, List<Contact>> grouped = list.stream()
				.filter(c -> c.getCompany() != null)
				.collect(groupingBy(Contact::getCompany));
		
		grouped.entrySet().forEach(e -> System.out.println(
				"Company: " + e.getKey() + " Members: " + StringUtils.joinWith(", ", e.getValue())
		));
	}

}
