package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

public class checkEmtpyStream {

	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		List<String> mappedList = list.stream().map(String::toLowerCase).collect(Collectors.toList());
		
		System.out.println(String.format("Mapped list is null: %s, is empty: %s", (mappedList == null), mappedList.isEmpty()));
		
		boolean noneBlank = list.stream().noneMatch(StringUtils::isBlank);
		System.out.println("None blank: " + noneBlank);

	}

}
