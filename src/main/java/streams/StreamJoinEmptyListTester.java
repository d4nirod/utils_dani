/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StreamJoinEmptyListTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> l = new ArrayList<>();
		System.out.println("joined str: '" + l.stream().collect(Collectors.joining(",")) + "'" );
	}

}
