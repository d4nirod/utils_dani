/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package streams;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class CastListFilterTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<?> l = Arrays.asList("foo", null, "bar", Integer.valueOf(1), "foo");
		AtomicInteger index = new AtomicInteger();
		l.stream()
//		.filter(Objects::nonNull)			--> not needed since Class.isInstance() evals to false for nulls
		.filter(String.class::isInstance)
		.map(String.class::cast)
		.distinct()
		.forEach(s -> System.out.println(index.getAndIncrement() + ": " + s));
	}

}
