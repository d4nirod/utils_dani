package streams;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;


public class SplitListByType {

	public static void main(String[] args) {
		List<Number> numberList = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			if ((i % 2) == 0) {
				numberList.add(new Integer(i));
			} else {
				numberList.add(new Long(i));
			}
		}
		Function<Number, Class<?>> type =  n -> n.getClass();
		Map<Class<?>, List<Number>> groupedNumbers = numberList.stream().collect(Collectors.groupingBy(type));
		for (Entry<Class<?>, List<Number>> entry : groupedNumbers.entrySet()) {
			System.out.println("type: " + entry.getKey() + ", values: " + entry.getValue().stream().map(n -> n.toString()).collect(Collectors.joining(", ")));
		}
		
		List<Number> ints =  groupedNumbers.get(Integer.class);

	}
	
	public static <T> List<T> filterMessages(List<Number> numbers, Class<T> clazz) {
		return CollectionUtils.emptyIfNull(numbers)
		.stream()
		.filter(clazz::isInstance)
		.map(clazz::cast)
		.collect(Collectors.toList());
	}

}
