package streams;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PartitioningBy {

	public static void main(String[] args) {
		
		List<Integer> mainlist = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		
		List<Integer> comparelist = Arrays.asList(2, 4, 6, 8);
		
		final Map<Boolean, List<Integer>> split = mainlist.stream().collect(Collectors.groupingBy(i -> comparelist.contains(i)));
		
		split.entrySet().stream()
			.forEachOrdered(e -> System.out.println(String.format(
					"Key: %s, values: %s", e.getKey(), String.join(", ", e.getValue().toString()))));
		// TODO Auto-generated method stub

	}

}
