/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package streams;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections4.ListUtils;

public class AnyMatchEmptyListTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> emptyList = Collections.emptyList();
		System.out.println("empty String list result: " + emptyList.stream().anyMatch(String::isEmpty));
		
		List<String> list = Arrays.asList("blah", "");
		System.out.println("non-empty String list result: " + list.stream().anyMatch(String::isEmpty));
		
		System.out.println("empty Object list result: " + ListUtils.emptyIfNull(null).stream().anyMatch(Objects::nonNull));
		
	}

}
