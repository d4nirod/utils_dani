/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package enums;

public class StringEnumTester {

	public enum Category {
		COMMENT("comment"), 
		APPROVAL("approval"), 
		ASSIGNMENT("assignment");
		
		public final String value;

		Category(String value) {
			this.value = value;
		}
		
//		public static Category valueOfLabel(String label) {
//		    for (Category e : values()) {
//		        if (e.label.equals(label)) {
//		            return e;
//		        }
//		    }
//		    return null;
//		}
//		
		@Override
		public String toString() { 
		    return this.value; 
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.out.println(Category.COMMENT);
		System.out.println(Category.APPROVAL);
		System.out.println(Category.ASSIGNMENT);
		
		Category category = Category.APPROVAL;
		switch (category)  {
			case APPROVAL:
				System.out.println(category + ": it's an approval category");
				break;
			default:
				System.out.println(category + ": it's other category");
		}

	}
	
	

}
