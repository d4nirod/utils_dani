/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package generics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ListCasting {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Map<String, Object> map = new HashMap<>();
		
		List<String> list = new ArrayList<>();
		list.add("a");
		list.add("b");
		
		map.put("1", list);
		map.put("2", null);
		map.put("3", new ArrayList<>());
		map.put("4", Integer.valueOf(0));
		
		for (String k : map.keySet()) {
			final List<String> l = (List<String>) map.get(k);
			System.out.println(String.format("key: %s, instanceof List: %s, value: %s", k, (map.get(k) instanceof List), l));
		}
		final String key = "5";
		final List<String> l = (List<String>) map.get(key);
		System.out.println(String.format("key: %s, instanceof List: %s, value: %s", key, (map.get(key) instanceof List), l));
	}

}
