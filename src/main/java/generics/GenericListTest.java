package generics;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GenericListTest {

	public static void main(String[] args) {

		GenericListTest test = new GenericListTest();

		ArrayList<ChildClazzA> listA = new ArrayList<ChildClazzA>();
		listA.add(new ChildClazzA());

		ArrayList<ChildClazzB> listB = new ArrayList<ChildClazzB>();
		listB.add(new ChildClazzB());

		ArrayList<Object> listD = new ArrayList<Object>();

		System.out.println("List A *******");
		test.copyList(listA, listD);
		for(Object item : listD)
			System.out.println(item.getClass());
		listD.clear();
		System.out.println("List B *******");
		test.copyList(listB, listD);
		for(Object item : listD)
			System.out.println(item.getClass());


	}

	public <T> void copyList(List<? extends BaseClazz> org, List<Object> ds){
		for(BaseClazz item : org){
			if( item instanceof ChildClazzA){
				System.out.println("*** Is type ChildClazzA!");
				ds.add((ChildClazzA)item);

			}else if(item instanceof ChildClazzB){
				System.out.println("*** Is type ChildClazzB!");
				ds.add((ChildClazzB)item);
			}

		}
	}
}
