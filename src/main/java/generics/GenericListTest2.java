package generics;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GenericListTest2 {

	public static void main(String[] args) {

		GenericListTest2 test = new GenericListTest2();

		ArrayList<ChildClazzA> listA = new ArrayList<ChildClazzA>();
		listA.add(new ChildClazzA());

		ArrayList<ChildClazzB> listB = new ArrayList<ChildClazzB>();
		listB.add(new ChildClazzB());

		ArrayList<Object> listD = new ArrayList<Object>();

		System.out.println("Doing list A *******");
		test.copyList(listA, listD);
		listD.clear();
		System.out.println("Doing list B *******");
		test.copyList(listB, listD);
	}

	@SuppressWarnings("unchecked")
	public <T> void copyList(List<? extends BaseClazz> org, List<Object> ds){
		try {
			for(BaseClazz item : org){

	//			Class<T> persistentClass = (Class<T>) ((ParameterizedType) item.getClass()
	//                    .getGenericSuperclass()).getActualTypeArguments()[0];
	//			ds.add(persistentClass.cast(item));

				Class<? extends BaseClazz> clazz = item.getClass();


					clazz.getMethod("printName", null).invoke(item, null);
					if(item instanceof ChildClazzA){
						clazz.getMethod("printStuffA", null).invoke(item, null);

					}else
						clazz.getMethod("printStuffB", null).invoke(item, null);

	//			Type type = clazz.getGenericSuperclass();
	//			Class<T> persistentClass = (Class<T>) ((ParameterizedType) item.getClass()
	//                    .getGenericSuperclass()).getActualTypeArguments()[0];
				ds.add(clazz.cast(item));


			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
}
