package generics;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class ClassNameArgument {

	public static void main(String[] args) {
		ClassNameArgument o = new ClassNameArgument();
		try {
			System.out.println(ClassNameArgument.doStuff(o.getClass(), "ClassNameArgument.txt"));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public final static String doStuff(Class<?> clazz, String filename) throws Exception {
		String resPath = null;
		String basePath = clazz.getPackage().getName().replaceAll("\\.", "/");
		resPath = basePath + "/" + filename;
		InputStream is = clazz.getClassLoader().getResourceAsStream(resPath);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int b = 0;
		while ((b = is.read()) != -1) {
			baos.write(b);
		}
		return new String(baos.toByteArray());
	}

}
