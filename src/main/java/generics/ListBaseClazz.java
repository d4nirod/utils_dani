package generics;

import java.util.List;
import java.util.ArrayList;

public class ListBaseClazz {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<BaseClazz> myClassList = new ArrayList<BaseClazz>();
//		if(myClassList instanceof ArrayList<BaseClazz>){
//			
//		}
//		Cannot perform instanceof check against parameterized type ArrayList<BaseClazz>.
//		Use the form ArrayList<?> instead since further generic type information will be erased at runtime
		if(myClassList instanceof List<?>){
			System.out.println(true);
		}

	}

}
