package generics;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class GenericContainerTest {

	private static <T> void metevalor(Class<T> t, Object o) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException{
		t.getMethod("append", String.class).invoke(o, "blah");
	}
	private static  <T> ArrayList<T> getList(Class<T> type){

		ArrayList<T> l = new ArrayList<T>();
		try {
			l.add((T)type.newInstance());
			Method m = type.getMethod("length", null);
			System.out.println("invoke length(): "+m.invoke(l.get(0), null));
			metevalor(type, l.get(0));
			System.out.println("invoke length(): "+m.invoke(l.get(0), null));

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		return l;
	}

	public static void main(String[] args) {

		System.out.println("l1 class: "+getList(StringBuilder.class).get(0).getClass());
		//getList(String.class).get(0).length();
		System.out.println("l2 class: "+getList(StringBuffer.class).get(0).getClass());

	}

}
