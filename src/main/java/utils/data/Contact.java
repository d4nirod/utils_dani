package utils.data;

public class Contact {
	private String	name;
	private String	company;

	public Contact(String name, String company) {
		this.name = name;
		this.company = company;
	}

	public String getName() {
		return name;
	}

	public String getCompany() {
		return company;
	}

	@Override
	public String toString() {
		return name;
	}

}