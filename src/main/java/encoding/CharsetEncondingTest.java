package encoding;

import java.io.File;

import common.FileUtils;


public class CharsetEncondingTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			byte[] data = FileUtils.readBytes(new File("c:/tmp/ASCII.Q34"));
			String[] charsets = {"IBM850", "windows-1252", "ISO-8859-1", "ISO-8859-15"};
			String filestr = "c:/tmp/ASCII_%s.Q34";
			for(int i=0; i<charsets.length; i++){
				System.out.print("Writing "+String.format(filestr, charsets[i]));
				FileUtils.writeBytesAsString(data, new File(String.format(filestr, charsets[i])), charsets[i]);
				System.out.println("done.");
			}
			System.out.println("");
			
			filestr = "c:/tmp/myteststring_%s.txt";
			String str = "prueba con E*E may*scula, e*e min*scula y * con cedilla *pregunta *admiraci*n y vocales acentuadas: *****";
			for(int i=0; i<charsets.length; i++){
				System.out.print("Writing "+String.format(filestr, charsets[i])+" ...");				
				FileUtils.writeStringAsBytes(str, new File(String.format(filestr, charsets[i])), charsets[i]);
				System.out.println("done.");
			}
						
		} catch (Exception e) {
			
			
			e.printStackTrace();
		}
	}

}
