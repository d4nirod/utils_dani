package encoding;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

public class CharSetTest {

    public static void main(String[] args) {
        System.out.println("Default Charset:      " + Charset.defaultCharset().name());
        
        System.setProperty("file.encoding", "Latin-1");
        
        System.out.println("file.encoding:        " + System.getProperty("file.encoding"));
        System.out.println("Default Charset:      " + Charset.defaultCharset().name());
        System.out.println("Default I/O Charset:  " + getDefaultCharSet());
    }

    private static String getDefaultCharSet() {
        OutputStreamWriter writer = new OutputStreamWriter(new ByteArrayOutputStream());
        String enc = writer.getEncoding();
        return enc;
    }
}
