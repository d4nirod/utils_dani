package encoding;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

public class CharsetDecoderTest {

	public static void main(String[] args) {
		CharsetDecoder cs = Charset.forName("UTF-8").newDecoder();
		final String str = "El ni*o es un fen*meno meteorol*gico. Implica p*rdida de *.";
	    try {
	        System.out.println("(1) "+cs.decode(ByteBuffer.wrap(str.getBytes("utf-8"))).toString());

	    }
	    catch(CharacterCodingException e){
	    	e.printStackTrace();
	    } catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	    try {
	        System.out.println("(2) "+cs.decode(ByteBuffer.wrap(str.getBytes("iso-8859-15"))).toString());

	    }
	    catch(CharacterCodingException e){
	    	e.printStackTrace();
	    } catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

}
