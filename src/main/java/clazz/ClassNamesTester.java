/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package clazz;

public class ClassNamesTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		printNames(ClassNamesTester.class);
		printNames(String.class);
		printNames(byte.class);
		
	}

	protected static void printNames(Class<?> clazz) {
		System.out.println("getCanonicalName: " + clazz.getCanonicalName());
		System.out.println("getName: " + clazz.getName());
		System.out.println("getSimpleName: " + clazz.getSimpleName());
		System.out.println();
	}

}
