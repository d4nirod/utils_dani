package datetime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateRangeTestMillis {

	public static void main(String[] args) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String sIni = "28/04/2014";
		String sFin = "29/04/2014";
		Date inicio = null;
		Date fin = null;
		Date hoy = new Date();
		try{
			inicio = df.parse(sIni);
			fin = df.parse(sFin);
			long startTimeInMillis = inicio.getTime();
			long endTimeInMillis = fin.getTime();

			String result = "NO EST*";
			if(hoy.getTime() >= startTimeInMillis && hoy.getTime() < endTimeInMillis){
				result = "EST*";
			}
			System.out.println(String.format("%s : %s en rango %s - %s", hoy, result, inicio, fin));


		}catch (ParseException e){
			System.out.println(e.getLocalizedMessage());
		}

	}

}
