package datetime;

import java.util.Date;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;


public class CommonsDateRange {

	public static void main(String[] args) {
		FastDateFormat df = FastDateFormat.getInstance("dd/MM/yyyy");
		Date quince = DateUtils.setDays(new Date(), 15); //quince de este mes
		Date rangoIni = DateUtils.addMonths(quince, 1);  //quince del mes q viene
		Date rangoFin = DateUtils.addYears(quince, 1);  //quince del mes del a*o que viene
		Range<Date> range = Range.between(rangoIni, rangoFin);

		for(int i=0; i<14; i++){
			System.out.println(String.format("%s is in range [%s - %s]? %s",
					df.format(quince), df.format(rangoIni), df.format(rangoFin), range.contains(quince)));
			quince = DateUtils.addMonths(quince, 1);
		}

	}

}
