package datetime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateRangeTest {

	public static void main(String[] args) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String sIni = "28/04/2014 00:00:00";
		String sFin = "29/04/2014 23:59:59";
		Date inicio = null;
		Date fin = null;
		Date hoy = new Date();
		try{
			inicio = df.parse(sIni);
			fin = df.parse(sFin);
			String result = "NO EST*";
			if(inicio!=null && fin!=null && !(hoy.before(inicio) || hoy.after(fin)) ){
				result = "EST*";
			}
			System.out.println(String.format("%s : %s en rango %s - %s", hoy, result, sIni, sFin));

		}catch (ParseException e){
			System.out.println(e.getLocalizedMessage());
		}

	}

}
