package datetime;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

public class DateRangesMonth {

	public static void main(String[] args) {
		Date dateHoy =new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateHoy);
		int mesHoy = cal.get(Calendar.MONTH)+1;
		System.out.println(cal.getTime() + " mes: "+ mesHoy);
		cal.add(Calendar.MONTH, 1);
		int mesQviene = cal.get(Calendar.MONTH)+1;
		System.out.println(cal.getTime() + " mes que viene: "+ mesQviene);
		cal.add(Calendar.MONTH, 11);
		int mesAnyoQviene = cal.get(Calendar.MONTH)+1;
		System.out.println(cal.getTime() + " mes anyo que viene: "+ mesAnyoQviene);
		//quince de este
		Date quinceEste = DateUtils.setDays(dateHoy, 15);
		//quince del mes q viene
		Date quinceQviene = DateUtils.addMonths(quinceEste, 1);
		System.out.println("quince q viene "+quinceQviene);
		//quince del mes del a*o que viene
		System.out.println("quince mes a*o q viene "+DateUtils.addYears(quinceEste, 1));


		Calendar cal2 = Calendar.getInstance();
		cal2.set(2015, 11, 22);
		Date dic22 = cal2.getTime();
		quinceQviene = DateUtils.addMonths(DateUtils.setDays(dic22, 15), 1);
		System.out.println(quinceQviene);




	}

}
