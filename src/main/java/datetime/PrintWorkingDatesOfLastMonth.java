/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package datetime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.json.JSONArray;
import org.json.simple.JSONObject;

public class PrintWorkingDatesOfLastMonth {
	public static void main(String[] args) {
		Locale locale = new Locale("es", "ES");
		
		LocalDate ld = LocalDate.now().minusMonths(1).withDayOfMonth(1);
		final int currentMonth = ld.getMonthValue();
		
		while (ld.getMonthValue() == currentMonth) {
			DayOfWeek dayOfWeek = ld.getDayOfWeek();
			if (!dayOfWeek.equals(DayOfWeek.SATURDAY) && !dayOfWeek.equals(DayOfWeek.SUNDAY)) {
				System.out.println(
						ld.format(DateTimeFormatter.ofPattern("yyyy-MM-dd EEEE", locale)));
			} else {
				System.out.println();
			}
			ld = ld.plusDays(1);
		}
	}
}
