package datetime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;

public class PrintDateRange {

	public static void main(String[] args) {
		FastDateFormat df = FastDateFormat.getInstance("dd/MM/yyyy");
//		Date quince = DateUtils.setDays(new Date(), 15); //quince de este mes
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 15);

//		Date rangoIni = DateUtils.addMonths(quince, 1);  //quince del mes q viene
//		Date rangoFin = DateUtils.addYears(quince, 1);  //quince del mes del a*o que viene
		List<Date> res = new ArrayList<Date>(12);
		for(int i=1; i<13; i++){
			cal.add(Calendar.MONTH, 1);
			res.add(cal.getTime());
//			res.add(DateUtils.addMonths(quince, i));
		}

		for(Date d:res)
			System.out.println(df.format(d));

	}

}
