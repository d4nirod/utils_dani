package datetime;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class TimeStampTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// create a java calendar instance
		Calendar calendar = Calendar.getInstance();

		// get a java.util.Date from the calendar instance.
		// this date will represent the current instant, or "now".
		java.util.Date now = calendar.getTime();

		// a java current time (now) instance
		java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
		System.out.println("SQL timestamp: "+currentTimestamp);
		
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String sdate = df.format(GregorianCalendar.getInstance().getTime());
		System.out.println("SimpleDateFormat timestamp: "+sdate);

	
		

	}

}
