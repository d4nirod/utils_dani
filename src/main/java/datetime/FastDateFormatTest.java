package datetime;

import java.util.GregorianCalendar;
import java.util.Locale;

import org.apache.commons.lang3.time.FastDateFormat;

public class FastDateFormatTest {

	public FastDateFormatTest() {
	}

	public static void main(String[] args) {
		FastDateFormat fmt = FastDateFormat.getInstance("yyyyMMddHHmmss");
		FastDateFormat fmt2 = FastDateFormat.getInstance("dd MMMM yyyy", new Locale("ca", "ES"));
		System.out.println(fmt.format(GregorianCalendar.getInstance().getTime()));
		System.out.println(fmt2.format(GregorianCalendar.getInstance().getTime()));
	}
}
