/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package datetime;

import java.io.FileWriter;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DumpWorkingDatesOfLastMonthAsJson {
	public static void main(String[] args) {
		Locale locale = new Locale("es", "ES");
		
		LocalDate ld = LocalDate.now().minusMonths(1).withDayOfMonth(1);
		final int currentMonth = ld.getMonthValue();
		JSONArray workdays = new JSONArray();
		
		while (ld.getMonthValue() == currentMonth) {
			if (!ld.getDayOfWeek().equals(DayOfWeek.SATURDAY) && !ld.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
				JSONObject obj = new JSONObject();
				obj.put("date", ld.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
				obj.put("dow", ld.getDayOfWeek().toString());
				workdays.add(obj);
			}
			ld = ld.plusDays(1);
		}
//		System.out.println(workdays.toJSONString());
		
		try (FileWriter file = new FileWriter("/Users/daniel/Documents/month_work_days.json")) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String pretty = gson.toJson(workdays);
			file.write(pretty);
			System.out.println("Successfully wrote JSON object to file...");
			System.out.println("\nJSON Object: " +pretty);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
