/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package datetime;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.time.FastDateFormat;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class DumpWorkingDatesOfLastMonthAsJson2 {
	
	private static final String[] CSV_HEADERS = { "date", "dow"};
	private static final String DATE_STRING = FastDateFormat.getInstance("yyyyMMdd'T'HHmm").format(new Date());


	public static void main(String[] args) throws IOException {
		
		LocalDate ld = LocalDate.now().minusMonths(1).withDayOfMonth(1);
		final int currentMonth = ld.getMonthValue();
		List<LinkedHashMap<String, String>> workdays = new ArrayList<LinkedHashMap<String, String>>();
		
		while (ld.getMonthValue() == currentMonth) {
			if (!ld.getDayOfWeek().equals(DayOfWeek.SATURDAY) && !ld.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
				LinkedHashMap<String, String> workday = new LinkedHashMap<>();
				workday.put("date", ld.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
				workday.put("dow", ld.getDayOfWeek().toString());
				workdays.add(workday);
			}
			ld = ld.plusDays(1);
		}
		
		saveToJsonFile(workdays);
		System.out.println("-------------------------------------------");
		saveToCSVFile(workdays);
	}

	private static void saveToJsonFile(List<LinkedHashMap<String, String>> target) throws IOException {
		Type listType = new TypeToken<List<LinkedHashMap<String, String>>>() {}.getType();
//		Gson gson = new Gson();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(target, listType);
		try (FileWriter file = new FileWriter(
				"/Users/daniel/Documents/month_work_days." + DATE_STRING + ".json")) {
			file.write(json);
			System.out.println("Successfully wrote JSON to file: ");
			System.out.println(json);

		} 
		System.out.println(json);

	}
	

	private static void saveToCSVFile(List<LinkedHashMap<String, String>> mapList) throws IOException {
		CSVFormat csvFormat = CSVFormat.DEFAULT.withHeader(CSV_HEADERS);
		try (CSVPrinter filePrinter = new CSVPrinter(
				new FileWriter("/Users/daniel/Documents/month_work_days." + DATE_STRING + ".csv"), csvFormat);
			CSVPrinter consolePrinter = new CSVPrinter(System.out, csvFormat);
		) {
			for (LinkedHashMap<String, String> map : mapList) {
				filePrinter.printRecord(map.values());
				consolePrinter.printRecord(map.values());
			}
		}
	}

}
