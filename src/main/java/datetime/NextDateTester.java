/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package datetime;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.time.DateUtils;

public class NextDateTester {

	private LocalDateTime currentDate;

	public NextDateTester(LocalDateTime startDate) {
		this.currentDate = startDate;
	}
	
	public LocalDateTime getNextDate() {
		currentDate = currentDate.plusDays(1);
//		currentDate = currentDate.plusHours(1);
		currentDate = currentDate.plusMinutes(1);
//		currentDate = currentDate.plusSeconds(1);
		return currentDate;
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
	    DateTimeFormatter dateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM);
	    DateTimeFormatter timeFormatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);

		LocalDateTime now = LocalDateTime.now();
		NextDateTester tester = new NextDateTester(now);
		Set<String> dates = new HashSet<>();
		Set<String> times = new HashSet<>();
		
		
		for (int i = 0; i < 10000; i++) {
			LocalDateTime date = tester.getNextDate();
			boolean dateExisted = !dates.add(date.format(dateFormatter));
			boolean timeExisted = !times.add(date.format(timeFormatter));
			if (dateExisted || timeExisted) {
				System.out.print(date.format(dateTimeFormatter));
				if (dateExisted ) {
					System.out.print(" Repeated date found: " + date.format(dateFormatter));
				}
				if (timeExisted) {
					System.out.print(" Repeated time found: " + date.format(timeFormatter));
				}
				System.out.println("\n");
			}
		}
	}
	
	

}
