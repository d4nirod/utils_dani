/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package datetime;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;

public class ZonedDateTimeFormattingTester {
	private static final Locale LOCALE_ES = new Locale("es", "ES");
	private static final Locale LOCALE_SV = new Locale("sv");

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Date now = new Date();
		System.out.println("---DATETIME---");
		printLocalizedDateTime(now, Locale.US, ZoneId.of("America/Los_Angeles"));
		printLocalizedDateTime(now, Locale.UK, ZoneId.of("Europe/London"));
		printLocalizedDateTime(now, Locale.ENGLISH, ZoneId.of("Europe/London"));
		printLocalizedDateTime(now, LOCALE_ES, ZoneId.of("Europe/Madrid"));
		printLocalizedDateTime(now, LOCALE_SV, ZoneId.of("Europe/Stockholm"));
		
		System.out.println("---DATE---");
		printLocalizedDate(now, Locale.US, ZoneId.of("America/Los_Angeles"));
		printLocalizedDate(now, Locale.UK, ZoneId.of("Europe/London"));
		printLocalizedDate(now, Locale.ENGLISH, ZoneId.of("Europe/London"));
		printLocalizedDate(now, LOCALE_ES, ZoneId.of("Europe/Madrid"));
		printLocalizedDate(now, LOCALE_SV, ZoneId.of("Europe/Stockholm"));
		
		System.out.println("---TIME---");
		printLocalizedTime(now, Locale.US, ZoneId.of("America/Los_Angeles"));
		printLocalizedTime(now, Locale.UK, ZoneId.of("Europe/London"));
		printLocalizedTime(now, Locale.ENGLISH, ZoneId.of("Europe/London"));
		printLocalizedTime(now, LOCALE_ES, ZoneId.of("Europe/Madrid"));
		printLocalizedTime(now, LOCALE_SV, ZoneId.of("Europe/Stockholm"));
	}
	
	public static void printLocalizedDateTime(Date date, Locale locale, ZoneId zoneId) {
		ZonedDateTime dateTime = ZonedDateTime.ofInstant(date.toInstant(), zoneId);
		System.out.println(locale + " " + zoneId + ": " +
				"(local " + dateTime.toLocalDateTime() + " " + dateTime.format(DateTimeFormatter.ofPattern("z", locale))  + "): " + 
				dateTime.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL)) 
				+ " | " +
				dateTime.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG)) 
				+ " | " +
				dateTime.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withLocale(locale)) 
				+ " | " +
				dateTime.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT).withLocale(locale)) 
		);
	}
	
	public static void printLocalizedDate(Date date, Locale locale, ZoneId zoneId) {
		ZonedDateTime dateTime = ZonedDateTime.ofInstant(date.toInstant(), zoneId);
		System.out.println(locale + " " + zoneId + ": " +
				"(local " + dateTime.toLocalDate() + " " + dateTime.format(DateTimeFormatter.ofPattern("z", locale)) + "): " + 
				dateTime.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).withLocale(locale)) 
				+ " | " +
				dateTime.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG).withLocale(locale)) 
				+ " | " +
				dateTime.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale)) 
				+ " | " +
				dateTime.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(locale)) 
				);
	}

	public static void printLocalizedTime(Date date, Locale locale, ZoneId zoneId) {
		ZonedDateTime dateTime = ZonedDateTime.ofInstant(date.toInstant(), zoneId);
		dateTime.truncatedTo(ChronoUnit.DAYS);
		System.out.println(locale + " " + zoneId + ": " +
				"(local " + dateTime.toLocalTime() + " " + dateTime.format(DateTimeFormatter.ofPattern("z", locale))  + "): " + 
				dateTime.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.FULL)) 
				+ " | " +
				dateTime.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.LONG)) 
				+ " | " +
				dateTime.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM).withLocale(locale)) 
				+ " | " +
				dateTime.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).withLocale(locale)) 
				+ " | *" +
				dateTime.format(DateTimeFormatter.ofPattern("HH:mm").withLocale(locale)) 
				+ " *"
		);
	}

}
