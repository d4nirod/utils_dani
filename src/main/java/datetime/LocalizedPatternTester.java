/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package datetime;

import java.text.*;
import java.time.chrono.IsoChronology;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.util.*;

public class LocalizedPatternTester {

	public static void main(String[] args) {

		ArrayList<Locale> locales = new ArrayList<>();
		locales.add(Locale.US);
		locales.add(Locale.UK);
		locales.add(Locale.GERMANY);
		locales.add(new Locale("ES", "es"));

		Date date = new Date();

		for (Locale locale : locales) {
			SimpleDateFormat sdf = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.SHORT, locale);
			String pattern = sdf.toPattern();
			String localizedPattern = sdf.toLocalizedPattern();
			System.out.println("Locale: " + locale.getDisplayName());
			System.out.println("Pattern: " + pattern);
			System.out.println("LocalizedPattern: " + localizedPattern);
			String localizedPattern2 = DateTimeFormatterBuilder.getLocalizedDateTimePattern(FormatStyle.SHORT, null, IsoChronology.INSTANCE, locale);
			System.out.println("LocalizedPattern2: " + localizedPattern2);
			


			try {
				SimpleDateFormat temp = new SimpleDateFormat(localizedPattern, locale);
				System.out.println("Localized pattern re-parsed successfully");
			} catch (IllegalArgumentException e) {
				System.out.println("Localized pattern re-parsed unsuccessfully: " + e.getMessage());
			}
			SimpleDateFormat df = new SimpleDateFormat(pattern, locale);
			String dateString = df.format(date);
			System.out.println("Resulting date: " + dateString);
			String yearlessPattern = pattern.replaceAll("\\W?[Yy]+\\W?", "");
			System.out.println("YearlessPattern = " + yearlessPattern);
			SimpleDateFormat yearlessSDF = new SimpleDateFormat(yearlessPattern, locale);
			System.out.println("Resulting date without year: " + yearlessSDF.format(date) + "\n");
		}
	}
}