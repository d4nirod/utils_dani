package datetime;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.FastDateFormat;

public class DateAtZeroZero {

	public static void main(String[] args) {
		FastDateFormat DATE_PARSER = FastDateFormat.getInstance("dd HH:mm:ss");
		FastDateFormat DATE_FMT = FastDateFormat.getInstance("dd/MM/yyyy HH:mm:ss.SSS");
		try {
			Date d = (Date) DATE_PARSER.parseObject("15 00:00:00");
			System.out.println(DATE_FMT.format(d));
		} catch (ParseException e) {
			e.printStackTrace();
		}



	}

}
