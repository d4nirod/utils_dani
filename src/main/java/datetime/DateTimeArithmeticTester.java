/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package datetime;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateTimeArithmeticTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(0);
		cal.set(2019, 11, 10, 00, 00, 00);
		Date date = cal.getTime();
		
		ZonedDateTime zDate = ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
		DateTimeFormatter fmt = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL).withLocale(Locale.UK);
		
		System.out.println("Original datetime: " + fmt.format(zDate));
		System.out.println("Modified datetime: " + fmt.format(zDate.plusSeconds(-1)));
		
	}

}
