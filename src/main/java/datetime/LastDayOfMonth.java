package datetime;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;

public class LastDayOfMonth {

	public static void main(String[] args) {
		FastDateFormat df = FastDateFormat.getInstance("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		Date ultimo = DateUtils.setDays(new Date(), cal.getActualMaximum(Calendar.DATE));
		System.out.println("Ultimo d*a de este mes: "+df.format(ultimo));

		for(int i=0; i<13; i++){
			System.out.println(String.format("Date: %s. Last day of month: %s",
					df.format(cal.getTime()), cal.getActualMaximum(Calendar.DATE)));
			cal.add(Calendar.MONTH, 1);
		}
	}

}
