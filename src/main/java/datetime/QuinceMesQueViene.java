package datetime;

import java.util.Calendar;

public class QuinceMesQueViene {

	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.DAY_OF_MONTH, 15); //quince de este mes
		cal.add(Calendar.MONTH, 1); //quince del mes q viene
		System.out.println(cal.getTime());

	}

}
