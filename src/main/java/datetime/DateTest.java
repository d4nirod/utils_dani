package datetime;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTest {
	
	static void printDate(String d){
		
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		try {
			d = d.replaceAll("-", "/");		
			Date date = fmt.parse(d);
			System.out.printf("%s (%s)\n", date, fmt.format(date));
			
			
		} catch (ParseException e) {
	
			System.out.println("Formato incorrecto");
		}catch (NullPointerException e) {
		
			System.out.println("La fecha es null");
		}
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String[] dates = {"5-5-09", "01/1/2009", "2/02/2009", "3/3/2009", "aaa/2", null, "", "ad*fa", "12", "01/01/09"};
		for (String s : dates){
			System.out.print(s+" : ");
			printDate(s);
		}
		

	}

}
