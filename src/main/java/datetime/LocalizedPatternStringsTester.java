/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package datetime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.chrono.IsoChronology;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.Locale;

public class LocalizedPatternStringsTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Date date = new Date();
		for (Locale locale : DateFormat.getAvailableLocales()) {
//			System.out.println(locale.toString());
			SimpleDateFormat sdf = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.SHORT, locale);
			String pattern = sdf.toPattern();
			String localizedPattern = sdf.toLocalizedPattern();
//			System.out.println("Locale: " + locale.getDisplayName());
//			System.out.println("Pattern: " + pattern);
//			System.out.println("LocalizedPattern: " + localizedPattern);
			String localizedPattern2 = DateTimeFormatterBuilder.getLocalizedDateTimePattern(FormatStyle.SHORT, null, IsoChronology.INSTANCE, locale);
//			System.out.println("LocalizedPattern2: " + localizedPattern2);
			if (!pattern.equals(localizedPattern2)) {
				System.out.println(String.format("isEqual pattern: %s, Locale: %s, SDF pattern: %s, SDF localizedPattern: %s, DTF localizedPattern: %s ", 
						pattern.equals(localizedPattern2), locale, pattern, localizedPattern, localizedPattern2));
			}
		}
	}

}
