package datetime;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.FastDateFormat;

public class ParseDate {

	private static final FastDateFormat DATE_PARSER = FastDateFormat.getInstance("dd/MM/yyyy");

	public static void main(String[] args) {
		String fecha = "10/06/2015";
		try {
			System.out.println(String.format("Parsing string '%s'", fecha));
			Date d = (Date) DATE_PARSER.parseObject(fecha);
			System.out.println("Date parsed: "+d);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
