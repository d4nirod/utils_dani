/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package datetime;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ZoneIdParsingTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// CEST, PST, PT, PDT give error
		List<String> strings = Arrays.asList("GMT", "Europe/London", "America/New_York", "GMT+2", "UTC", "UTC-8",
				"CET");
		for (String s : strings) {
			System.out.println(String.format("Testing '%s' : '%s", s, ZoneId.of(s)));
		}
		
		System.out.println("SYSTEM DEFAULT zoneId: " + ZoneId.systemDefault().toString());
		
		System.out.println("Available zoneIds: ");
		ZoneId.getAvailableZoneIds().stream().forEach(System.out::println);

	}

}
