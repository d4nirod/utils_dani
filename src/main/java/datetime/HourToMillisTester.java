/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package datetime;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTimeConstants;


public class HourToMillisTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.out.println("Millis per hour");

		System.out.println("DateTimeConstants.MILLIS_PER_HOUR: " + 1L * DateTimeConstants.MILLIS_PER_HOUR);
		System.out.println("Duration.ofHours(1).toMillis(): "    + 1L * Duration.ofHours(1).toMillis());
		System.out.println("TimeUnit.HOURS.toMillis(1): "        + 1L * TimeUnit.HOURS.toMillis(1));

	}

}
