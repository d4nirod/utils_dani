package datetime;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateAndTimeZoneTest {

	public static void main(String[] args) {
		final TimeZone tz = TimeZone.getDefault();
		final Date d = new Date();
		boolean dst = tz.inDaylightTime(d);

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");
		System.out.println("Date default string: "+d);
	    System.out.println("Date formatted string: "+df.format(d));
	    System.out.println("Default timezone: "+tz.getDisplayName(dst, TimeZone.SHORT));

	    Calendar cal = Calendar.getInstance();
	    cal.setTime(d);
	    cal.set(Calendar.YEAR, 2016);
	    cal.set(Calendar.MONTH, Calendar.JULY);
	    System.out.println("New Date default string: "+cal.getTime());
	    System.out.println("New Date formatted string: "+df.format(cal.getTime()));
	    dst = tz.inDaylightTime(cal.getTime());
	    System.out.println("New Date default timezone: "+tz.getDisplayName(dst, TimeZone.SHORT));
	}

}
