package datetime;

import java.time.LocalDateTime;
import java.time.MonthDay;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class ZoneTruncatedTest {

	public static void main(String[] args) {
		ZoneId gmtZoneId = ZoneId.of("GMT");
		ZonedDateTime now = ZonedDateTime.now(gmtZoneId);
		ZoneId londonZoneId = ZoneId.of("Europe/London");
		ZoneId newYorkZoneId = ZoneId.of("America/New_York");
		ZoneId losAngelesZoneId = ZoneId.of("America/Los_Angeles");
		ZonedDateTime created = ZonedDateTime.of(LocalDateTime.of(2018, 10, 12, 12, 0, 0), gmtZoneId);
		
		
		
		for (int i = 0; i < 24; i++) {
			ZonedDateTime runtime = now.plusHours(i);
			ZonedDateTime LondonTime = runtime.withZoneSameInstant(londonZoneId);
			ZonedDateTime NYTime = runtime.withZoneSameInstant(newYorkZoneId);
			ZonedDateTime LATime = runtime.withZoneSameInstant(losAngelesZoneId);
			if (is6Local(runtime, created.withZoneSameInstant(gmtZoneId)) 
					|| is6Local(LondonTime, created.withZoneSameInstant(gmtZoneId)) 
					|| is6Local(NYTime,  created.withZoneSameInstant(gmtZoneId)) 
					|| is6Local(LATime, created.withZoneSameInstant(gmtZoneId))) {
				System.out.println(String.format(
						"runtime: %s (is 6: %s), London: %s (is 6 local: %s), New York: %s (is 6 local: %s), Los Angeles: %s (is 6 local: %s", 
						runtime, is6Local(runtime, created.withZoneSameInstant(gmtZoneId)),
						LondonTime, is6Local(LondonTime, created.withZoneSameInstant(gmtZoneId)),
						NYTime, is6Local(NYTime, created.withZoneSameInstant(gmtZoneId)),
						LATime, is6Local(LATime, created.withZoneSameInstant(gmtZoneId))
						));
			}
			
		}
		
	}
	
	public static boolean is6Local(ZonedDateTime runtime, ZonedDateTime created) {
		ZonedDateTime six = runtime.truncatedTo(ChronoUnit.HOURS).withHour(6);
		ZonedDateTime start = six.minusSeconds(1);
		ZonedDateTime end = six.plusHours(1);
		
		if (runtime.isAfter(start) && runtime.isBefore(end) && created.isAfter(start) && created.isBefore(end)) {
			return true;
		}
		return false;
	}

}
