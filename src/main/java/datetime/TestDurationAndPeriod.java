package datetime;

import static org.junit.Assert.assertEquals;

import java.time.Duration;
import java.time.Instant;
import java.time.Period;
import java.time.temporal.ChronoUnit;

import org.junit.Test;

public class TestDurationAndPeriod {
	
	@Test
	public void testDurationEqualsPeriod() {
		Instant now = Instant.now();
		assertEquals(now.minus(Duration.ofDays(1)), now.minus(Period.ofDays(1)));
		assertEquals(now.minus(Duration.ofDays(7)), now.minus(Period.ofWeeks(1)));
		assertEquals(now.truncatedTo(ChronoUnit.DAYS).minus(Duration.ofHours(12)),
				now.truncatedTo(ChronoUnit.DAYS).minus(1, ChronoUnit.HALF_DAYS));
		
		// this fails if now time is < 12:00H
//		assertEquals(now.minus(4, ChronoUnit.HOURS).truncatedTo(ChronoUnit.DAYS).minus(1, ChronoUnit.HALF_DAYS),
//				now.minus(4, ChronoUnit.HOURS).truncatedTo(ChronoUnit.HALF_DAYS).minus(Duration.ofDays(1)));
	}
}
