package datetime;

import java.time.ZoneId;

public class CheckDefaultAndUserTimezone {

	public static void main(String[] args) {
		ZoneId systemDefault = ZoneId.systemDefault();
		ZoneId userSystem =  ZoneId.of(System.getProperty("user.timezone"));
		System.out.println(systemDefault + " == " + userSystem + " " + (systemDefault.equals(userSystem)) );

	}

}
