package certs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Enumeration;

public class CertificatesTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			PrivateKeyEntry pke = CertificatesTest.getPrivateKeyEntry();
//			CertificatesTest.listCerts();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void listCerts() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException{

		String alias = null;
		String keyStoreFile = "c:/tmp/CAdES/BM2009_2.pfx";
		String password = "t3mp0r4l";
		KeyStore keystore = KeyStore.getInstance("PKCS12");
		keystore.load(new FileInputStream(keyStoreFile), password.toCharArray());

		// List the aliases
	    Enumeration<String> en = keystore.aliases();
	    for (; en.hasMoreElements(); ) {
	    	alias = en.nextElement();
	        // Does alias refer to a private key?
	        System.out.printf("alias: %s, isKeyEntry: %s\n" ,alias, keystore.isKeyEntry(alias));
	        // Does alias refer to a trusted certificate?
	        System.out.printf("alias: %s, isCertificateEntry: %s\n" ,alias, keystore.isCertificateEntry(alias));
	    }
	}

	public static KeyStore.PrivateKeyEntry getPrivateKeyEntry() throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException{

		String alias = null;
		String keyStoreFile = null;

			Certificate[] certChain = null;
			ArrayList<Certificate> certList = new ArrayList<Certificate>();
			keyStoreFile = "c:/tmp/CAdES/BM2009_2.pfx";

			alias = "{2F5DE0D0-85E5-449A-B355-980CAE56202B}";

			String password = "t3mp0r4l";
			KeyStore keystore = KeyStore.getInstance("PKCS12");
			keystore.load(new FileInputStream(keyStoreFile), password.toCharArray());

			certChain = keystore.getCertificateChain(alias);
			for (int i = 0; i < certChain.length; i++)
				certList.add(certChain[i]);

			KeyStore.PrivateKeyEntry keyEntry = new KeyStore.PrivateKeyEntry(
					(PrivateKey)keystore.getKey(alias, password.toCharArray()),
					certChain
				);
			return keyEntry;


	}
}
