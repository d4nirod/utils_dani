package ssl;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.GetMethod;

import ssl.naive.NaiveTrustProvider;

public class XTrustProviderTest {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			String uriStr = "https://teleintegra.bancamarch.es/Pagos/";
			System.out.println("Trying " + uriStr);

			XTrustProvider.install();

			// Protocol easyhttps = new Protocol("https", (ProtocolSocketFactory) new EasySSLProtocolSocketFactory(), 443);
			// URI uri = new URI(uriStr, true);
			// use relative url only
			GetMethod httpget;

			httpget = new GetMethod(uriStr);

			// httpget = new GetMethod(uri.getPathQuery());
			// HostConfiguration hc = new HostConfiguration();
			// hc.setHost(uri.getHost(), uri.getPort(), easyhttps);

			HttpClient client = new HttpClient();

			// client.executeMethod(hc, httpget);
			client.executeMethod(httpget);

			InputStream is = httpget.getResponseBodyAsStream();
			String content = readResponse(is);
			httpget.releaseConnection();

			System.out.println(content);

		} catch (URIException e) {
			e.printStackTrace();
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String readResponse(InputStream in) throws IOException {
		StringBuffer out = new StringBuffer();
		byte[] b = new byte[4096];
		for (int n; (n = in.read(b)) != -1;) {
			out.append(new String(b, 0, n));
		}
		return out.toString();
	}

}
