package ssl;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

public final class MyX509TrustManager implements X509TrustManager {

	/**
	 * Doesn't throw an exception, so this is how it approves a certificate.
	 *
	 * @see javax.net.ssl.X509TrustManager#checkClientTrusted(java.security.cert.X509Certificate[], String)
	 **/
	public void checkClientTrusted(X509Certificate[] chain, String authType) {
		System.out.println("checkClientTrusted()");
		System.out.println("Auth type=" + authType);

	}

	/**
	 * Doesn't throw an exception, so this is how it approves a certificate.
	 *
	 * @see javax.net.ssl.X509TrustManager#checkServerTrusted(java.security.cert.X509Certificate[], String)
	 **/
	public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		System.out.println("checkServerTrusted()");
		System.out.println("Auth type=" + authType);
	}

	public X509Certificate[] getAcceptedIssuers() {
		System.out.println("getAcceptedIssuers()");
		return new X509Certificate[] {};
	}

}

// MyX509TrustManager
