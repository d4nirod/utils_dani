package ssl;

import java.net.Socket;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

public class MyX509TrustManagerTest {

	public static void main(String[] arguments) {
		try {
			String host = "teleintegra.bancamarch.es";
			int port = 443;
			System.out.println("Testing SSL connection to "+host+":"+port);

			//			overrideTrustManager();

			//use our own trust manager so we can always trust
			//the URL entered in the configuration.
			X509TrustManager tm = new MyX509TrustManager();
			KeyManager[] km = null;
			X509TrustManager[] tma = new X509TrustManager[] { tm };

			SSLContext sslContext = SSLContext.getInstance("TLS");
			//SSLv3");
			sslContext.init(km, tma, new java.security.SecureRandom());
			SSLSocketFactory sf1 = sslContext.getSocketFactory();
//			HttpsURLConnection.setDefaultSSLSocketFactory(sf1);

			// HttpsURLConnection.setDefaultHostnameVerifier(new MyHostnameVerifier());



			Socket s0 = new Socket(host, port);
//			SSLSocketFactory ssf = (SSLSocketFactory) SSLSocketFactory.getDefault();
			SSLSocket sconnection = (SSLSocket) sf1.createSocket(s0, host, port, true);
			sconnection.setSoTimeout(1000 * 60);

			sconnection.setEnabledProtocols(sconnection.getEnabledProtocols());
			sconnection.setEnabledCipherSuites(sconnection.getEnabledCipherSuites());

			sconnection.setUseClientMode(true);
			sconnection.setNeedClientAuth(false);
			sconnection.setWantClientAuth(false);
			sconnection.setEnableSessionCreation(true);

			// FAIL if certificate not present!!
			System.out.println("Connecting...");
			sconnection.startHandshake();

			System.out.println("Connected !");

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace(System.out);
		} catch (KeyManagementException e) {
			e.printStackTrace(System.out);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	  public static void overrideTrustManager() {
//		  //use our own trust manager so we can always trust
//		  //the URL entered in the configuration.
//		  X509TrustManager tm = new MyX509TrustManager();
//		  KeyManager[] km = null;
//		  X509TrustManager[] tma = new X509TrustManager[] { tm };
//		  try {
////			  Security.addProvider( new com.sun.net.ssl.internal.ssl.Provider());
//
//
//			  SSLContext sslContext = SSLContext.getInstance("TLS");
//			  //SSLv3");
//			  sslContext.init(km, tma, new java.security.SecureRandom());
//			  SSLSocketFactory sf1 = sslContext.getSocketFactory();
//			  HttpsURLConnection.setDefaultSSLSocketFactory(sf1);
//
//			  // HttpsURLConnection.setDefaultHostnameVerifier(new MyHostnameVerifier());
//
//		  } catch (NoSuchAlgorithmException e) {
//			  e.printStackTrace(System.out);
//		  } catch (KeyManagementException e) {
//			  e.printStackTrace(System.out);
//		  }
//	  }

}
