package typecasting;

public class CastingTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Parent parent = new Parent("Pepe", "Gomez Perez", "123");
		Derived derived = new Derived("Pepito", "Gomez Fdez.", "1234", "Miau");
		//upcasting: ok, no problem ever.
		Parent pu = derived;
		System.out.println(String.format("Downcasted# Name: %1$s, Surname: %2$s, id: %3$s", pu.getName(), pu.getSurname(), pu.getId()));
		//downcasting: ok Derived obj is originally a Parent
		Derived dpu = (Derived) pu;
		System.out.println(String.format("Upcasted  # Name: %1$s, Surname: %2$s, id: %3$s, School: %4$s", dpu.getName(), dpu.getSurname(), dpu.getId(), dpu.getSchool()));
		//downcasting: run-time error
		Derived dd = (Derived) parent;
		System.out.println(String.format("Upcasted  # Name: %1$s, Surname: %2$s, id: %3$s", dd.getName(), dd.getSurname(), dd.getId()));

	}

}
