package typecasting;

public class Derived extends Parent {
	private String school;

	public Derived(String name, String surname, String id, String school) {
		super(name, surname, id);
		this.school = school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getSchool() {
		return school;
	}

}
