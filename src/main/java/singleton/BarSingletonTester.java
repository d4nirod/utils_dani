package singleton;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BarSingletonTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
		System.out.printf("Program started at %s. Waiting 3s.\n", 
				new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").format(Calendar.getInstance().getTime()));
		System.out.println("*****************************************");
		Thread.sleep(3000);
		
		Bar singleton = Bar.getBar();
		
		System.out.println("*****************************************");
		System.out.println("Singleton Init time: " + singleton.getInitTimeString());
		Thread.sleep(2000);
		System.out.println("Singleton Current time: " + singleton.getCurrentTimeString());
		System.out.println("Check Singleton Init time: " + singleton.getInitTimeString());
		
		System.exit(0);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(1);
			
		}

	}

}
