package singleton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

class Bar {
	private static class BarHolder {
		
		static {
			System.out.printf("BarHolder static initializer called at %s. Sleep 2s.\n", 
				new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").format(Calendar.getInstance().getTime()));
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
		
		{	
			System.out.println("BarHolder instance initializer called");
		}
		
		BarHolder(){
			System.out.println("BarHolder contructor called");
		}
			
		public static Bar bar = new Bar();
	}
	
	public static Bar getBar() {
		return BarHolder.bar;
	}
	
	private Bar(){
		System.out.printf("Bar contructor called at %s\n", 
				new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").format(Calendar.getInstance().getTime()));
		this.initTime = Calendar.getInstance().getTime();
	}
	
	private static DateFormat datefmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
	private Date initTime;
	public String getInitTimeString() {
		return datefmt.format(initTime);
	}
	
	public String getCurrentTimeString() {
		return datefmt.format(Calendar.getInstance().getTime());
	}
	
}