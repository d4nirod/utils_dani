package singleton;

public class EnumSingletonTester {

	public static void main(String[] args) {
		for(int i = 0; i < 3; i++) {
			System.out.println("########" +i);
			EnumSingleton.INSTANCE.doStuff(i);
			System.out.println("########");
		}
	}

}
