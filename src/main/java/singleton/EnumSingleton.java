package singleton;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public enum EnumSingleton {
	INSTANCE;
	private List<String> list;
	
	static {
		System.out.println("Static block called");		
	}
	
	private EnumSingleton() {
		list = new ArrayList<>();
		System.out.println("Constructor called");
	}
	
	public void doStuff(int i) {		
		list.add(String.valueOf(i));
		System.out.println("doStuff(): " + StringUtils.join(list, ","));
	}

	public List<String> getList() {
		return list;
	}

}
