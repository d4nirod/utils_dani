/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package emojis;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.parser.Parser;

import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import com.vdurmont.emoji.EmojiParser;
import com.vdurmont.emoji.EmojiParser.EmojiTransformer;
import com.vdurmont.emoji.EmojiParser.FitzpatrickAction;
import com.vdurmont.emoji.Fitzpatrick;

public class EmojiTester {

	/**
	 * @param args
	 * @throws EncoderException
	 */
	public static void main(String[] args) throws Exception {
//		EmojiManager.getAll().forEach(e -> System.out.println(e.getUnicode()));
		List<String> list = new ArrayList<>();

		EmojiManager.getAll().stream().filter(Emoji::supportsFitzpatrick)
				.forEach(e -> Stream.of(Fitzpatrick.values()).map(e::getUnicode).forEach(list::add));
		System.out.println(StringUtils.join(list, ", "));
//				.flatMap(e -> Stream.of(Fitzpatrick.values()).map(f -> e.getUnicode(f)));
//						.map(f -> e.getUnicode(f)));

		toUnicodeTest();
		parseToAliasesTest();
		parseEmojiTest();
	}

	public static void toUnicodeTest() {
		System.out.println("--------- toUnicodeTest ---------");

		String str = "An :grinning:awesome :smiley:string &#128516;with a few :wink:emojis! :thumbsup|type_4:";
		String result = EmojiParser.parseToUnicode(str);
		System.out.println(result);
		System.out.println(StringEscapeUtils.escapeJava(result));

		System.out.println();
	}

	public static void parseToAliasesTest() {
		System.out.println("--------- parseToAliasesTest ---------");

		String str = "Here is a boy: \uD83D\uDC66\uD83C\uDFFF!";
		System.out.println(EmojiParser.parseToAliases(str));
		// Prints twice: "Here is a boy: :boy|type_6:!"
		System.out.println(EmojiParser.parseToAliases(str, FitzpatrickAction.REMOVE));
		// Prints: "Here is a boy: :boy:!"
		System.out.println(EmojiParser.parseToAliases(str, FitzpatrickAction.IGNORE));
		// Prints: "Here is a boy: :boy:🏿!"
		System.out.println();
	}

	public static void parseEmojiTest() throws Exception {
		System.out.println("--------- parseEmojiTest ---------");
		// hex html entity (same code as Mac Unicode "U+1F44D"
		parseAndPrintEmoji("&#X1F44D");
		parseAndPrintEmoji("&#X1F44D;");
		// hex html entity with fitz (same code as Mac Unicode "U+1F44D U+1F3FE"
		parseAndPrintEmoji("&#X1F44D&#X1F3FE");
		// Unicode no fitz
		parseAndPrintEmoji("\uD83D\uDC4D");
		// Unicode w/ fitz
		parseAndPrintEmoji("\uD83D\uDC4D\uD83C\uDFFE");
		// Alias no fitz
		parseAndPrintEmoji(":thumbsup:");
		// Two literal emojis
		parseAndPrintEmoji("👍😃");
		// Two java escaped emojis
		parseAndPrintEmoji("\uD83D\uDC4D\uD83D\uDE03");

		// Two emoji aliases
		parseAndPrintEmoji(":thumbsup::wink:");

//		parseAndPrintEmoji("flirt");
		// Alias w/ slack-like Fitz format
		parseAndPrintEmoji(":thumbsup::skin-tone-5:");
		// Alias w/ emoji-java Fitz type format
		parseAndPrintEmoji(":thumbsup|type_5:");
		parseAndPrintEmoji(":blah:");
	}

	private static void parseAndPrintEmoji(String str) throws Exception {
		System.out.println(StringEscapeUtils.escapeJava(str) + " :");
//		if (str.startsWith("&#x")) {
		str = Parser.unescapeEntities(str, true);
//		}
		boolean isEmoji = EmojiManager.isEmoji(str);
		System.out.println("\tisEmoji: " + isEmoji);
//	    System.out.println("\tgetForTag: " + EmojiManager.getForTag(str));
		System.out.println("\tgetForAlias: " + EmojiManager.getForAlias(str));
		System.out.println("\tgetByUnicode: " + EmojiManager.getByUnicode(str));

		String parsedToUnicode = EmojiParser.parseToUnicode(str);
		String emojiUnicode = null;
//		if (!isEmoji) {
//			String removedEmojisStr = EmojiParser.removeAllEmojis(str);
//			if (StringUtils.isEmpty(removedEmojisStr)) {
//				// String contained only an emoji, likely with a valid fitz
//				Emoji emoji = EmojiManager.getByUnicode(parsedToUnicode);
//				if (emoji != null) {
//					emojiUnicode = emoji.getUnicode();
//				}
//			}
//		}
		Emoji emoji = null;
		if (!isEmoji) {
			boolean containsEmoji = EmojiManager.containsEmoji(parsedToUnicode);
			boolean isOnlyEmojis = EmojiManager.isOnlyEmojis(parsedToUnicode);
			List<String> extractedEmojiStrings = EmojiParser.extractEmojis(parsedToUnicode);
			if (isOnlyEmojis && extractedEmojiStrings.size() == 1) {
				emojiUnicode = extractedEmojiStrings.get(0);
			} else {
				emojiUnicode = null;
			}
		} else {
			emojiUnicode = parsedToUnicode;
		}

//	    emojiUnicode = parsedToUnicode;

		System.out.println("\temojiUnicode :" + emojiUnicode);

		System.out.println("\temojiUnicode (Java escaped): " + StringEscapeUtils.escapeJava(emojiUnicode));
		System.out.println("\temojiUnicode (URL escaped): "
				+ (emojiUnicode != null ? URLEncoder.encode(emojiUnicode, StandardCharsets.UTF_8.toString()) : null));

		System.out.println("\tparseToHtmlHexadecimal:" + EmojiParser.parseToHtmlHexadecimal(str));
		System.out.println("\tparseToHtmlDecimal:" + EmojiParser.parseToHtmlDecimal(str));
		System.out.println("\tparseToAliases (with fitz): "
				+ EmojiParser.parseToAliases(parsedToUnicode, FitzpatrickAction.PARSE));
		System.out.println("\tparseToAliases (no fitz): " 
				+ EmojiParser.parseToAliases(parsedToUnicode, FitzpatrickAction.REMOVE));
		
		System.out.println("\t*my parseToAliases (with fitz): "
				+ parseToAliases(parsedToUnicode, FitzpatrickAction.PARSE));
		System.out.println("\t*my parseToAliases (no fitz): " 
				+ parseToAliases(parsedToUnicode, FitzpatrickAction.REMOVE));

		String encodedHexString = emojiUnicode != null ? Hex.encodeHexString(emojiUnicode.getBytes("UTF-8")) : null;
		System.out.println("\thex encoded from unicode: " + encodedHexString);
		System.out.println("\thex decoded from hex string: "
				+ (encodedHexString != null ? new String(Hex.decodeHex(encodedHexString)) : null));

		emoji = EmojiManager.getByUnicode(emojiUnicode);
		if (emoji != null) {
			System.out.println("\t* getAliases: " + emoji.getAliases());
			System.out.println("\t* getTags: " + emoji.getTags());
			System.out.println("\t* getUnicode: " + emoji.getUnicode());
			System.out.println("\t* getDescription: " + emoji.getDescription());
			
		}
		System.out.println();
	}

	public static String parseToAliases(String input, final FitzpatrickAction fitzpatrickAction) {
		return EmojiParser.parseFromUnicode(input, uc -> {
			Stream<String> aliases = uc.getEmoji().getAliases().stream();
			switch (fitzpatrickAction) {
			default:
			case PARSE:
				if (uc.hasFitzpatrick()) {
					return aliases.map(a -> ":" + a + "|" + uc.getFitzpatrickType() + ":")
							.collect(Collectors.joining(", "));
				} else {
					return aliases.map(a -> ":" + a + ":")
							.collect(Collectors.joining(", "));
				}
			case REMOVE:
				return aliases.map(a -> ":" + a + ":")
						.collect(Collectors.joining(", "));
			case IGNORE:
				return aliases.map(a -> ":" + a + ":" + uc.getFitzpatrickUnicode())
						.collect(Collectors.joining(", "));
			}
		});
	}
}
