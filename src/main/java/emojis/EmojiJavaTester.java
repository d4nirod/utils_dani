/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package emojis;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import com.vdurmont.emoji.EmojiManager;
import com.vdurmont.emoji.Fitzpatrick;

public class EmojiJavaTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EmojiManager.getAll().forEach(e -> {
				System.out.println(String.format(
						"Unicode: '%s', JavaEscapedUnicode: '%s', isEmoji: %s, gotByUnicode: %s, aliases: [%s] ", 
						e.getUnicode(),
						StringEscapeUtils.escapeJava(e.getUnicode()),
						EmojiManager.isEmoji(e.getUnicode()),
						EmojiManager.getByUnicode(e.getUnicode()) != null,
						StringUtils.join(e.getAliases(), ", ")));
				if (e.supportsFitzpatrick()) {
					System.out.println(String.format(
							"\t[%s]",
							Stream.of(Fitzpatrick.values())
//							.map(f -> StringEscapeUtils.escapeJava(e.getUnicode(f)))
							.map(f -> e.getUnicode(f))
							.collect(Collectors.joining(", ")))
					);
				}
		});

	}

}
