package lambda;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LambdaForEachBreakPointTest {
	public static void main(String[] args) {
		List<String> strings = new ArrayList<>();
		List<Integer> ints = Arrays.asList(0, 1, 2, 3);
//		ints.set(3, null);
		
		ints	.forEach(i -> strings.add(i.toString()));
	}

}
