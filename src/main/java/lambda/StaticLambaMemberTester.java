/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package lambda;

import java.util.ArrayList;
import java.util.List;

public class StaticLambaMemberTester {
	
	private static StringListProvider EMPTY_LIST_PROVIDER = ArrayList<String>::new;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		StaticLambaMemberTester t1 = new StaticLambaMemberTester();
		System.out.println("Try 1");
		t1.doStuff("1");
		
		StaticLambaMemberTester t2 = new StaticLambaMemberTester();
		System.out.println("Try 2");
		t2.doStuff("2");

	}
	
	private void doStuff(String s) {
		List<String> list = EMPTY_LIST_PROVIDER.getList();
		list.add(s);
		list.stream().forEach(System.out::println);
	}

	public interface StringListProvider {
		public List<String> getList();
	}
}
