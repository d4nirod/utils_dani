/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package lambda;


import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import org.apache.commons.lang3.StringUtils;


public class ConditionalConsumerTester {

	public static void main(String[] args) {
		Map<String, String> map = new HashMap<>();
		
		putData("foo", v -> map.put("key1", v.trim()));
		putData(" ", v -> map.put("key2", v.trim()));
		putData(null, v -> map.put("key3", v.trim()));
		putData("bar ", v -> map.put("key4", v.trim()));
		
		for (Entry<String, String> entry : map.entrySet()) {
			System.out.println("Key: " + entry.getKey() + ", Value: " + entry.getValue());
		}
	}
	
	private static void putData(String value, Consumer<? super String> consumer) {
		if (StringUtils.isBlank(value)) {
			consumer.accept(value);
		}
	}

}
