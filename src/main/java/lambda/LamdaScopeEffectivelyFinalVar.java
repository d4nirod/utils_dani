package lambda;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class LamdaScopeEffectivelyFinalVar {

	public static void main(String[] args) {
		localVarDoesNotWork();
		System.out.println("----------------------------------------");
		arrayElementRefDoesWork();
		System.out.println("----------------------------------------");
		atomicIntegerDoesWork();
	}

	private static void localVarDoesNotWork() {
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
		int step = 1;
		list.forEach(i -> {
			int localStep = step;
			System.out.println("Step: " + localStep + " value: " + i);
			localStep++;
		});
		System.out.println("Finished iterating. Step value: " + step);
	}
	
	private static void arrayElementRefDoesWork() {
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
		// This code is legal, as step variable remains “effectively final”. 
		// But the object it references to will NOT have the same state after execution of the lambda!
		
		int[] step = { 0 };
		list.forEach(i -> {
			System.out.println("Step: " + (++step[0]) + " value: " + i);
		});
		System.out.println("Finished iterating. Step value: " + step[0]);
	}
	
	private static void atomicIntegerDoesWork() {
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
		// This code is legal, as step variable remains “effectively final”. 
		// But the object it references to will NOT have the same state after execution of the lambda!
		
		final AtomicInteger step = new AtomicInteger(0);
		list.forEach(i -> {
			System.out.println("Step: " + step.incrementAndGet() + " value: " + i);
		});
		System.out.println("Finished iterating. Step value: " + step.get());
	}

}
