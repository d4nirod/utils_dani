/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package lambda;

import java.util.HashMap;
import java.util.Map;

public class MapComputeIfAbsentAddTester {
	public static void main(String[] args) {
		Map<String, String> map = new HashMap<>();
		String object1 = "blah 1";
		String object2 = null;
		String object1_1 = "blah 1_1";
		
		addToMapIfNotNull("key1", object1, map);
		map.compute("key1", (k, v) -> object1);
		map.compute("key2", (k, v) -> object2);
		map.compute("key1", (k, v) -> object1_1);

		System.out.println(map);
		
		System.out.println("-----------------");
		
		 HashMap<String, String> map2 = new HashMap<>();
		 map2.put("1", "ONE");
		 map2.put("2", null);

		 map2.computeIfAbsent("3", val -> "THREE");
		 map2.computeIfAbsent("4", val -> null);
		 map2.computeIfAbsent("3", val -> "THREE_2");

	    System.out.println(map2);
		
	}
	
	private static void addToMapIfNotNull(String key, String value, Map<String, String> map) {
		map.compute(key, (k, v) -> value);
	}

}
