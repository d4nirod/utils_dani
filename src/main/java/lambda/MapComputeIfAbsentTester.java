/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package lambda;

import static java.util.stream.Collectors.mapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import utils.data.Contact;

public class MapComputeIfAbsentTester {

	public static void main(String[] args) {
		List<Contact> contacts = Arrays.asList(
				new Contact("John Smith", "Acme"),
				new Contact("Joe Bloggs", null),
				new Contact("Jane White", "Acme"),				
				new Contact("Tom Brown", "Acme"),				
				new Contact("Fred Public", "NewCo"),				
				new Contact("Simon Private", "NewCo"),			
				new Contact("Simon Private", "NewCo")   //Check for dupes
		);
		Map <String, List<String>> byCompany = new HashMap<>();
		for (Contact c : contacts) {
			if (c.getCompany() != null) {
				if (!byCompany.containsKey(c.getCompany())) {
					byCompany.put(c.getCompany(), new ArrayList<>(Arrays.asList(c.getName())));
				} else {
					byCompany.get(c.getCompany()).add(c.getName());
				}
			}
		}
		printMap("*** CASE 1", byCompany);
		

		Map <String, List<String>> byCompany2 = new HashMap<>();
		contacts.stream().filter(c -> c.getCompany() != null)
			.forEach(c -> byCompany2.computeIfAbsent(c.getCompany(), members -> new ArrayList<>()).add(c.getName()));
		printMap("*** CASE 2", byCompany);
		
		Map <String, List<String>> byCompany3 = contacts.stream()
				.filter(c -> c.getCompany() != null)
				.collect(Collectors.groupingBy(Contact::getCompany,
						mapping(Contact::getName, Collectors.toList())));
		printMap("*** CASE 3", byCompany3);
	}

	private static void printMap(String message, Map<String, List<String>> byCompany) {
		System.out.println(message);
		byCompany.entrySet().forEach(e -> System.out.println(
				"Company: " + e.getKey() + " Members: " + StringUtils.joinWith(", ", e.getValue())));
		System.out.println("");
		
	}

}
