package json;

import java.util.Arrays;
import java.util.List;

import org.json.simple.JSONValue;

public class JsonTest {

	public static void main(String[] args) {
		List<String> lista = Arrays.asList(new String[]{"123", "456", "789"});
		String jsonText = JSONValue.toJSONString(lista);
		System.out.println(jsonText);

	}

}
