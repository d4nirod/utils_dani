package aop;
/*
 *
 * Copyright Farmers WIFE S.L. All Rights Reserved.
 * 
 */



public class TestMySadAspect {

	public static void main(String[] args) {
		doubleUp("aa", 2, true);
		doubleUp("bb", 3, true);
		doubleUp("cc", 4, false);
		doubleUp("dd", 5, false);
		printIt("asdfasdfasdf", true);
	}

	static void doubleUp(String s, int x, boolean b) {
		printIt(s + ": " + x*2, b);
	}
	
	static void printIt(String s, boolean b) {
		System.out.print(s);
		if (b) System.out.print("\n");
	}
}
