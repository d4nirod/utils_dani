package aop;
/*
 *
 * Copyright Farmers WIFE S.L. All Rights Reserved.
 * 
 */



public aspect MySadAspect {
	//WORKS
//	pointcut entry(String s, int i, boolean b): call(* TestMySadAspect.doubleUp(String, int, boolean)) && args(s, i, b) && if(i > 3);
//    pointcut writing(): call(void TestMySadAspect.printIt(..)) && ! within(MySadAspect);
//    
//    @SuppressWarnings("static-method")
//	after(String s, int i, boolean b): writing() && cflow(entry(s, i, b)) {
//        System.out.println(" [ADVISED: current arg is " + i + "]");
//    }
//	

// WORKS TOO
//	pointcut entry(int i): call(* TestMySadAspect.doubleUp(..)) && args(*, i, *) && if(i > 3);
//	pointcut writing(): call(void TestMySadAspect.printIt(..)) && ! within(MySadAspect);
//	
//	@SuppressWarnings("static-method")
//	after(int i): writing() && cflow(entry(i)) {
//		System.out.println(" [ADVISED: current arg is " + i + "]");
//	}
	
	pointcut entry(int i): call(* TestMySadAspect.doubleUp(..)) && args(.., i, *) && if(i > 3);
    pointcut writing(): call(void TestMySadAspect.printIt(..)) && ! within(MySadAspect);
    
    @SuppressWarnings("static-method")
	after(int i): writing() && cflow(entry(i)) {
        System.out.println(" [ADVISED: current arg is " + i + "]");
    }
	
	
	/* used to work...
	 	pointcut entry(int i): call(* TestMySadAspect.doubleUp(int, ..)) && args(i, ..) && if(i > 3);
	    pointcut writing(): call(void TestMySadAspect.printIt(..)) && ! within(MySadAspect);
	    
	    after(int i): writing() && cflow(entry(i)) {
	        System.out.println(" [ADVISED: current arg is " + i + "]");
	    }
	 */
	
	
}
