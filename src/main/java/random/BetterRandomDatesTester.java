/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package random;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.time.DateUtils;

public class BetterRandomDatesTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

		Set<Integer> days = new HashSet<>();
		Set<Integer> hours = new HashSet<>();
		Set<Integer> minutes = new HashSet<>();
		Set<Integer> seconds = new HashSet<>();
		Date now = new Date();
		for (int i = 0; i < 10; i++) {
			LocalDateTime date = getNextRandomDate();
			boolean dayExisted = !days.add(date.getDayOfMonth());
			boolean hourExisted = !hours.add(date.getHour());
			boolean minuteExisted = !minutes.add(date.getMinute());
			boolean secondExisted = !seconds.add(date.getSecond());
			System.out.print(date.format(dateTimeFormatter));
			if (dayExisted ) {
				System.out.print(" Repeated day found: " + date.getDayOfMonth());
			}
			if (hourExisted) {
				System.out.print(" Repeated hour found: " + date.getHour());
			}
			if (minuteExisted) {
				System.out.print(" Repeated minute found: " + date.getMinute());
			}
			if (secondExisted) {
				System.out.print(" Repeated second found: " + date.getSecond());
			}
			System.out.println("\n");
		}
	}
	
	public static LocalDateTime getNextRandomDate() {
		// Declare DateTimeFormatter with desired format
	    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

	    // Save current LocalDateTime into a variable
	    LocalDateTime now = LocalDateTime.now();

	    // Format LocalDateTime into a String variable and print
	    

	    //Get random amount of days between 5 and 10
	    
	    
//		long randomAmountOfDays = random.nextLong(10 - 5 + 1) + 5;

	    // Add randomAmountOfDays to LocalDateTime variable we defined earlier and store it into a new variable
	    LocalDateTime future = now.plusDays(ThreadLocalRandom.current().nextLong(365));
	    future = future.plusHours(ThreadLocalRandom.current().nextLong(24));
	    
	    future = future.plusMinutes(ThreadLocalRandom.current().nextLong(60));
	    future = future.plusSeconds(ThreadLocalRandom.current().nextLong(60));
	    
//	    System.out.println("Now: " + now.format(dateTimeFormatter) + " future: " + future.format(dateTimeFormatter));
	    return future;
	}

}
