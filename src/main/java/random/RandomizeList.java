package random;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RandomizeList {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();
		for (int i = 0; i < 100; i++) list.add(i);
		
		for (int j = 0; j < 11; j++) {
			Collections.shuffle(list);
			System.out.println(list);
		}

	}

}
