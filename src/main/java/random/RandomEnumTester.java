/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package random;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.RandomUtils;

public class RandomEnumTester {
	public static final int MAX = 10000;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Map<Days, AtomicInteger> map = new HashMap<>();
		for (Days d : Days.values()) { 
			map.put(d, new AtomicInteger(0));
		}
		
		for (int i = 0; i < MAX; i++) {
			final Days d = randomEnum(Days.class);
//			System.out.println(String.format("[%d] %s", i, d));
			map.get(d).incrementAndGet();
		}
		for (Entry<Days, AtomicInteger> entry : map.entrySet()) {
			Days day = entry.getKey();
			int occurrences = entry.getValue().get();
			System.out.println(String.format(
					"%s: %.2f%% (%d/%d)", 
					day, (double) MAX / (double) occurrences, MAX, occurrences));
		}

	}
	
	public static <T extends Enum<?>> T randomEnum(Class<T> clazz) {
	    	int x = RandomUtils.nextInt(0, clazz.getEnumConstants().length);
	        return clazz.getEnumConstants()[x]; 
	}
	
   enum Days{
       MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;
   }

}
