package random;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Prints an integer between min and max, inclusive.
 * @see java.util.Random#nextInt(int)
 */
public class RandomInt {

	public static void main(String[] args) {
		int min = 0;
		int max = 1;
		//Usually this should be a field rather than a method variable so it's not re-seeded every call.
		Random rand = new Random();
		List<Integer> ls = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			//nextInt is normally exclusive of the top value so add 1 to make it inclusive
			// The difference between min and max can be at most Integer.MAX_VALUE - 1
			ls.add(rand.nextInt((max - min) + 1) + min);
		}
		System.out.println(ls);
		ls.clear();
		
		// Now just get a list of ints from min to max
		min = 5;
		max = 10;
		// nextInt is normally exclusive of the top value, so add 1 to make it inclusive
        for (int i = min; i <= max; i++) {
        		ls.add(ThreadLocalRandom.current().nextInt(min, max + 1));
        }
        System.out.println(ls);

	}
}
