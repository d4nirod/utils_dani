package random;

public class RandomAlphaNumericString {
    
    private static final String ALPHA_NUM ="0123456789abcdef";

    public static String getAlphaNumeric(int len) {
        StringBuffer sb = new StringBuffer(len);
        for (int i=0; i<len; i++) {
            int ndx = (int)(Math.random()*ALPHA_NUM.length());
            sb.append(ALPHA_NUM.charAt(ndx));
        }
        return sb.toString();
        }

    public static void main(String[] args) {
        int len = 8;
        System.out.println(len+" character long: " +getAlphaNumeric(len));
      
        //Another method
        String alphaNumerics = "qwertyuiopasdfghjklzxcvbnm1234567890";
        String t = "";
        for (int i=0; i<len; i++) {
            t += alphaNumerics.charAt((int) (Math.random() * alphaNumerics.length()));
        }
        System.out.println("with new code: " + t);        
    }
}

