/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package random;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.time.DateUtils;

public class RandomDatesTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// System.out.println(cal.get(Calendar.YEAR) + "-" +
		// (cal.get(Calendar.MONTH) + 1) + "-" +
		// cal.get(Calendar.DAY_OF_MONTH));
		Date now = new Date();
		for (int i = 0; i < 10; i++) {
			System.out.println(getRandomDate() + " - " + getRandomDate(now, DateUtils.addYears(now, 10)));

		}
	}

	public static Date getRandomDate() {
		GregorianCalendar cal = new GregorianCalendar();
		int year = RandomUtils.nextInt(2019, 2029);
		cal.set(Calendar.YEAR, year);

		int dayOfYear = RandomUtils.nextInt(1, cal.getActualMaximum(Calendar.DAY_OF_YEAR));

		cal.set(Calendar.DAY_OF_YEAR, dayOfYear);
		return Date.from(cal.toInstant());
	}

	public static Date getRandomDate(Date startExcl, Date endIncl) {
		return Date.from(Instant.ofEpochMilli(
				ThreadLocalRandom.current().nextLong(DateUtils.addDays(startExcl, 1).getTime(), endIncl.getTime())));
	}

}
