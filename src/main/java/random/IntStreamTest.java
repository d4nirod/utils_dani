package random;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class IntStreamTest {

	public static void main(String[] args) {
		final int min = 5;
		final int max = 10;
		IntStream range = IntStream.rangeClosed(min, max);
		List<Integer> ls = range.boxed().collect(Collectors.toList());

        //perform a random shuffle  using the Collections Fisher-Yates shuffle
        Collections.shuffle(ls);
        System.out.println(ls);
        
        
	}

}
