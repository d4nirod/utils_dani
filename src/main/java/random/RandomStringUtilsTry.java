package random;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomStringUtilsTry {
	public static void main(String[] args) {
		Set<String> strings = new HashSet<>();
		for(int j = 0; j < 99; j++) {
			for(int i = 0; i < 99; i++) {
				String str = RandomStringUtils.randomAlphanumeric(8);
				if (strings.add(str)) {
					System.out.println("Added: " + str);
				} else {
					System.out.println("DUPE:" + str);
				}
			}
		}
	}
}
