package vss;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class VSSReportFormatter {

	/**
	 * @param args
	 */
	static StringBuilder dir = new StringBuilder();
	static StringBuilder file = new StringBuilder();
	static String usuario;
	static String fecha;
	static boolean ignore = false;

	public static void main(String[] args) throws IOException {
		BufferedReader f = new BufferedReader(new FileReader(args[0]));
		String line;
		while ((line=f.readLine())!=null) {
			if (esNuevo(line)) {
				if (file.length()>0) escribir();
				usuario = line.substring(24,41).trim();
				fecha = line.substring(41,57).trim();
				file.append(line.substring(0,23));
				dir.append(line.substring(59,87));
				ignore = false;
			} else continuar(line);
		}
		if (file.length()>0) escribir();
		f.close();
	}

	private static void escribir() {
		System.out.println(String.format("%1$s\\%2$s\t%3$s\t%4$s",dir.toString().trim(),file.toString().trim(),usuario,fecha));
		dir = new StringBuilder();
		file = new StringBuilder();

	}

	private static void continuar(String line) {
		if ((line.length()<87) || (line.startsWith("$")) || ignore) {
			ignore = true;
			return;
		}
		file.append(line.substring(1,23));
		dir.append(line.substring(60,87));
	}

	private static boolean esNuevo(String line) {
		if (line.length()<87) return false;
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy  hh:mm");
		try {
			String str = line.substring(41,57);
			formatter.parse(str);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

}
