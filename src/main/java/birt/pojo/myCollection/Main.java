package birt.pojo.myCollection;

import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		A objA0 = new A("nameA0", getBList(getCList()));
		A objA1 = new A("nameA1", getBList(getCList()));
		A objA2 = new A("nameA2", getBList(getCList()));
		List<A> objAList = new ArrayList<A>();
		objAList.add(objA0);
		objAList.add(objA1);
		objAList.add(objA2);
		
		for (A a : objAList) {
			for (B b : a.getbObjectList()) {
				for (C c : b.getbObjectList()) {
					System.out.println(a.getName() + " " + b.getName() + " " + c.getName());
				}
			}
		}
	}
	
	public static List<C> getCList() {
		C objC0 = new C("cName0");
		C objC1 = new C("cName1");
		C objC2 = new C("cName2");
		List<C> objCList = new ArrayList<C>();
		objCList.add(objC0);
		objCList.add(objC1);
		objCList.add(objC2);
		
		return objCList;
	}
	
	public static List<B> getBList(List<C> objCList) {
		B objB0 = new B("nameB0", objCList);
		B objB1 = new B("nameB1", objCList);
		B objB2 = new B("nameB2", objCList);
		List<B> objBList = new ArrayList<B>();
		objBList.add(objB0);
		objBList.add(objB1);
		objBList.add(objB2);
		
		return objBList;
	}
}