package birt.pojo.myCollection;

import java.util.ArrayList;
import java.util.List;

public class A {
	private String name;
	
	private List<B> bObjectList = new ArrayList<B>();

	public A(String name, List<B> bObjectList) {
		super();
		this.name = name;
		this.bObjectList = bObjectList;
	}

	public List<B> getbObjectList() {
		return bObjectList;
	}

	public void setbObjectList(List<B> bObjectList) {
		this.bObjectList = bObjectList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
