package birt.pojo.myCollection;

public class C {
	private String name;

	public C(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
