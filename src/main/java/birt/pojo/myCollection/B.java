package birt.pojo.myCollection;

import java.util.ArrayList;
import java.util.List;

public class B {
	private String name;

	private List<C> cObjectList = new ArrayList<C>();

	public B(String name, List<C> cObjectList) {
		super();
		this.name = name;
		this.cObjectList = cObjectList;
	}

	public List<C> getbObjectList() {
		return cObjectList;
	}

	public void setbObjectList(List<C> bObjectList) {
		this.cObjectList = bObjectList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}