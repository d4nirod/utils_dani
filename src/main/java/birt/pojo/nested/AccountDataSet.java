package birt.pojo.nested;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class AccountDataSet {
	
	private int customerNumber;
	private Iterator<Account> itr;

//	public AccountDataSet(int customerNumber) {
//		super();
//		this.setCustomerNumber(customerNumber);
//	}
	
	public AccountDataSet() {
		super();
	}

	
	public List<Account> getAccounts() {
		List<Account> accounts = new ArrayList<Account>();
		accounts.add(new Account(10, "Cuenta n*mina", 3000, this.customerNumber));
		accounts.add(new Account(20, "Cuenta ahorro", 10000, this.customerNumber));
		accounts.add(new Account(30, "Cuenta gastos", 2500, this.customerNumber));
		return accounts;
	}
	
	public void open(Object obj, Map<String, Object> map) {
		itr = getAccounts().iterator();
	}

	public Object next() {		
		if (itr.hasNext())
			return itr.next();
		else
			return null;
	}

	public void close() {
		itr = null;

	}

	public int getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}

}
