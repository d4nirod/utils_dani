package birt.pojo.nested;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CustomerDataSet {

	private Iterator<Customer> itr;

	public List<Customer> getCustomers() {
		List<Customer> customers = new ArrayList<Customer>();

		Customer c = new Customer(100, "Mortadelo", "Barcelona",  "Catalunya", "Spain", 1500 );
		AccountDataSet accounts = new AccountDataSet();
		accounts.setCustomerNumber(c.getNumber());
		c.setAccounts(accounts);
		customers.add(c);

		c = new Customer(101, "Pepe Gotera", "Madrid",  "Madrid", "Spain", 2000 );
		accounts = new AccountDataSet();
		accounts.setCustomerNumber(c.getNumber());
		c.setAccounts(accounts);
		customers.add(c);

		c = new Customer(102, "Carpanta", "Palma de Mallorca",  "Baleares", "Spain", 2500 );
		accounts = new AccountDataSet();
		accounts.setCustomerNumber(c.getNumber());
		c.setAccounts(accounts);
		customers.add(c);

		return customers;
	}

	public void open(Object obj, Map<String, Object> map) {
		itr = getCustomers().iterator();
	}

	public Object next() {		
		if (itr.hasNext())
			return itr.next();
		else
			return null;
	}

	public void close() {
		itr = null;
	}
}