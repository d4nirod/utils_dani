package birt.pojo.nested;

public class Account {
	private int number;
	private String name;
	private int amount;
	private int customerNumber;
	
	public Account(int number, String name, int amount, int customerNumber) {
		super();
		this.number = number;
		this.name = name;
		this.amount = amount;
		this.customerNumber = customerNumber;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}

}
