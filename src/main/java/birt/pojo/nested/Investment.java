package birt.pojo.nested;

public class Investment {
	private int number;
	private String name;
	private int amount;
	
	public Investment(int number, String name, int amount) {
		super();
		this.number = number;
		this.name = name;
		this.amount = amount;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
}
