package birt.pojo.nested;

public class Customer {
	private int number;
	private String name;
	private String city;
	private String state;
	private String country;
	private int creditLimit;
	
	private AccountDataSet accounts;
//	private List<Investment> investments;
		
	
	public Customer(int number, String name, String city, String state, String country, int creditLimit) {
		super();
		this.number = number;
		this.name = name;
		this.city = city;
		this.state = state;
		this.country = country;
		this.creditLimit = creditLimit;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(int creditLimit) {
		this.creditLimit = creditLimit;
	}
	public AccountDataSet getAccounts() {
		return accounts;
	}
	public void setAccounts(AccountDataSet accounts) {
		this.accounts = accounts;
	}
//	public List<Investment> getInvestments() {
//		return investments;
//	}
//	public void setInvestments(List<Investment> investments) {
//		this.investments = investments;
//	}

}
