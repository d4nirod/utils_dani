package properties;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class PropertiesListTest {

	public static void main(String[] args) {
		
		Properties prop = new Properties();
		StringWriter sw = new StringWriter();
//		PrintWriter writer = new PrintWriter(System.out);
		PrintWriter writer = new PrintWriter(sw);
		
		// add some properties
		prop.put("Height", "200");
		prop.put("Width", "150");
		prop.put("Scannable", "true");
		prop.put("Editable", null);
		
		// print the list with a PrintWriter object
		prop.list(writer);
//		prop.list(sw);
		System.out.println(sw.toString());		
		
		// flush the stream
		writer.flush();
		
		System.out.println(asDebuggingString(prop, null));
		

	}
	
	public static String asDebuggingString(final Properties props, List<String> exclusions) {
		if (props == null) return null;
		if (exclusions == null) exclusions = Collections.EMPTY_LIST;
		StringBuilder sb = new StringBuilder();
		for (final String name : props.stringPropertyNames()) {
			if (!exclusions.contains(name)) {
				sb.append(name).append("=").append(props.getProperty(name)).append(", ");
			}
		}
		String result = sb.toString();
		if (!result.isEmpty()) {
			result = result.substring(0, result.lastIndexOf(", "));
		}
		return result;
	}

}
