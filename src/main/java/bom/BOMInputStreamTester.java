package bom;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;

public class BOMInputStreamTester {

	public static void main(String[] args) {
		String bomFile = "c:/temp/sepa/test/utf8-bom.xml";
		String noBomFile = "c:/temp/sepa/test/utf8-nobom.xml";
		System.out.println("***BOM file***");
		readFile(bomFile, false);
		readFile(bomFile, true);
		
		System.out.println("");
		System.out.println("***No-BOM file***");
		readFile(noBomFile, false);
		readFile(noBomFile, true);
		

	}

	private static void readFile(String file, boolean includeBom) {
		BOMInputStream bomIS  = null;
		
		try {
			if(includeBom)
				bomIS = new BOMInputStream(new FileInputStream(file), true );
			else
				bomIS = new BOMInputStream(new FileInputStream(file));   //excluye el bom del stream
		    
		    ByteOrderMark bom = bomIS.getBOM();
		    		    
//		    InputStreamReader readerBom = new InputStreamReader(new BufferedInputStream(bomIS), "UTF-8");
		    //use reader
		    
		    byte[] data = IOUtils.toByteArray(bomIS);
		    
		    int n = 10;
		    int posTag = -1;
		    boolean foundTag = false;
		    System.out.printf("File first %d chars BOM %s: ", n, includeBom?"included":"excluded");
		    for(int i=0; i< n-1 && i<data.length; i++){
		    	System.out.printf("%x ", data[i]);
		    	if(!foundTag && data[i] == 0x3c){
		    		posTag = i;
		    		foundTag = true;
		    	}
		    }
		    System.out.println( " found first tag marker (<) at position: "+ (foundTag?posTag:"none"));

		    
		} catch (IOException e) {
			e.printStackTrace();
			
		} finally {
			if(bomIS!=null)   try{ bomIS.close();   }catch(IOException e){}
		}
	}
}
