package console;
import java.io.Console;

public class ConsoleInputPassword {


    public static void main(String[] args) throws Exception {
//    	TextDevice terminal = TextDevices.defaultTextDevice();
        Console terminal = System.console();
        if (terminal==null) {
            throw new Exception("No se pudo instanciar la consola.");
        } // end if
 
        String user= new String (terminal.readLine("Usuario:"));
        String pass= new String (terminal.readPassword("Password:"));
        
        System.out.printf("usr:%s\npwd:%s\n", user, pass);
        
        
        Console cons;
        char[] passwd;
        if ((cons = System.console()) != null &&
            (passwd = cons.readPassword("[%s]", "Password:")) != null) {
        	System.out.println(passwd);
            java.util.Arrays.fill(passwd, ' ');
        }
 
    }
}