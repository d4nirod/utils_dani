package httpclient;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Random;
import java.util.Vector;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.pool.PoolStats;
import org.apache.http.util.ByteArrayBuffer;


public class MultiThreadHttpClientTest implements Runnable {
	
	private final static int MAX_THREADS = 4;
	private final static int REPEATS = 1;
	public final static int READ_BYTES = 1024;
	private final static String uriStr = "http://teleintegra.bancamarch.es/htmlVersion/login.jsp";
	private URI uri = null; 

	public void run() {
		
		String tnumber = Thread.currentThread().getName();
		String tlabel = "[Thread #"+ tnumber+"]";
		System.out.printf("%s Started.\n", tlabel);
		OurHttpClient httpclient = OurHttpClient.getInstance();
		HttpGet httpget = null;
		try {
//				URIBuilder builder = new URIBuilder("http://pc004154.bancamarch.es:8080/htmlVersion/login.jsp");
//				URIBuilder builder = new URIBuilder("http://telemarch.bancamarch.es:8080/htmlVersion/login.jsp");
			URIBuilder builder = new URIBuilder(uriStr);
			Random rand  = new Random(); 
			builder.setParameter("idioma", ""+rand.nextInt(4));
			uri = builder.build();			
			httpget = new HttpGet(uri);			
			
//				BasicResponseHandler responseHandler = new BasicResponseHandler(); //returns strings only so use own impl.				
//				ResponseHandler<byte[]> handler = new ResponseHandler<byte[]>() {
//					public byte[] handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
//						HttpEntity entity = response.getEntity();
//						if (entity != null) {
//							return EntityUtils.toByteArray(entity);
//						} else {
//							return null;
//						}
//					}
//				};
			
			ResponseHandler<byte[]> handler = new ResponseHandler<byte[]>() {
				public byte[] handleResponse(HttpResponse response) throws IOException {
					printStats("[Thread #"+ Thread.currentThread().getName()+"]");
					HttpEntity entity = response.getEntity();
					if (entity == null) {
						throw new IllegalArgumentException("HTTP entity may not be null");
					}
					InputStream is = entity.getContent();
					if (is == null) return null;
					
					try {
						if (entity.getContentLength() > Integer.MAX_VALUE) {
							throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");
						}
						int i = (int) entity.getContentLength();
						if (i < 0) {
							i = 4096;
						}
						ByteArrayBuffer buffer = new ByteArrayBuffer(i);
						byte[] tmp = new byte[READ_BYTES];
						int l;
						while ((l = is.read(tmp)) != -1) {
							buffer.append(tmp, 0, l);
							try {
								Thread.sleep(1500);   //ralentizamos la transfer
								
							} catch (InterruptedException e) {e.printStackTrace();}
						}
						return buffer.toByteArray();
						
					} finally {
						is.close();
					}
				}
			};
		
			System.out.printf("%s Executing GET %s\n", tlabel, httpget.getURI());
			byte[] res = httpclient.execute(httpget, handler); 
			printStats(tlabel);
							

			System.out.printf("%s Request completed. %d bytes read. Finished thread\n", tlabel, res.length);							
			
		} catch (IOException e) {
			printStats(tlabel);
			if(httpget != null) httpget.abort();
			System.out.printf("%s Request aborted due to error: %s\n", tlabel, e.getMessage());
			printStats(tlabel);
			
		} catch (URISyntaxException e) {			
			System.out.printf("%s Error: %s", tlabel, e.getMessage());
			
		}finally {		
			Thread.yield();
		}
	}
	
	public void printStats(String threadname){
		PoolingClientConnectionManager cm = (PoolingClientConnectionManager)OurHttpClient.getInstance().getConnectionManager();
		PoolStats stats = cm.getStats(new HttpRoute(new HttpHost(uri.getHost()) ));
		System.out.printf("%s Route stats: max: %02d, avail: %02d, leased: %02d, pending: %02d\n", 
				threadname, stats.getMax(), stats.getAvailable(), stats.getLeased(), stats.getPending());							
		
		stats = cm.getTotalStats();		
		System.out.printf("%s Total stats: max: %02d, avail: %02d, leased: %02d, pending: %02d\n", 
				threadname, stats.getMax(), stats.getAvailable(), stats.getLeased(), stats.getPending());							
	}

	
	public static void main(String[] args) {
		//creamos una colecci*n de hilos
		Vector<Thread> threads = new Vector<Thread>(MAX_THREADS);
		
		for(int i=0; i< REPEATS; i++){
			
			for(int j=0; j<MAX_THREADS; j++){
				threads.add(new Thread(new MultiThreadHttpClientTest()));
				threads.get(j).setName(""+(j+1));
			}
			Random rnd = new Random();
			System.out.printf("***Run %d of %d\n", i+1, REPEATS);			
			for(Thread t : threads){
				//esperamos un corto tiempo aleatorio para empezar
				try {
					Thread.sleep(rnd.nextInt(20)*10);
					
				} catch (InterruptedException e) {	
					System.out.println(t.getName() + ": "+e.getMessage());
					e.printStackTrace();
				}				
				t.start();
			}
			//esperamos a que todos los threads acaben
			for(Thread t : threads){
				if(t.isAlive()){
					try {
						t.join();
						
					} catch (InterruptedException e) { System.out.println(t.getName() + ": "+e.getMessage()); }
				}
			}
			
			System.out.println("All threads finished.");
			threads.clear();
		}
		System.out.println("***End runs");
		try {
			System.out.println("#################Sleeping before shutdown");
			Thread.sleep(20000);
		} catch (InterruptedException e) {e.printStackTrace();}
		OurHttpClient.shutdown();
		System.out.println("Pool shut down.");
	}

}
