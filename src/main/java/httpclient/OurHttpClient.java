package httpclient;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.pool.PoolStats;

public class OurHttpClient extends DefaultHttpClient {

	private static final int MAX_CONNS_ROUTE = 3;
	private static final int MAX_CONNS_TOTAL = 50;
	private static final int CONNECTION_TIMEOUT = 50000;
	private static final int SO_TIMEOUT = 10000;
	private static PoolingClientConnectionManager cm;
	private static IdleConnectionEvictor connEvictor;
	
	private static class HttpClientHolder {
		
		static {  //static initializer
			System.out.printf("[%s] HttpClientHolder static initializer called\n", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").format(Calendar.getInstance().getTime()));
			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
			schemeRegistry.register(new Scheme("https", 443, SSLSocketFactory.getSocketFactory()));
			cm = new PoolingClientConnectionManager(schemeRegistry);
			cm.setDefaultMaxPerRoute(MAX_CONNS_ROUTE);
			cm.setMaxTotal(MAX_CONNS_TOTAL);
			
		}
		
		public static OurHttpClient instance = new OurHttpClient(cm);				
	}
	
	public static OurHttpClient getInstance(){
		return HttpClientHolder.instance;
	}

	private OurHttpClient(ClientConnectionManager connMgr) { // singleton
		super(connMgr);
		System.out.printf("[%s] OurHttpClient constructor called\n", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").format(Calendar.getInstance().getTime()));
		this.setRedirectStrategy(new LaxRedirectStrategy()); 
		//redirect all HEAD, GET and POST requests. relax restrictions on automatic redirection of POST methods imposed by HTTP specification.
		HttpParams httpParams = this.getParams();
		httpParams.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, CONNECTION_TIMEOUT);
		httpParams.setParameter(CoreConnectionPNames.SO_TIMEOUT, SO_TIMEOUT);
		
		
//		HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//	    HttpProtocolParams.setContentCharset(httpParams, HTTP.ISO_8859_1);
		
		//Create the connection evictor 
		connEvictor = new IdleConnectionEvictor();			
		connEvictor.start();			
	}
	
	public static void shutdown() {
		connEvictor.shutdown();
		try {
			connEvictor.join();
		} catch (InterruptedException e) {}
		
		HttpClientHolder.instance.getConnectionManager().shutdown();
		
	}
	
	public static class IdleConnectionEvictor extends Thread {
//		private final ClientConnectionManager connMgr;
		private volatile boolean shutdown;

		public IdleConnectionEvictor() {
			super();
//			this.connMgr = connMgr;
		}

		@Override
		public void run() {
			setName("IdleConnectionEvictor");
			try {
				while (!shutdown) {
					synchronized (this) {
						wait(5000);
						PoolStats stbefore = cm.getTotalStats();
						// Close expired connections
						cm.closeExpiredConnections();
						// Optionally, close connections
						// that have been idle longer than 5 sec
						cm.closeIdleConnections(10, TimeUnit.SECONDS);
						
						PoolStats stafter = cm.getTotalStats();
						if(isDifferentStats(stbefore, stafter)){
							System.out.println("[IdleConnectionEvictor] Pool stats BEFORE: "+formatStats(stbefore));
							System.out.println("[IdleConnectionEvictor] Pool stats AFTER : "+formatStats(stafter));
						}
					}
				}
			} catch (InterruptedException ex) {
				// terminate
			}
		}
		
		boolean isDifferentStats(PoolStats a, PoolStats b){
			if(      a.getAvailable()!= b.getAvailable()) return true;
			else if( a.getLeased()   != b.getLeased()   ) return true;
			else if( a.getPending()  != b.getPending()  ) return true;
			else return false;
		}

		private String formatStats(PoolStats st) {						
			return String.format("max: %02d, avail: %02d, leased: %02d, pending: %02d", 
				st.getMax(), st.getAvailable(), st.getLeased(), st.getPending());
		}

		public void shutdown() {
			shutdown = true;
			synchronized (this) {
				notifyAll();
			}
		}
	}
}
