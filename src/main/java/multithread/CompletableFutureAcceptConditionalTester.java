/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package multithread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.Consumer;

import org.apache.commons.lang3.RandomUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CompletableFutureAcceptConditionalTester {
	private static Logger log = LogManager.getLogger();
	private ThreadPoolExecutor executor;
	
	public CompletableFutureAcceptConditionalTester(int nThreads) {
		executor = new ThreadPoolExecutor(3, nThreads, 100, TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>());
		executor.allowCoreThreadTimeOut(true);
//		executorService = Executors.newFixedThreadPool(nThreads);
	}
	
	private int getExecutorPoolSize() {
		return executor.getPoolSize();
	}

	public static void main(String[] args) {
		
		CompletableFutureAcceptConditionalTester t = new CompletableFutureAcceptConditionalTester(10);
		List<CompletableFuture<?>> futures = new ArrayList<>();
		AtomicInteger i = new AtomicInteger(0);
		log.info(String.format("[%s] All futures created. Classloader: %s", t.getThreadInfo(), Thread.currentThread().getContextClassLoader()));
		while(i.get() < 10) {
			int id = i.getAndIncrement();
			futures.add(t.toStringIfEvenAsync(id, RandomUtils.nextInt(), s -> t.printStringCallback(id, s)));
		}
//		t.toStringIfTrueAsync(true, s -> t.printString(1, s));
//		t.toStringIfTrueAsync(false, s -> t.printString(2, s));

		log.warn(String.format("[%s] All futures created", t.getThreadInfo()));
		
//		CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(
//				futures.stream().toArray(CompletableFuture<?>[]::new));
//		try {
//			combinedFuture.get();
//		} catch (InterruptedException | ExecutionException e) {
//			log.warn(String.format("[%s] Error in main: %s", Thread.currentThread().getName(), e.getMessage()));
//			Thread.currentThread().interrupt();
//		}
		
//		futures.forEach(CompletableFuture::join);

		while (t.getExecutorPoolSize() > 0) {
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();  // set interrupt flag
				log.warn("Thread interrupted", e);
//				e.printStackTrace();
			}
		}
		log.warn(String.format("[%s] Exiting", t.getThreadInfo()));
		
	}

	private String getThreadInfo() {
		return String.format("%s - pool: %d, core: %d, active: %d, queue: %d", 
				Thread.currentThread().getName(), executor.getPoolSize(), 
				executor.getCorePoolSize(), executor.getActiveCount(), executor.getQueue().size());
	}
	
	public CompletableFuture<Void> toStringIfEvenAsync(int id, int n, Consumer<String> callback) {
		return CompletableFuture.supplyAsync(() -> {

			String result = toStringIfEven(n);
			if (result != null) {
				return result;
			}
			throw new CompletionException(new Exception("[id: " + id + "] Result is odd"));
		}, executor).whenComplete((r, ex) -> {
			log.info(String.format("[%s] Classloader: %s", getThreadInfo(), Thread.currentThread().getContextClassLoader()));
			if (ex != null) {
				log.error(String.format("[%s] [id: %s] [n: %d] Error: string is '%s': %s", 
						getThreadInfo(), id, n, r, ex.getMessage()));
			}
		}).thenAccept(callback);
		
//		CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> toStringIfEven(n), executor);
//		return future.thenAccept(s -> {
//			if (s != null) {
//				log.warn(String.format("[%s] [id: %s] [n: %d] OK: string is '%s'", 
//						getThreadInfo(), id, n, s));
//				callback.accept(s);
//			} else {
//				log.error(String.format("[%s] [id: %s] [n: %d] Error: string is '%s'", 
//						getThreadInfo(), id, n, s));
//			}
//		});

	}
	
	public String toStringIfEven(int n) {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();  // set interrupt flag
			log.warn(String.format("[%s] Thread interrupted", getThreadInfo()), e);
//			e.printStackTrace();
		}
		if (n % 2 == 0) {
			return Integer.toString(n);
		}
//		throw new RuntimeException("Parameter is not even");
		return null;
	}
	
	public void printStringCallback(int id, String s) {
		log.warn(String.format("[%s] [id: %s] Callback result: %s", 
				getThreadInfo(), id, s));
	}

}
