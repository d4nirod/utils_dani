/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package multithread;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinPool.ForkJoinWorkerThreadFactory;

public class ForkJoinPoolTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(ForkJoinPool.defaultForkJoinWorkerThreadFactory.getClass());
		ForkJoinPool commonPool = ForkJoinPool.commonPool();
		System.out.println(commonPool.getFactory().getClass());
	}

}
