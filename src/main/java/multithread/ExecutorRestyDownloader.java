package multithread;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import us.monoid.web.Resty;

public class ExecutorRestyDownloader {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		final String url = "https://teledes.bancamarch.es/htmlVersion/csbweb/csbwebdl.jsp?submit=Descargar&archivoId=";
		List<String> ids = Arrays.asList(new String[]{"167", "164", "162", "149"});

		ExecutorService pool = Executors.newFixedThreadPool(2);
		List<Callable<File>> tasks = new ArrayList<Callable<File>>(ids.size());
		for (final String id : ids) {
			tasks.add(new Callable<File>() {
				public File call() throws Exception {
					return new Resty().bytes("https://teledes.bancamarch.es/htmlVersion/csbweb/csbwebdl.jsp?submit=Descargar&archivoId="+id)
							.save(new File("c:/Dani/tmp/fichero_"+id, ".xml") );
				}
			});
		}
		List<Future<File>> results = pool.invokeAll(tasks);
//		for (Future<File> ff : results) {
//			System.out.println(ff.get());
//		}
	}

}
