package multithread;

class FirstThreadingExample {
	public static void main(String[] args) {
		// The second argument is a delay between
		// successive outputs. The delay is
		// measured in milliseconds. "10", for
		// instance, means, "print a line every
		// hundredth of a second".
		ExampleThread mt = new ExampleThread("A", 31);
		ExampleThread mt2 = new ExampleThread("B", 25);
		ExampleThread mt3 = new ExampleThread("C", 10);
		mt.start();
		mt2.start();
		mt3.start();
	}
}