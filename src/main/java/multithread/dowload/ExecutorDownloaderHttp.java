package multithread.dowload;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

public class ExecutorDownloaderHttp {

	public static void main(String[] args) {

		final String url = "http://localhost:8080/htmlVersion/csbweb/csbwebdl.jsp?submit=Descargar&archivoId=";
		try {
			HttpContext ctx = initHttpContext();

			List<String> ids = Arrays.asList(new String[] { "167", "164", "162", "149" });
			ExecutorService pool = Executors.newFixedThreadPool(4);
			for (String id : ids) {
				pool.submit(new DownloadTask(url + id, "c:/Dani/tmp/fichero_" + id	+ ".xml", ctx));
			}
			pool.shutdown();
			pool.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static HttpContext initHttpContext() throws ClientProtocolException, IOException {
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet("http://localhost:8080/htmlVersion/csbweb/csbwebdl.jsp?tipoDoc=debitos&oficina=29&btnOficina=Aceptar");
		HttpContext localContext = new BasicHttpContext();
		HttpResponse response = httpclient.execute(httpget, localContext);
		EntityUtils.consume(response.getEntity());
		return localContext;
	}



	private static class DownloadTask implements Runnable {

		private String url;
		private final String toPath;
		HttpClient httpclient;
		HttpContext ctx;

		public DownloadTask(String url, String toPath, HttpContext ctx) {
			this.url = url;
			this.toPath = toPath;
			this.ctx = ctx;
			httpclient = new DefaultHttpClient();
		}

		@Override
		public void run() {
			// surround with try-catch if downloadFile() throws something
			try {
				downloadFile(url, toPath);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		private void downloadFile(String url, String toPath) throws IOException {
			System.out.printf("Downloading [%s] to [%s]\n", url, toPath);
			HttpGet httpget = new HttpGet(url);
			HttpResponse response = httpclient.execute(httpget, this.ctx);

			HttpEntity entity = response.getEntity();

			if (entity != null) {
				java.io.FileOutputStream fos = new java.io.FileOutputStream(toPath);
				entity.writeTo(fos);
				fos.close();
			}else
				System.out.print("Response is NULL");

		}
	}
}
