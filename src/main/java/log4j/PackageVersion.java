package log4j;

import org.apache.logging.log4j.core.Layout;

public class PackageVersion {

	public static void main(String[] args) {
		Package pak = Layout.class.getPackage();
		System.out.println(pak);
		System.out.println("Specification Title   : "+pak.getSpecificationTitle());
		System.out.println("Specification Vendor  : "+pak.getSpecificationVendor());
		System.out.println("Specification Version : "+pak.getSpecificationVersion()); //this gets you the version number

		System.out.println("Implementation Title  : "+pak.getImplementationTitle());
		System.out.println("Implementation Vendor : "+pak.getImplementationVendor());
		System.out.println("Implementation Version: "+pak.getImplementationVersion()); //this gets you the version number
	}

}
