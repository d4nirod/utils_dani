/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package optional;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


public class OptionalNullChecksTester {
	private static final String IT_DEPT = "IT";
	private static final String FINANCE_DEPT = "Finance";
	private static final String BEANCOUNTER_TEAM = "beancounters";
	private static final String DEV_TEAM = "devs";

	public static void main(String[] args) {

		
		String name1 = "John";
		String name2 = null;
		String name3 = "Maurice";
		
		Map<String, String> namesToTeams = new HashMap<>();
		namesToTeams.put(name1, DEV_TEAM);
		namesToTeams.put(name3, BEANCOUNTER_TEAM);
		
		Map<String, String> teamsToDepartments = new HashMap<>();
		teamsToDepartments.put(DEV_TEAM, IT_DEPT);
		teamsToDepartments.put(BEANCOUNTER_TEAM, FINANCE_DEPT);
		
		Optional.ofNullable(name1).map(namesToTeams::get)
		.ifPresent(t -> System.out.println(name1 + ":" + t + ":" + teamsToDepartments.get(t)));

		Optional.ofNullable(name2).map(namesToTeams::get)
		.ifPresent(t -> System.out.println(name2 + ":" + t + ":" + teamsToDepartments.get(t)));
		
		Optional.ofNullable(name3).map(namesToTeams::get)
		.ifPresent(t -> System.out.println(name3 + ":" + t + ":" + teamsToDepartments.get(t)));

	}

}
