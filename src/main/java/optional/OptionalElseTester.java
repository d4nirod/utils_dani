/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package optional;

import java.text.ParseException;
import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.time.DateUtils;

public class OptionalElseTester {

	public static void main(String[] args) throws ParseException {
		Date date1 = DateUtils.parseDate("01/01/2019", "dd/MM/YYYY");
		Date dateNull = null;
		
		Date d1 = Optional.ofNullable(date1).orElse(null);
		System.out.println(d1);
		Date dn = Optional.ofNullable(dateNull).orElse(new Date());
		System.out.println(dn);
		
		System.out.println("==============");
		
		String s = null;
		Optional<String> stringOpt = Optional.ofNullable(s);
		System.out.println(stringOpt.orElse(OptionalElseTester.getString()));

		s = "the blah string";
		stringOpt = Optional.ofNullable(s);
		System.out.println(stringOpt.orElseGet(OptionalElseTester::getString));
	}
	
	public static String getString() {
		System.out.println("--> getString() called");
		return "a stringy string";
	}

}
