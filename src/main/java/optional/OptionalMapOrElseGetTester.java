/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package optional;

import java.util.Optional;

public class OptionalMapOrElseGetTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s1 = "BLAH";
		System.out.println(s1 + ": " + doStuff(s1));
		String s2 = null;
		System.out.println(s2 + ": " + doStuff(s2));
	}
	
	public static String doStuff(String str) {
		Optional<String> opt = Optional.ofNullable(str);
		return opt.map(s -> s.toLowerCase()).orElse("was null");
	}

}
