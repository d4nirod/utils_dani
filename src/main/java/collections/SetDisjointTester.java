/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package collections;

import java.util.Collections;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.SetUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SetDisjointTester {
	
	static Logger log = LogManager.getLogger();


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Set<String> a = SetUtils.hashSet("a", "b", "c", "d");
		Set<String> b = SetUtils.hashSet("b", "c");
		log.debug("{}, {}, disjoint (have no elements in common): {}", a, b, Collections.disjoint(a, b));
		log.debug("{}, {}, containsAny (have elements in common): {}", a, b, CollectionUtils.containsAny(a, b));
		Set<String> c = SetUtils.hashSet("e", "f");
		log.debug("{}, {}, disjoint: {} (have no elements in common)", a, c, Collections.disjoint(a, c));
		log.debug("{}, {}, containsAny (have elements in common): {}", a, c, CollectionUtils.containsAny(a, c));
		Set<String> d = SetUtils.emptySet();
		log.debug("{}, {}, disjoint: {} (have no elements in common)", a, d, Collections.disjoint(a, d));
		log.debug("{}, {}, containsAny (have elements in common): {}", a, d, CollectionUtils.containsAny(a, d));
		log.debug("{}, {}, containsAny (have elements in common): {}", d, a, CollectionUtils.containsAny(d, a));
		
	}

}
