package collections;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArrayListCapacityFill {

	public static void main(String[] args) {
		//create list size n
		List<Integer> l = Arrays.asList(new Integer[10]);
		//fill with 0
		Collections.fill(l, new Integer(0));

		//now create another list with size < l.size
		List<Integer> l2 = Arrays.asList(new Integer[]{11, 22, 33, 44});
		//set l2 elements to l in same order
		for(int i=0; i<l2.size(); i++)
			l.set(i, l2.get(i));

		System.out.println("List size: "+l.size());
		for(int i=0; i<l.size(); i++)
			System.out.println("["+i+"]: "+l.get(i));

	}

}
