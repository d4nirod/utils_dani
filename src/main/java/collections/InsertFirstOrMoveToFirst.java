package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class InsertFirstOrMoveToFirst {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// NOTE: this resulted in an UnsupportedOperationException on the method call later 
		// as the add(int, Object) method is not supported by AbstractList-something
		// List<String> list = Arrays.asList("A", "B", "C", "D");
		
		ArrayList<String> list = new ArrayList<>(Arrays.asList("A", "B", "C", "D"));
		System.out.println("Orig: " + StringUtils.join(list));
		System.out.println("--------------------------");
		
		doStuff(list, "A");
		doStuff(list, "1");
		doStuff(list, "C");
	}
 
	@SuppressWarnings("unchecked")
	private static void doStuff(List<String> list, String s) {
		int i = list.indexOf(s);
		if (i > 0)
			list.remove(i);
		if (i != 0) 
			list.add(0, s);
		
		System.out.println("Adding: '" + s + "': " + StringUtils.join(list));
	}

}
