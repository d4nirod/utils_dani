package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.lang3.StringUtils;

public class IterateReplacing {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		List<String> originalList = Arrays.asList("a", "b", "c", "d");
		System.out.println("Trying to replace 'c' with enhanced for-loop/variable assignment matched_el = newValue");
		List<String> l = new ArrayList<>(originalList);
		System.out.println("Before: " + StringUtils.join(l));
		for (String s : l) {
			if (s.equals("c"))
				s = "x";
		}
		System.out.println("After: " + StringUtils.join(l));
		System.out.println("---------------------------------");

		System.out.println("Trying to replace 'c' with normal for-loop/list.get(i) + list.set(i, newValue)");
		l = new ArrayList<>(originalList);
		System.out.println("Before: " + StringUtils.join(l));
		for (int i = 0; i < l.size(); i++) {
			if (l.get(i).equals("c")) {
				l.set(i, "x");
			}
		}
		System.out.println("After: " + StringUtils.join(l));
		System.out.println("---------------------------------");
		
		System.out.println("Trying to replace 'c' with listIterator for-loop/it.set(newValue)");
		l = new ArrayList<>(originalList);
		System.out.println("Before: " + StringUtils.join(l));
		for(ListIterator<String> it = l.listIterator(); it.hasNext(); ) {
			String s = it.next();
			if (s.equals("c")) {
				it.set("x");
			}
		}
		System.out.println("After: " + StringUtils.join(l));
		System.out.println("---------------------------------");
		

	}

}
