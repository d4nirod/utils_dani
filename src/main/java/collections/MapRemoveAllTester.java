/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package collections;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MapRemoveAllTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Map<String, String> map = new HashMap<>();
		map.put("a", "");
		map.put("b", "");
		map.put("c", "");

		Set<String> set = new HashSet<>();
		set.add("a");
		set.add("b");

		map.keySet().removeAll(set);

		System.out.println(map); // only contains "c"

	}

}
