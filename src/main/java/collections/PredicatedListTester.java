/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.PredicateUtils;

public class PredicatedListTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> list = new ArrayList<>(Arrays.asList("1", null, "3"));
//		List<String> predicatedList = ListUtils.predicatedList(list, PredicateUtils.notNullPredicate());
//		CollectionUtils.filter(list, PredicateUtils.notNullPredicate());
		
		listWithoutNulls(list).stream().forEach(System.out::println);
		System.out.println("---");
//		listWithoutNulls(6, null, "6", null, "8").stream().forEach(System.out::println);
		System.out.println("---");
		
		Object[] objects = new Object[] { 1, "2", null };
		listWithoutNulls(objects).stream().forEach(System.out::println);
		System.out.println("---");
		
	}
	
	public static <T extends Object>  List<T> listWithoutNulls(List<T> list) {
		CollectionUtils.filter(list, PredicateUtils.notNullPredicate());
		return list;
	}
	
	public static <T extends Object> List<T> listWithoutNulls(T... strings) {
		return listWithoutNulls(new ArrayList<>(Arrays.asList(strings)));
	}

}
