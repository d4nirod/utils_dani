package collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShallowCopyTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> a = new ArrayList<String>();
		a.add("a");
		a.add("b");
		a.add("c");
		List<String> b = new ArrayList<String>(a);

		System.out.println("There should be no output after this line.");

		// Note, b is already a shallow copy of a;
		for(int i = 0; i < a.size(); i++) {
		    if (a.get(i) != b.get(i)) {
		        System.out.println("Oops, this was a deep copy.");// Note this is never called.
		    }
		}

		// Now use Collections.copy and note that b is still just a shallow copy of a
		Collections.copy(b, a);
		for(int i = 0; i < a.size(); i++) {
		    if (a.get(i) != b.get(i)) {
		        System.out.println("Oops, i was wrong this was a deep copy");// Note this is never called.
		    }
		}

		// Now do a deep copy - requires you to explicitly copy each element
		for(int i = 0; i< a.size(); i++) {
		    b.set(i, new String(a.get(i)));
		}

		// Now see that the elements are different in each 
		for(int i =0; i< a.size(); i++) {
		    if (a.get(i) == b.get(i)) {
		        System.out.println("oops, i was wrong, a shallow copy was done."); // note this is never called.
		    }
		}

	}
}
