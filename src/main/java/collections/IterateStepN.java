package collections;

import java.util.ArrayList;
import java.util.List;

public class IterateStepN {

	public static void main(String[] args) {
		final int step = 15;
		final int size = 37;
		List<Integer> l = new ArrayList<Integer>(size);
		for(int i=0; i<size;i++)
			l.add(i+1);
		System.out.println("Array/List size: "+l.size()+", step: "+step);
		System.out.println(l.size()+" % "+step+ " = " +l.size()%step+"\n");
		System.out.println("AS LIST:");
		int i=0;
		for(i=0; i<l.size() && i+step<l.size(); i+=step){
			List<Integer> sl = l.subList(i, i+step);
			for(Integer n : sl)
				System.out.print(String.format("%02d ", n));
			System.out.println();
		}
		if(i<l.size()){
			List<Integer> sl = l.subList(i, l.size());
			for(Integer n : sl)
				System.out.print(String.format("%02d ", n));
			System.out.println();
		}

		System.out.println("--------------------------");
		System.out.println("i:"+i);

		System.out.println("============================");


		Integer arr[] = l.toArray(new Integer[size]);
//		int validSize = arr.length & ~1;
//		System.out.println("Valid size: "+validSize);

		System.out.println("AS ARRAY:");
		int j=0;
		for(i=0; i<arr.length; i+=step){
			for(j=i; j<(i+step) && j<arr.length;j++){
				System.out.print(String.format("%02d ", arr[j]));
			}
			System.out.println();
		}

		System.out.println("--------------------------");
		System.out.println("i:"+i);
		System.out.println("j:"+j);
	}

}
