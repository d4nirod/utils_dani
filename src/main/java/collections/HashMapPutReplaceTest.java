package collections;

import java.util.HashMap;

public class HashMapPutReplaceTest {

	public static void main(String[] args) {
		HashMap<String, Object> map = new HashMap<>();
		map.put("key", 10);
		Object old = map.put("key", "blah");
		System.out.println("old: " + old + ", new: " + map.get("key"));

	}

}
