package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class IteratorPassedAroundTest {

	public static void main(String[] args) {
		Iterator<Integer> iterator = getIterator();
		List<Integer> list1 = new ArrayList<>();
		iterator.forEachRemaining(list1::add);
		System.out.println("\n### main() iterator: " + list1);
		iterateIterator(iterator);
		
		Iterator<Integer> iterator2 = getIterator();
		List<Integer> list2 = new ArrayList<>();
		while (iterator2.hasNext()) list2.add(iterator2.next());
		System.out.println("\n### main() iterator2: " + list2);
		iterateIterator(iterator2);
		
		Iterable<Integer> iterable = getIterable();
		List<Integer> list3 = new ArrayList<>();
		iterable.forEach(list3::add);
		System.out.println("\n### main() iterable: " + list3);
		iterateIterable(iterable);
		

	}
	static Iterator<Integer> getIterator() {
		List<Integer> list = Arrays.asList(1, 2, 3, 4);
		return list.iterator();
	}
	
	static void iterateIterator(Iterator<Integer> iterator) {
		System.out.println("### iterateIterator(): ");
		iterator.forEachRemaining(System.out::println);
	}

	static Iterable<Integer> getIterable() {
		List<Integer> list = Arrays.asList(6, 7, 8, 9);
		return list;
	}
	
	static void iterateIterable(Iterable<Integer> iterable) {
		System.out.println("### iterateIterable(): ");
		iterable.forEach(System.out::println);
	}

}
