package collections;
import java.util.ArrayList;


public class ArrayListSizeTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList<Integer> l = new ArrayList<Integer>(50);
		int max = 10;
		int i=0;
		while(l.size()< max){
			l.add(i++);			
		}
		System.out.println("List size: "+l.size());
		for(Integer item : l)
			System.out.println("Item: "+item);
	}

}
