package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;

public class Arrays$ArrayListTest {
	private static Class<?> _Arrays$ArrayListClazz;
	static {
		try {
			_Arrays$ArrayListClazz = Class.forName("java.util.Arrays$ArrayList");
		} catch (ClassNotFoundException e) {
			_Arrays$ArrayListClazz = null;
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public static void main(String[] args) {
		
		List<String> l1 = Arrays.asList("a", "b", "c");
		doStuffToList(l1);
		System.out.println( "After doStuff: : " + l1 + "\n");
		
		List<String> l2 = new ArrayList<>();
		l2.add("1");
		l2.add("2");
		l2.add("3");
		doStuffToList(l2);
		System.out.println( "After doStuff: : " + l2 + "\n");
		
		List<String> l3 = new ArrayList<>(l1);
		doStuffToList(l3);
		System.out.println( "After doStuff: : " + l3 + "\n");
		
	
	}

	private static void doStuffToList(List<String> l) {
		System.out.println(l + " is Arrays$ArrayList: " + isArrays$ArrayList(l));
		System.out.println(l + " is ArrayList: " + (l instanceof ArrayList));
		int size = l.size();
		try {
			l.add("add");
		} catch (Exception e) {
			System.out.println( l + ": " + e);
		}
		System.out.println(l + " can add: " + (l.size() > size));
	}
	
	/**
	 * Checks if a list is instance of java.util.Arrays$ArrayList internal class since we make extensive use of Arrays.asList(..) 
	 * when creating lists which doesn't actually create an real ArrayList and it's inmmutable!
	 * @param list
	 * @return true if the list is assignable from Arrays$ArrayList, false if the list is null or not assignable.
	 */
	public static boolean isArrays$ArrayList(List<?> list) {
		if (_Arrays$ArrayListClazz == null) {
			if (LogManager.getLogger() != null) {
				LogManager.getLogger().warn("isArrays$ArrayList(): _Arrays$ArrayListClazz is null, it was not propertly initialised. ");
			}
			return false;
		}
		if (list == null) return false;
		
		return list.getClass().isAssignableFrom(_Arrays$ArrayListClazz);
	}

}
