/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

public class IteratorAddWhileIteratingTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		List<String> list = new ArrayList<>();
//		for (int i = 0; i <  20; i++) {
//			list.add("" + i);
//		};
//		System.out.println(StringUtils.join(list, ','));
//		ListIterator<String> it = list.listIterator();
//		while (it.hasNext()) {
//			String el = it.next();
//			if ((Integer.parseInt(el) % 2) != 0) {
//				it.add("[<- odd]");
//			}
//		}
//		System.out.println("Done");
//		System.out.println(StringUtils.join(list, ','));
		
		// Now with maps
		 List<Map<String, Object>> mapList = new ArrayList<>();
		 final String key1 = "key1";
		 mapList.add(new HashMap<>());
		 mapList.get(0).put(key1, 1);
		 addToFirstNonContainingMap(key1, 11, mapList);
		 addToFirstNonContainingMap("key2", 2, mapList);
		 addToFirstNonContainingMap(key1, 111, mapList);
		 addToFirstNonContainingMap("key2", 22, mapList);

		 
		 
		 mapList.stream().forEach(m -> {
			 System.out.println("Map element");
			 m.entrySet().stream().forEach(e -> System.out.println("\tKey: " + e.getKey() + ", value: " + e.getValue()));
		 });
		 
		 
	}
	
	public static void addToFirstNonContainingMap(String key, Object value, Collection<Map<String, Object>> mapList) {
		Map<String, Object> map = null;
//		for (Map<String, Object> m : mapList) {
//			if (!m.containsKey(key)) {
//				map = m;
//				break;
//			}
//		}
		Optional<Map<String, Object>> found = mapList.stream().filter(m -> !(m.containsKey(key))).findFirst();
//		if (foundMap == null) {
		
		if (found.isPresent()) {
			map = found.get();
		} else {
			map = new HashMap<>();
			mapList.add(map);
		}
		map.put(key, value);
		
	}
	
	

}
