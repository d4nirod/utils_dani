/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package collections;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

public class ListDifferencesTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<Integer> a = Arrays.asList(1, 2, 3, 4, 5);
		List<Integer> b = Arrays.asList(2, 3, 5, 6);
	    System.out.println("a: " + StringUtils.join(a, " , "));
	    System.out.println("b: " + StringUtils.join(b, " , "));
	    System.out.println();
	    System.out.println("CollectionUtils.subtract(a, b):     " + StringUtils.join(CollectionUtils.subtract(a, b), " , "));
	    System.out.println("CollectionUtils.retainAll(a, b):    " + StringUtils.join(CollectionUtils.retainAll(a, b), " , "));
	    System.out.println("CollectionUtils.collate(a, b):      " + StringUtils.join(CollectionUtils.collate(a, b), " , "));
	    System.out.println("CollectionUtils.disjunction(a, b):  " + StringUtils.join(CollectionUtils.disjunction(a, b), " , "));
	    System.out.println("CollectionUtils.intersection(a, b): " + StringUtils.join(CollectionUtils.intersection(a, b), " , "));
	    System.out.println("CollectionUtils.union(a, b):        " + StringUtils.join(CollectionUtils.union(a, b), " , "));

	}

}
