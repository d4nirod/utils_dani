package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArraysAsListImmutableTest {
	
	public static void main(String[] args) {
		
		List<String> l = Arrays.asList("a", "b", "c");
		System.out.println("is instanceof ArrayList: " + (l instanceof ArrayList));
		l.add("e");
		System.out.println(l);
		
	}

}
