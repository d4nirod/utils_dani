/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package collections;

import java.util.Collections;
import java.util.List;

public class EmptyListContainsTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> list = Collections.emptyList();
		System.out.println("list contains blah: " + list.contains("blah"));
	}

}
