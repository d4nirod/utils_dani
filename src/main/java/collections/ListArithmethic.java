/*
 *
 * Copyright Farmers WIFE S.L. All Rights Reserved.
 * 
 */

package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;


public class ListArithmethic {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		List<String> orig = Arrays.asList("A", "B", "C", "D");
		List<String> newl  = Arrays.asList("B", "D", "F", "G");
		diff(orig, newl);
		diff(new ArrayList<String>(), newl);
		diff(orig, new ArrayList<String>());
		
		System.out.println("========================");
		List<String> a = Arrays.asList("a", "b", "c", "d");
		List<String> b = Arrays.asList("c", "d", "e");
		System.out.println("a: " + StringUtils.join(a));
		System.out.println("b: " + StringUtils.join(b));
		System.out.println("--------------------------");

		System.out.println("disjunction: " + StringUtils.join(CollectionUtils.disjunction(b, a)));
		System.out.println("a - b: " + StringUtils.join(CollectionUtils.subtract(a, b)));
		System.out.println("b - a: " + StringUtils.join(CollectionUtils.subtract(b, a)));

	}

	private static void diff(List<String> a, List<String> b) {
		print("a", a);
		print("b", b);
		List<String> added   = ListUtils.subtract(b, a);
		List<String> removed = ListUtils.subtract(a, b);
		print("added  ", added);
		print("removed", removed);
		System.out.println("-----------\n");
	}
	
	@SuppressWarnings("unchecked")
	public static void print(String msg, List<String> l) {
		System.out.println(msg + ": " + StringUtils.join(l));
	}

}
