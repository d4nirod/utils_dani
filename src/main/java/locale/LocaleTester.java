package locale;

import java.util.Locale;

public class LocaleTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Locale l = new Locale("es");
		System.out.printf("Language: %s, iso3: %s\n", l.getLanguage(), l.getISO3Language());
		l = new Locale("wx");
		System.out.printf("Language: %s, iso3: %s\n", l.getLanguage(), l.getISO3Language());

	}

}
