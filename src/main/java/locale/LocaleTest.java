package locale;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public class LocaleTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Locale locs[] = {	new Locale("es","ES"), 
							new Locale("ca", "ES"), 
							new Locale("en","UK"), 
							new Locale("de","DE")
		};
		
		for(int i=0; i<locs.length; i++){
			NumberFormat fmt = NumberFormat.getCurrencyInstance(locs[i]);
			fmt.setCurrency(Currency.getInstance("EUR"));		
			System.out.println(locs[i]+": "+fmt.format(1234567.89));
		}
	}

}
