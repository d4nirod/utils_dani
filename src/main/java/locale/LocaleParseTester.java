/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package locale;

import java.util.Arrays;
import java.util.IllformedLocaleException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;

public class LocaleParseTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> list = Arrays.asList("en-US", "en_US", "en", "blah", " ", "");
		for (String s : list) {
			Optional<Object> locale = parseLocale(s);
			System.out.println("locale is present: " + locale.isPresent());
			System.out.println("----");
		}
	}
	
	public static Optional<Object> parseLocale(String s) {
				
		return Optional.ofNullable(s).map(l -> {
			Locale locale = null;
			try {
				locale = LocaleUtils.toLocale(s);
				System.out.println(s + ": parsed with LocaleUtils.toLocale. language: " + locale.getLanguage()
						+ ", country: " + locale.getCountry());
				if (StringUtils.isBlank(locale.getLanguage())) {
					return null;
				}
				return locale;
			} catch (IllegalArgumentException e) {
				System.out.println(s + ": localeUtils.toLocale() failed on string: '" + s + "'");
			}

			try {
				locale = new Locale.Builder().setLanguageTag(s).build();
				System.out.println(s + ": parsed with Locale.Builder().setLanguageTag(s).build();. language: "
						+ locale.getLanguage() + ", country: " + locale.getCountry());
				return locale;
			} catch (IllformedLocaleException e) {
				System.out.println(s + ": failed  with Locale.Builder().setLanguageTag(s).build()");
				return null;
			}
		});
	}

	public static Locale parseLocale2(String s) {
		try {
			Locale locale = LocaleUtils.toLocale(s);
			System.out.println( s + ": parsed with LocaleUtils.toLocale. language: " + locale.getLanguage() + ", country: " + locale.getCountry());
			if (StringUtils.isBlank(locale.getLanguage())) {
				return null;
			}
			return locale;
		} catch (Exception e) {
			System.out.println(s + ": localeUtils.toLocale() failed on string: '" + s + "'");
		}

		try {
			Locale locale = new Locale.Builder().setLanguageTag(s).build();
			System.out.println(s + ": parsed with Locale.Builder().setLanguageTag(s).build();. language: " + locale.getLanguage() + ", country: " + locale.getCountry());
			return locale;
		} catch (Exception e) {
			System.out.println(s + ": failed  with Locale.Builder().setLanguageTag(s).build()");
			return null;
		}
	}
}
