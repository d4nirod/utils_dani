package locale;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class ListAvailableLocales {


	public static void main(String[] args) {
		printAvailableLocales();
	}
	
	public static void printAvailableLocales() {
		Locale list[] = SimpleDateFormat.getAvailableLocales();
		
		for (int i = 0; i<list.length; i++) {
			System.out.println(list[i].toString() + "\t\t"
					+ list[i].getDisplayName());
		}
	}
	
}
