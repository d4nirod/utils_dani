package locale;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Locale.LanguageRange;

import org.apache.commons.lang3.LocaleUtils;

public class LocaleLanguageTagMatchDemo {

	public static void main(String[] args) {
		// Create a collection of Locale objects to filter
		List<Locale> supportedLocales = new ArrayList<>();
		supportedLocales.add(Locale.ENGLISH);
		// supportedLocales.add(Locale.forLanguageTag("en-US"));
		supportedLocales.add(Locale.forLanguageTag("es"));

		// Express the user's preferences with a Language Priority List
		String ranges = "en-US;q=1.0,en-GB;q=1.0,fr-FR;q=0.0";
		List<LanguageRange> languageRanges = Locale.LanguageRange.parse(ranges);

		// Now lookup the Locale objects, returning best match
		// Locale bestMatch = Locale.lookup(languageRanges, availableLocales);
		Locale bestMatch = findBestMatch(languageRanges, supportedLocales);
		System.out.println("Best supported locale match: " + bestMatch.toString());
		Locale reversedBestMatch = findPreferredLocaleFromLocale(languageRanges, bestMatch);
		System.out.println("Best supported requested locale match: " + reversedBestMatch.toString());
	}

	private static List<Locale> filterRangesWithAvailableLocales(List<LanguageRange> languageRanges) {
		List<Locale> allAvailableLocales = LocaleUtils.availableLocaleList();
		List<Locale> allMatches = Locale.filter(languageRanges, allAvailableLocales);
		return allMatches;
	}

	private static Locale findPreferredLocaleFromLocale(List<LanguageRange> languageRanges, Locale locale) {
		List<Locale> allMatches = filterRangesWithAvailableLocales(languageRanges);
		for (Locale l : allMatches) {
			for (Locale ll : LocaleUtils.localeLookupList(l)) {
				if (locale.getLanguage().equals(ll.getLanguage())) {
					return ll;
				}
			}
		}
		return locale;
	}

	// private Locale findBestMatch(final List<Locale> requested, final
	// List<Locale> supported) {
	private static Locale findBestMatch(List<LanguageRange> languageRanges, final List<Locale> supported) {

		List<Locale> allMatches = filterRangesWithAvailableLocales(languageRanges);
		Locale bestMatch = Locale.lookup(toLanguageRanges(allMatches), supported);

		return bestMatch;
	}

	private static List<LanguageRange> toLanguageRanges(final List<Locale> locales) {
		final ArrayList<LanguageRange> languageRanges = new ArrayList<>();
		for (final Locale locale : locales) {
			languageRanges.add(new LanguageRange(locale.toLanguageTag()));
		}
		return languageRanges;
	}

	private static LanguageRange toLanguageRange(final Locale locale) {
		return new LanguageRange(locale.toLanguageTag());
	}

}
