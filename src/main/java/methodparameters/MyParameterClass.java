package methodparameters;

public class MyParameterClass {
	public MyParameterClass(String value) {
		this.value = value;
	}

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	

}
