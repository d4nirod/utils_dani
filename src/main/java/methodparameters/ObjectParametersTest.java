package methodparameters;

import java.util.Date;

public class ObjectParametersTest {

	/**
	 * @param args
	 */
	
	public static void doStuff(MyParameterClass p){
		p.setValue(new Date().toString());
	}
	
	public static void main(String[] args) throws InterruptedException {
//		ObjectParametersTest t = new ObjectParametersTest();
		MyParameterClass o = new MyParameterClass("original");
		System.out.println(o.getValue());
		for(int i=0; i<5; i++){
			doStuff(o);
			System.out.println(o.getValue());
			Thread.sleep(1000);
		}
	}
}
