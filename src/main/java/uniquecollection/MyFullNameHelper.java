package uniquecollection;

import java.util.Collection;
import java.util.HashSet;

public class MyFullNameHelper implements FullNameHelper
{
	public Collection<FullName> getUnqiueFullNames(Collection<FullName> fullNames) {
    	Collection<MyFullName> mycol = new HashSet<MyFullName>();
    	for(FullName f : fullNames)
    		mycol.add((MyFullName)f);
    	
    	fullNames.clear();
    	fullNames.addAll(mycol);
    	return fullNames;
    }

    public static class MyFullName implements FullName {
        private final String firstName;
        private final String lastName;

        public MyFullName(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public String getFirstName() {
            return this.firstName;
        }

        public String getLastName() {
            return this.lastName;
        }
        
        public boolean equals(FullName f){
        	if(f== null)
        		return false;
        	
        	else if(this.firstName.equals(f.getFirstName()) && this.lastName.equals(f.getLastName()))
        		return true;
        	
        	else
        		return false;
        	
        }
    }
}
