package httpcomponents;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class SecurizarTarjetaTest {
	public SecurizarTarjetaTest() {
		httpclient= new DefaultHttpClient();
	}
	private HttpClient httpclient; 

	public static void main(String[] args) {
		
			SecurizarTarjetaTest test = new SecurizarTarjetaTest();
		try {
			String token = test.obtenerToken();
			String msg = test.construyeMensaje4B('A', "4966320029564214","201211", "DN43081062S", "01",
					                        "blanco", "color del caballo blanco de santiago", token );
			String resXml = test.enviaXML(msg);
			System.out.println("###");
			System.out.println(resXml);
			System.out.println("###");
	
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {          
			test.close();
        }
	}
	
	private String enviaXML(String msg) throws ClientProtocolException, IOException, URISyntaxException {
		String uriStr =  "http://localhost:8080/MyTestWebapp//SecurizarServlet";		 
		HttpPost httppost = new HttpPost(new URI(uriStr));
		System.out.println(httppost.getURI());
		System.out.println("****************************************************");
		StringEntity entityRq = new StringEntity(msg);
		httppost.setEntity(entityRq);
		HttpResponse response = httpclient.execute(httppost);
		HttpEntity entity = response.getEntity();
		String res = "";
		if (entity != null) {
			entity = new BufferedHttpEntity(entity);
			res = EntityUtils.toString(entity);
		}
		
		return res;
		
	}

	public String obtenerToken() throws URISyntaxException, ClientProtocolException, IOException{
		
		String uriStr =  "http://localhost:8080/MyTestWebapp/4b/Token.Generate.jsp";
		URI base = new URI(uriStr);
		List<NameValuePair> qparams = new ArrayList<NameValuePair>();
		qparams.add(new BasicNameValuePair("IdApPortal", "HB.0061.001"));
		qparams.add(new BasicNameValuePair("IdAp4BAcceso", "ES.0444.002"));
		qparams.add(new BasicNameValuePair("IdDescAcceso", "DN88765432Z"));
		qparams.add(new BasicNameValuePair("PwdPortalAcceso", "M0b17e"));		
		URI uri = URIUtils.createURI(base.getScheme(), base.getHost(), base.getPort(),
				base.getPath(),URLEncodedUtils.format(qparams, "ISO-8859-1"), 
				base.getFragment());

		HttpGet httpget = new HttpGet(uri);
		System.out.println(httpget.getURI());
		System.out.println("****************************************************");
		HttpResponse response = httpclient.execute(httpget);
		HttpEntity entity = response.getEntity();
		String res = "";
		if (entity != null) {
			entity = new BufferedHttpEntity(entity);
			res = EntityUtils.toString(entity);
		}
		System.out.println("###");
		System.out.println(res);
		System.out.println("###");
		Hashtable<String, String> results = new Hashtable<String, String>(8);
		// Regex para obtener clave-valor  ([^=\s]+)=\"([^\"]*)\"
		Pattern pattern = Pattern.compile("([^=\\s]+)=\\\"([^\\\"]*)\\\"");
		Matcher matcher = pattern.matcher(res);
		while (matcher.find()) {           		         			
			results.put(matcher.group(1), matcher.group(2));			    				    			    
		}
		Enumeration<String> keys = results.keys();

		//iterate through Hashtable values Enumeration
		while(keys.hasMoreElements()){
			String key = keys.nextElement();
			String value = results.get(key);
			System.out.println(key + " : " + value);
		}
		return results.get("TokenAcceso");
	}
	
	public String construyeMensaje4B(char operacion, String pan, String fecha, String dni, String lang,
			String pwd,	String frase, String token) {
		StringBuffer strb = new StringBuffer();
	    strb.append("<?xml version='1.0' encoding='ISO-8859-1'?>\n");
	    strb.append("<CES.ES4B>\n");
	    if ("A".equals(""+operacion)) {
	      strb.append("<CES.ES4B.Alta.Req>\n");
	      strb.append("<PAN>").append(pan).append("</PAN>\n");
	      strb.append("<FechaCaducidad>").append(fecha).append("</FechaCaducidad>\n");
	      strb.append("<CodIdPersonal>").append(dni).append("</CodIdPersonal>\n");
	      strb.append("<CSB>0061</CSB>\n");
	      strb.append("<Idioma>").append(lang).append("</Idioma>\n");
	      strb.append("<ModoAutenticacion>\n");
	      if (pwd.length()>20) pwd = pwd.substring(0,20);
	      strb.append("<PalabraPaso>").append(StringEscapeUtils.escapeXml(pwd)).append("</PalabraPaso>\n");
	      if ((frase!=null)&&(!"".equals(frase.trim()))) {
	        if (frase.length()>30) frase = frase.substring(0,30);
	        strb.append("<FrasePersonal>").append(StringEscapeUtils.escapeXml(frase)).append("</FrasePersonal>\n");
	      }
	      strb.append("</ModoAutenticacion>\n");
	      strb.append("<IdApPortal>HB.0061.001</IdApPortal>\n");
	      strb.append("<Token>").append(token).append("</Token>\n");
	      strb.append("</CES.ES4B.Alta.Req>\n");
	    } else if ("B".equals(operacion)) {
	      strb.append("<CES.ES4B.Baja.Req>\n");
	      strb.append("<PAN>").append(pan).append("</PAN>\n");
	      strb.append("<CSB>0061</CSB>\n");
	      strb.append("<CodIdPersonal>").append(dni).append("</CodIdPersonal>\n");
	      strb.append("<IdApPortal>HB.0061.001</IdApPortal>\n");
	      strb.append("<Token>").append(token).append("</Token>\n");
	      strb.append("</CES.ES4B.Baja.Req>\n");
	    } else {
	      strb.append("<CES.ES4B.ModificacionDatos.Req>\n");
	      strb.append("<PAN>").append(pan).append("</PAN>\n");
	      strb.append("<CSB>0061</CSB>\n");
	      strb.append("<CodIdPersonal>").append(dni).append("</CodIdPersonal>\n");
	      strb.append("<Idioma>").append(lang).append("</Idioma>\n");
	      if (((pwd!=null)&&(!"".equals(pwd)))||((frase!=null)&&(!"".equals(frase)))) {
	        strb.append("<ModoAutenticacion>\n");
	        if ((pwd!=null)&&(!"".equals(pwd))) {
	          if (pwd.length()>20) pwd = pwd.substring(0,20);
	          strb.append("<PalabraPaso>").append(StringEscapeUtils.escapeXml(pwd)).append("</PalabraPaso>\n");
	        }
	        if ((frase!=null)&&(!"".equals(frase))) {
	          if (frase.length()>30) frase = frase.substring(0,30);
	          strb.append("<FrasePersonal>").append(StringEscapeUtils.escapeXml(frase)).append("</FrasePersonal>\n");
	        }
	        strb.append("</ModoAutenticacion>\n");
	      }
	      strb.append("<IdApPortal>HB.0061.001</IdApPortal>\n");
	      strb.append("<Token>").append(token).append("</Token>\n");
	      strb.append("</CES.ES4B.ModificacionDatos.Req>\n");
	    }
	    strb.append("</CES.ES4B>\n");
		return strb.toString();
	}
	
	private void close() {
		httpclient.getConnectionManager().shutdown();		
	}	

}
