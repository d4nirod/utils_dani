package httpcomponents;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import ssl.httpclient4.EasySSLSocketFactory;
import ssl.httpclient4.EasySSLSocketFactory2;



public class HttpClientTest2 {
	
	public static void main(String[] args) {
		HttpClient httpclient = new DefaultHttpClient();;
		try {
			String uriStr =  "http://localhost:8080/MyTestWebapp/4b/Token.Generate.jsp";
//			URI base = new URI(uriStr);
			List<NameValuePair> qparams = new ArrayList<NameValuePair>(
					Arrays.asList(	new BasicNameValuePair("IdApPortal", "HB.0061.001"),
									new BasicNameValuePair("IdAp4BAcceso", "ES.0444.002"),
									new BasicNameValuePair("IdDescAcceso", "DN88765432Z"),
									new BasicNameValuePair("PwdPortalAcceso", "M0b17e"),									
									new BasicNameValuePair("idioma", "2") )													
			);
			
//			URI uri = URIUtils.createURI(base.getScheme(), base.getHost(), base.getPort(),
//					                     base.getPath(),URLEncodedUtils.format(qparams, "ISO-8859-1"), 
//					                     base.getFragment());
			
			
			
			
			
			System.out.println(URLEncodedUtils.format(qparams, "ISO-8859-1"));
			System.out.println("****************************************************");
			
	
		} finally {          
            httpclient.getConnectionManager().shutdown();
        }
	}

}
