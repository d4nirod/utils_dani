package httpcomponents;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.pool.PoolStats;
import org.apache.commons.logging.impl.Log4JLogger;

public class HttpClientPool  {

	private static final int MAX_CONNS_ROUTE = 5;
	private static final int MAX_CONNS_TOTAL = 50;

	private static final int CONNECT_TIMEOUT = 10000;
	private static final int SOCKET_TIMEOUT = 35000;
	private static final int IDLE_TIMEOUT = 15000;
	
	private static Log4JLogger logger = new Log4JLogger(HttpClientPool.class.getName());

	private static enum Singleton {
		// Just one of me so constructor will be called once.
		Client;
		// The thread-safe client.
		private final CloseableHttpClient threadSafeClient;
		// The pool monitor.
		private final IdleConnectionMonitor monitor;
		private PoolingHttpClientConnectionManager connectionManager;

		// The constructor creates it - thus late
		private Singleton() {
			connectionManager = new PoolingHttpClientConnectionManager();
			// Increase max total connection
			connectionManager.setMaxTotal(MAX_CONNS_TOTAL);
			// Increase default max connection per route
			connectionManager.setDefaultMaxPerRoute(MAX_CONNS_ROUTE);
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(SOCKET_TIMEOUT).setConnectTimeout(CONNECT_TIMEOUT).build();

			// Build the client.
			threadSafeClient = HttpClients.custom().setDefaultRequestConfig(requestConfig)
						.setRedirectStrategy(new LaxRedirectStrategy())
						.setConnectionManager(connectionManager)
						.build();
			// Start up an eviction thread.
			monitor = new IdleConnectionMonitor(connectionManager);
			// Don't stop quitting.
			monitor.setDaemon(true);
			monitor.start();
		}

		public CloseableHttpClient get() {
			return threadSafeClient;
		}

		public PoolingHttpClientConnectionManager getConnectionManager() {
			return connectionManager;
		}
	}

	private static class IdleConnectionMonitor extends Thread {
		// The manager to watch.
		private final PoolingHttpClientConnectionManager cm;
		// Use a BlockingQueue to stop everything.
		private final BlockingQueue<Stop> stopSignal = new ArrayBlockingQueue<Stop>(1);

		// Pushed up the queue.
		private static class Stop {
			// The return queue.
			private final BlockingQueue<Stop> stop = new ArrayBlockingQueue<Stop>(1);

			// Called by the process that is being told to stop.
			public void stopped() {
				// Push me back up the queue to indicate we are now stopped.
				stop.add(this);
			}

			// Called by the process requesting the stop.
			public void waitForStopped() throws InterruptedException {
				// Wait until the callee acknowledges that it has stopped.
				stop.take();
			}
		}

		IdleConnectionMonitor(PoolingHttpClientConnectionManager cm) {
			super();
			logger.debug("Invocando el constructor del monitor de conexiones inactivas.");
			this.cm = cm;
		}

		@Override
		public void run() {
			setName("IdleConnectionMonitor");
			try {
				Stop stopRequest; // Holds the stop request that stopped the process.
				PoolStats before = cm.getTotalStats();
				while ((stopRequest = stopSignal.poll(5000, TimeUnit.MILLISECONDS)) == null) {  // Every 5 seconds.
					cm.closeExpiredConnections(); // Close expired connections
					cm.closeIdleConnections(IDLE_TIMEOUT, TimeUnit.MILLISECONDS); 	// Optionally, close connections that have been idle too long.
					// Veamos unas estadisticas del pool.
					PoolStats stafter = cm.getTotalStats();
					if(before.getLeased()!=0 || before.getAvailable()!=0 || before.getPending()!=0){
						logger.debug("Pool stats ANTES  : "+formatStats(before));
						logger.debug("Pool stats DESPUES: "+formatStats(stafter));
					}
				}
				// Acknowledge the stop request.
				stopRequest.stopped();
			} catch (InterruptedException ex) {
				// terminate
			}
		}

		public void shutdown() throws InterruptedException {
			logger.debug("Parando pool de cliente.");
			// Signal the stop to the thread.
			Stop stop = new Stop();
			stopSignal.add(stop);
			// Wait for the stop to complete.
			stop.waitForStopped();
			// Close the pool
			try {
				Singleton.Client.threadSafeClient.close();
			} catch (IOException e) {
				logger.error("Problema parando el cliente HTTP.");
			}
			// Close the connection manager.
			cm.close();
			logger.debug("Pool de clientes parado.");
		}
	}

	public static void shutdown() throws InterruptedException {
		// Shutdown the monitor.
		Singleton.Client.monitor.shutdown();
	}

	public static CloseableHttpClient getClient() {
		// The thread safe client is held by the singleton.
		return Singleton.Client.get();
	}

//	public static int getSocketTimeout(){
//		return Singleton.Client.getConnectionManager().getDefaultSocketConfig().getSoTimeout();
//	}
//
//	public static int getConnectTimeout(){
//		return Singleton.Client.getConnectionManager().
//	}

	public static String getTotalStats() {
		return formatStats(Singleton.Client.getConnectionManager().getTotalStats());
	}

	public static String getRouteStats(String host){
		return formatStats(Singleton.Client.getConnectionManager().getStats(new HttpRoute(new HttpHost(host))));
	}

	public static String formatStats(PoolStats st) {
		return String.format("max: %02d, avail: %02d, leased: %02d, pending: %02d",
				st.getMax(), st.getAvailable(), st.getLeased(), st.getPending());
	}
}
