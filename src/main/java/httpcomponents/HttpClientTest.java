package httpcomponents;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import ssl.httpclient4.EasySSLSocketFactory;
import ssl.httpclient4.EasySSLSocketFactory2;



public class HttpClientTest {
	
	public static void main(String[] args) {
		HttpClient httpclient = new DefaultHttpClient();;
		try {
			Scheme sch = new Scheme("https", 443, new EasySSLSocketFactory());
			httpclient.getConnectionManager().getSchemeRegistry().register(sch);
			
//			SchemeRegistry schemeRegistry = new SchemeRegistry();
//			schemeRegistry.register(new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
//			schemeRegistry.register(new Scheme("https", 443, new EasySSLSocketFactory()));
//			HttpParams params = new BasicHttpParams();
//			params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
//			params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
//			params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
//			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
//			ClientConnectionManager cm = new ThreadSafeClientConnManager(schemeRegistry);
//			httpclient = new DefaultHttpClient(cm);
			
/************************** PROXY **********************************************/			
			HttpHost proxy = new HttpHost("localhost", 3128);
			httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy); 			
/************************** PROXY **********************************************/			
			
			
//			String uriStr = "https://194.224.159.50/secaccess/jsp/Token.Generate.jsp";
			String uriStr =  "http://localhost:8080/MyTestWebapp/4b/Token.Generate.jsp";
//			String uriStr = "https://telepago2.4b.es:443/secaccess/jsp/Token.Generate.jsp";
//			String uriStr = "https://teleintegra.bancamarch.es/htmlVersion/logout.jsp";
			URI base = new URI(uriStr);
			List<NameValuePair> qparams = new ArrayList<NameValuePair>();
			qparams.add(new BasicNameValuePair("IdApPortal", "HB.0061.001"));
			qparams.add(new BasicNameValuePair("IdAp4BAcceso", "ES.0444.002"));
			qparams.add(new BasicNameValuePair("IdDescAcceso", "DN88765432Z"));
			qparams.add(new BasicNameValuePair("PwdPortalAcceso", "M0b17e"));
			
//			qparams.add(new BasicNameValuePair("idioma", "2"));
			URI uri = URIUtils.createURI(base.getScheme(), base.getHost(), base.getPort(),
					                     base.getPath(),URLEncodedUtils.format(qparams, "ISO-8859-1"), 
					                     base.getFragment());
			
			HttpGet httpget = new HttpGet(uri);
			System.out.println(httpget.getURI());
			System.out.println("****************************************************");
			HttpResponse response = httpclient.execute(httpget);
			HttpEntity entity = response.getEntity();
			String res = "";
			if (entity != null) {
			    entity = new BufferedHttpEntity(entity);
			    res = EntityUtils.toString(entity);
			}
			System.out.println("###");
			System.out.println(res);
			System.out.println("###");
			Hashtable<String, String> results = new Hashtable<String, String>(8);
			// Regex para obtener clave-valor  ([^=\s]+)=\"([^\"]*)\"
			Pattern pattern = Pattern.compile("([^=\\s]+)=\\\"([^\\\"]*)\\\"");
			Matcher matcher = pattern.matcher(res);
			while (matcher.find()) {           		         			
				results.put(matcher.group(1), matcher.group(2));			    				    			    
			}
			Enumeration<String> keys = results.keys();
			   
		    //iterate through Hashtable values Enumeration
		    while(keys.hasMoreElements()){
		    	String key = keys.nextElement();
		    	String value = results.get(key);
		    	System.out.println(key + " : " + value);
		    }
			
	
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {          
            httpclient.getConnectionManager().shutdown();
        }
	}

}
