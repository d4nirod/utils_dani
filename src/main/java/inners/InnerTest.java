package inners;

public class InnerTest {

	interface Ioverloader {
		public String getName();
		public void setName(String name);
		public String getSurname();
		public void setSurname(String surname);
	}

	class ExtendedSubject1 extends Subject1 implements Ioverloader{

		public ExtendedSubject1() {
			super();
		}

	}

	class ExtendedSubject2 extends Subject2 implements Ioverloader{

		public ExtendedSubject2() {
			super();
		}

	}

	public void doStuff(Ioverloader o, String s1, String s2, String s3){
		o.setName(s1);
		o.setSurname(s2);
		if(o instanceof ExtendedSubject2)
			((ExtendedSubject2) o).setTel(s3);
	}

	public static void main(String[] args) {
		InnerTest t = new InnerTest();
		ExtendedSubject1 es1 = t.new ExtendedSubject1();
		ExtendedSubject2 es2 = t.new ExtendedSubject2();

		t.doStuff(es1, "pepito",   "perez", null);
		t.doStuff(es2, "fulanito", "detal", "123");

		System.out.println("es1: class: "+es1.getClass()+
							" superclass: "+es1.getClass().getSuperclass()+
							" name: "+es1.getName() +
							" surname: " + es1.getSurname());

		System.out.println("es2: class: "+es2.getClass()+
				" superclass: "+es2.getClass().getSuperclass()+
				" name: "+es2.getName() +
				" surname: " + es2.getSurname()+
				" tel: "+es2.getTel());

	}



}
