package inners;

public class Subject2 {

	private String name;
	private String surname;
	private String tel;

	public Subject2() {
		name = "***";
		surname = "***";
		tel = "***";
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

}
