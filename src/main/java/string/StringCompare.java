package string;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;



public class StringCompare {

	public static void main(String[] args) {
		final List<String> strings = Arrays.asList(new String[] { null, "bla", "ble", "" });
		
		System.out.println("nullIsLESS | nullIsMORE");
		for(String e1 : strings) {
			for(String e2 : strings) {
				System.out.println(String.format("'%s' == `%s`: %s | %s", e1, e2, StringUtils.compare(e1, e2, true), StringUtils.compare(e1, e2, false)));
			}
			
		}
	}

}
