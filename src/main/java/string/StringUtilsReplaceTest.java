package string;

import org.apache.commons.lang3.StringUtils;

public class StringUtilsReplaceTest {

	public static void main(String[] args) {
		String s = "El ? perro ? de ? parra ? no tiene ? rabo";
		String[] vals = {"1", "2", "3", "4", "5"};
//		System.out.println(StringUtils.replaceEachRepeatedly(s, new String[]{"?"}, vals));
//		System.out.println(StringUtils.replaceEach(s, new String[]{"?"}, vals));
		System.out.println(String.format(StringUtils.replace(s, "?", "%s"), (Object[])vals));

		System.out.println("**************************************");
		String[] f = {"15/03/2015", "15.03.2015", "15-03-2015"};
		String[] searchList = {".", "-"};
		String[] replacementList = {"/", "/"};
		for(int i=0; i<f.length; i++){
			System.out.println(f[i] + " : " +StringUtils.replaceEach(f[i], searchList, replacementList));
		}




	}

}
