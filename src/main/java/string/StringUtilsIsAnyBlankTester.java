/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package string;

import static org.apache.commons.collections4.CollectionUtils.size;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

public class StringUtilsIsAnyBlankTester {

	/**
	 * @param args
	 */
	@SuppressWarnings("boxing")
	public static void main(String[] args) {
		List<String> l = null;

		System.out.println(String.format("{%s} [%d] isNoneBlank: %s", StringUtils.join(l, ", "), size(l),
				StringUtils.isNoneBlank(CollectionUtils.emptyIfNull(l).toArray(new String[0]))));

		l = new ArrayList<>();
		System.out.println(String.format("{%s} [%d] isNoneBlank: %s", StringUtils.join(l, ", "), size(l),
				StringUtils.isNoneBlank(l.toArray(new String[0]))));

		l.add("blah");
		System.out.println(String.format("{%s} [%d] isNoneBlank: %s", StringUtils.join(l, ", "), size(l),
				StringUtils.isNoneBlank(l.toArray(new String[0]))));

		l.add(null);
		System.out.println(String.format("{%s} [%d] isNoneBlank: %s", StringUtils.join(l, ", "), size(l),
				StringUtils.isNoneBlank(l.toArray(new String[0]))));

		l.remove(null);
		l.add(" ");
		System.out.println(String.format("{%s} [%d] isNoneBlank: %s", StringUtils.join(l, ", "), size(l),
				StringUtils.isNoneBlank(l.toArray(new String[0]))));

	}

}
