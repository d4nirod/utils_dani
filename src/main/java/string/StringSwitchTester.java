/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package string;

import org.apache.commons.lang3.StringUtils;

public class StringSwitchTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s = "blah";
		switchString(null);

	}

	/**
	 * @param s
	 */
	private static void switchString(String s) {
		switch (StringUtils.defaultString(s)) {
			case "asdfasd":
				System.out.println("String is asdfasd");
				break;
			case "blah":
				System.out.println("String is blah");
				break;
			default:
				System.out.println("String is unknown");
				break;
		}
		
	}

}
