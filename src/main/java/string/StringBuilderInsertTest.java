package string;

public class StringBuilderInsertTest {

	public static void main(String[] args) {
		String s = "filename.my.txt";
		StringBuilder sb = new StringBuilder(s);
		sb.insert(s.lastIndexOf('.'), "_12345");
		System.out.println("Original string: "+s);
		System.out.println("New string: "+sb);
	}

}
