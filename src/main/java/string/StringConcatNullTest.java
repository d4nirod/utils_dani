package string;

public class StringConcatNullTest {
	public static void main(String[] args) {
		String s1 = "bla";
		String s2 = null;
		String s3 = " ";
		String concat1 = s1 + s2 + s3 + s1;
		String concat2 = s1 + s2 + s1;
		System.out.println(concat1);
		System.out.println(concat2);
	}


}
