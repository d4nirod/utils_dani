package string;

import org.apache.commons.lang3.StringEscapeUtils;

public class StringEscaperTest {

	public static void main(String[] args) {
//	    static {
//	        specialCharactersRepresentation['&'] = "&amp;".toCharArray();
//	        specialCharactersRepresentation['<'] = "&lt;".toCharArray();
//	        specialCharactersRepresentation['>'] = "&gt;".toCharArray();
//	        specialCharactersRepresentation['"'] = "&#034;".toCharArray();
//	        specialCharactersRepresentation['\''] = "&#039;".toCharArray();
//	    }
		String src = "&lt;mytag id=&#034;666&#034; value=&#034;ampersandtilde&amp;&#039;&#034; /&gt;";
		String src2 = "<mytag id=\"666\" value=\"ampersandtilde&'\" />";
		System.out.println("Source: "+src);
		String unescaped = StringEscapeUtils.unescapeXml(src);
		System.out.println("Unescaped: "+unescaped);
		String reescaped = StringEscapeUtils.escapeXml(unescaped);
		System.out.println("Reescaped: "+reescaped);
		String unreescaped = StringEscapeUtils.unescapeXml(reescaped);
		System.out.println("Unescaped: "+unreescaped);

		System.out.println("["+src + "] is escaped? "+!src.equals(StringEscapeUtils.unescapeXml(src)));
		System.out.println("["+src2 + "] is escaped? "+!src2.equals(StringEscapeUtils.unescapeXml(src2)));

		System.out.println("'*' escaped: '"+StringEscapeUtils.escapeXml("*")+"'");


	}

}
