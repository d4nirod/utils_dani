package string;


import javax.swing.*;

public class StringBuilderTest {

	public static void main(String[] args) {
		String input; // Used for the input string.
		String reversed; // Reversed form or the input string.

		while (true) {
			input = JOptionPane.showInputDialog(null, "Enter a string");
			if (input == null)
				break;
			reversed = "";
			for (int i = 0; i < input.length(); i++) {
				reversed = input.substring(i, i + 1) + reversed;
			}
			JOptionPane.showMessageDialog(null, "Reversed:\n" + reversed);
		}
	}
}
