/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package string;

import org.apache.commons.lang3.StringUtils;

public class StringUtilsJoinWithTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String prefix = "prefix";
		String main ="main";
		String suffix = "suffix";
		System.out.println(StringUtils.joinWith(".", prefix, main, suffix));
		System.out.println(StringUtils.joinWith(".", null, main, suffix));
		System.out.println(StringUtils.joinWith(".", "", main, suffix));
		System.out.println(StringUtils.joinWith(".", prefix, main, null));
		System.out.println(StringUtils.joinWith(".", prefix, main, ""));
	}

}
