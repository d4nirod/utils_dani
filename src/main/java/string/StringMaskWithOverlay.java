package string;

import org.apache.commons.lang3.StringUtils;

public class StringMaskWithOverlay {

	public static void main(String[] args) {
		String ccNumber = "123232323767";
		String masked =
		StringUtils.overlay(ccNumber,
				StringUtils.repeat("X", ccNumber.length()-4), 0, ccNumber.length()-4);
		System.out.println(ccNumber);
		System.out.println(masked);

	}

}
