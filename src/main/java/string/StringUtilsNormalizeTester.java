/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package string;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class StringUtilsNormalizeTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> list = Arrays.asList(
				"El  perro de\t \tParra   no tiene rabo  ",
				"La ruta 66 de &&   $ dolió añejo"
				);
		for (String s : list) {
			String stripAccents = StringUtils.stripAccents(s);
			String removeNonAlpha = StringUtils.replaceAll(stripAccents, "[^A-Za-z0-9\\s]", "");
			String normalized = StringUtils.normalizeSpace(removeNonAlpha);
			String replacedSpc = StringUtils.replace(normalized, " ", "_");
			System.out.println(String.format("'%s' :\n'%s'\n'%s'\n'%s'\n'%s'\n", s, stripAccents, removeNonAlpha, normalized, replacedSpc));
		}
	}

}
