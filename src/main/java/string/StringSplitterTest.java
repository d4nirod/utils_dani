package string;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class StringSplitterTest {


		  public static void main(String[] args) {
		    String testString = "010000000000000";
		    String[] res = testString.split("(?<=\\G.{3})");		    
		    for(int i=0; i<res.length; i++)
		    	 System.out.println(res[i]);

		    // output : [Real, How, To]
		    System.out.println("-------------------------");
		    ArrayList<String> list = new ArrayList<>(Arrays.asList(res));
		    list.removeAll(Collections.singleton("000"));
	
		    for(String it:list)
		    	System.out.println(it);
		    }

	
}
