package string;


public class StringFormatter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// %[argument_index$][flags][width]conversion
		int i = 10;
		Long lng = new Long("070860000000002");
		char c = '1';
		String s = "adfasdfasdfasdfasd";
		String result = String.format(" long: [%1$016d] \n char: [%2$c] \n string: [%3$s] \n int: [%4$04d]", lng, c, s, i);
		System.out.println(result);
		
		System.out.println(String.format("%04d", i));

	}

}
