package string;
import org.apache.commons.lang3.StringUtils;


public class StringUtilsTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String larga = "qwertyuiopasdfghjkl*";
		String corta = "abc";
		String nums = "0123456789";
		int i=1;
		System.out.println(i++ +")"+StringUtils.substring(larga, 0, 6));
		System.out.println(i++ +")"+StringUtils.substring(corta, 0, 6));
		
		System.out.println(i++ +")"+StringUtils.substring(corta, -2, -1));
		System.out.println(i++ +")"+StringUtils.substring(corta, -4, 2));

		System.out.println(i++ +")"+StringUtils.substring(corta, -4, corta.length()));
		System.out.println(i++ +")"+StringUtils.substring(corta, -5, corta.length()));
		
		System.out.println(i++ +")"+StringUtils.substring(nums, -3, nums.length()));
		System.out.println(i++ +")"+StringUtils.substring(nums, 0, -3));
		System.out.println(i++ +")"+StringUtils.right(nums, 3));
		System.out.println(i++ +")"+StringUtils.removeEnd(nums, StringUtils.right(nums, 3)));
		
		String msg1 = "el perro de parra no tiene rabo";
		String msg2 = "el perro de sistema no disponibleparra no tiene rabo";
		String srch = "SISTEMA NO DISPONIBLE";
		System.out.println(msg1 + "] contains ["+srch+": " + StringUtils.containsIgnoreCase(msg1, srch));
		System.out.println(msg2 + "] contains ["+srch+": " + StringUtils.containsIgnoreCase(msg2, srch)); 
	}

}
