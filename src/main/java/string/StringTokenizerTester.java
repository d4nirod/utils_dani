/*
 * 
 * Copyright Farmers Wife S.L. All rights reserved / Todos los derechos reservados
 * 
 */

package string;

import java.util.StringTokenizer;

public class StringTokenizerTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String str = "cueza, tajada, curda, chispa, berza, cogorza, pedal, melopea, cebolla, chuza, melocotón, moña";
		StringTokenizer tokenizer = new StringTokenizer(str, ",");
		while (tokenizer.hasMoreElements()) {
			System.out.println("'" + tokenizer.nextToken().trim() + "'");
		}
		System.out.println("===============");
		str = "";
		tokenizer = new StringTokenizer(str, ",");
		while (tokenizer.hasMoreElements()) {
			System.out.println("'" + tokenizer.nextToken().trim() + "'");
		}
	}

}
