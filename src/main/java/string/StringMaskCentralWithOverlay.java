package string;

import org.apache.commons.lang3.StringUtils;

public class StringMaskCentralWithOverlay {

	public static void main(String[] args) {
		String ccNumbers[] = new String[] {"1", "12", "123", "1234", "123232323767", "687415460", "34971455889"};
		for(int i=0; i<ccNumbers.length; i++){
			int len = ccNumbers[i].length();
			int n = (int) (len * 0.5);;
			int end = (int) (len - (n * 0.5));
			int start = end - n;
			String masked = StringUtils.overlay(ccNumbers[i],
								StringUtils.repeat("X", n),
								start,
								end);

			System.out.print(ccNumbers[i]+ " : ");
			System.out.println(masked);
		}

	}

}
