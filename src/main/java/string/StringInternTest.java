package string;

public class StringInternTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String a = "foo"; 
		String b = "food".substring(0,3); 
		String c = b.intern(); 
		System.out.println(a == c); //true

	}

}
