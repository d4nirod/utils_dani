package string;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;


public class StringReplaceChars {

	public static void main(String[] args) {
		List<String> strings = Arrays.asList(new String[]{" ES76 2077 0024 0031 0257 5766", "ES76-2077-0024-0031-0257-5766 ", " ES76.2077.0024.0031.0257.5766 "});
		String[] searchList = {" ", "-", "."};
		String[] replacementList = {"", "", ""};
		
		for(String s:strings){
			System.out.printf("[%s] = [%s] \t", s, StringUtils.replaceEach(s, searchList, replacementList));
			System.out.printf("[%s] = [%s]\n", s, s.replaceAll("[ \\.-]", ""));
		}
		

	}

}
