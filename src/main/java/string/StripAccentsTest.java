package string;

import java.text.Normalizer;

import org.apache.commons.lang3.StringUtils;

public class StripAccentsTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String[] list = {"*l estuvo all*", "Esto es una e*e min*scula y * may*scula", 
							"*****", "*****", "*****"};
		
		for(int i=0; i<list.length; i++){
			System.out.printf("Orig: '%s' Strip: '%s'\n", 
								list[i], 
								StringUtils.stripAccents(list[i]));
//					Normalizer.normalize(list[i], Normalizer.Form.NFKC)

		}
	}

}
