/**
 * 
 */
package string;

/**
 * @author 8984
 *
 */
public class StringAsArgument {

	/**
	 * Test passing String as argument
	 * @param args
	 */
	public static void main(String[] args) {
		String str = "THE STRING";
		System.out.println("1: "+str);
		change(str);
		System.out.println("2: "+str);
		Integer integ = new Integer(1);
		System.out.println("1: "+integ.toString());
		change(integ);
		System.out.println("2: "+integ.toString());
	}
	
	public static void change(String s){
		s = "new value";
	}
	public static void change(Integer i){	
		i = new Integer(111);
	}

	
}