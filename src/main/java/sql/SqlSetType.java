package sql;

public enum SqlSetType {ARRAY,						
						BIGDECIMAL,
						BINARYSTREAM,
						BOOLEAN,
						BYTE,
						BYTES,
						CHARACTERSTREAM,
						DATE,
						DOUBLE,
						FLOAT,
						INT,
						LONG,
						OBJECT,
						SHORT,
						STRING,
						TIME,
						TIMESTAMP,
						URL
};