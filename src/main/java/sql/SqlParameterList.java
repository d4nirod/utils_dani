package sql;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class SqlParameterList {

	
	@SuppressWarnings("rawtypes")
	private List<ImmutablePair> list;

	@SuppressWarnings("rawtypes")
	public SqlParameterList() {
		list = new ArrayList<ImmutablePair>();
	}


	@SuppressWarnings("rawtypes")
	public SqlParameterList(int initialCapacity) {
		list = new ArrayList<ImmutablePair>(initialCapacity);
	}
	
	
	public boolean add(Object obj, SqlSetType i){
		return list.add(ImmutablePair.of(obj, i));
		
	}
	
	@SuppressWarnings("rawtypes")
	public Pair get(int index){
		return list.get(index);
	}
	
	public Object getValue(int index){
		return list.get(index).getLeft();
	}
	
	public SqlSetType getSqlType(int index){
		return (SqlSetType) list.get(index).getRight();
	}
	
	public int size(){
		return list.size();
	}

}
