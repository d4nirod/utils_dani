package sql;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

public class SqlUtils {
	
	public static void setStatementParams(PreparedStatement stmt, List<Object> parametrs) throws SQLException {
		for (int i = 0; i < parametrs.size(); i++) {
			Object p = parametrs.get(i);			
			if(p instanceof Boolean)
				stmt.setBoolean(i+1, (Boolean)p);
			if(p instanceof Integer)
				stmt.setInt(i+1, (Integer)p);
			if(p instanceof Long)
				stmt.setLong(i+1, (Integer)p);
			if(p instanceof Float)
				stmt.setFloat(i+1, (Float)p);
			if(p instanceof Double)
				stmt.setDouble(i+1, (Double)p);
			if(p instanceof BigDecimal)
				stmt.setBigDecimal(i+1, (BigDecimal)p);
			if(p instanceof String)
				stmt.setString(i+1, (String)p);
			if(p instanceof byte[])
				stmt.setBytes(i+1, (byte[])p);
			if(p instanceof Date)
				stmt.setDate(i+1, (Date)p);
			if(p instanceof Time)
				stmt.setTime(i+1, (Time)p);
			if(p instanceof Timestamp)
				stmt.setTimestamp(i+1, (Timestamp)p);
			if(p instanceof Blob)
				stmt.setBlob(i+1, (Blob)p);
			
			
		}
		
	}

}
