package sql;

import java.io.ByteArrayInputStream;
import java.util.Calendar;

public class SqlUtilsTester {

	public SqlUtilsTester() {
	}

	public static void main(String[] args) {
		SqlParameterList l = new SqlParameterList();
		l.add(99, SqlSetType.INT);
		l.add("pepito", SqlSetType.STRING);
		l.add(Calendar.getInstance().getTime(), SqlSetType.DATE);
		l.add(new ByteArrayInputStream(new byte[]{0, 1}), SqlSetType.CHARACTERSTREAM);

		for(int i=0; i<l.size(); i++){
			System.out.println(l.getValue(i) + " : " + l.getSqlType(i).toString());
			System.out.println(l.get(i));
		}
	}

}
