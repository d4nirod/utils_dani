<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "org.apache.commons.lang3.StringUtils" %>
<%@ page import = "java.security.Provider" %>
<%@ page import = "java.security.Security" %>
<%@ page import = "java.util.Calendar" %>
<%@ page import = "java.text.SimpleDateFormat" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>JCE PROVIDER TEST</title>
</head>
<body>
	<h1>JCE PROVIDER TEST - <%=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()) %></h1>
	<hr>
	<h2>List of JCE Providers ********</h2>
	<table width="100%">			
	<%	
		try {
			Provider p[] = Security.getProviders();
			for (int i = 0; i < p.length; i++) {
				out.println("<tr><td>"+p[i]+"</td></tr>");
	//			for (Enumeration e = p[i].keys(); e.hasMoreElements();)
	//				System.out.println("\t" + e.nextElement());
			}
		} catch (Exception e) {
			out.println(e);
		}
	%>
	</table>	
</body>
</html>