<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "org.apache.commons.lang3.StringUtils" %>
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.util.Enumeration" %>
<%@ page import = "java.util.Calendar" %>
<%@ page import = "java.text.SimpleDateFormat" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>LIST SYSTEM PROPERTIE</title>
</head>
<body>
	<h1>LIST SYSTEM PROPERTIES - <%=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()) %></h1>
	<hr>
	<h2>List of System Propeties</h2>
	<table width="100%" border="1">			
	<%	
		Properties p = System.getProperties();
		Enumeration keys = p.keys();
		while (keys.hasMoreElements()) {
		  String key = (String)keys.nextElement();
		  String value = (String)p.get(key);
		  out.println("<tr><td>"+key+"</td><td>"+value+"<td></tr>");
		}
	%>
	</table>	
</body>
</html>