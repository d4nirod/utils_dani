<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="org.apache.commons.lang3.math.NumberUtils" %>

<%
//servicio simple de echo para pruebas ajax, espera dos parametros html: mensaje a devolver y delay: tiempo de espera
//falta implementar lo mismo para json
String html =  request.getParameter("html");
int delay = NumberUtils.toInt(request.getParameter("delay"));
if(StringUtils.isNotBlank(html)){
	Thread.sleep(delay*1000);
	response.getWriter().write(html);
}else {
	response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	response.setCharacterEncoding("UTF-8");
}
return;
%>