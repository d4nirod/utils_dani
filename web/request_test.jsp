<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ page import = "java.text.DateFormat" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Calendar" %>
<%@ page import = "java.util.Enumeration" %>
<%@ page import = "org.apache.http.client.utils.URIBuilder" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1><%=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()) %></h1>
<br><br>
<table>
	<tr><td>request.getQueryString()  </td><td><% out.println(request.getQueryString()); %></td><tr>
	<tr><td>request.getRequestURI()   </td><td><% out.println(request.getRequestURI()); %></td><tr>
	<tr><td>request.getRequestURL()   </td><td><% out.println(request.getRequestURL()); %></td><tr>
	<tr><td>request.getContextPath()  </td><td><% out.println(request.getContextPath()); %></td><tr>
	<tr><td>request.getServletPath()  </td><td><% out.println(request.getServletPath()); %></td><tr>
	<tr><td>request.getPathInfo()     </td><td><% out.println(request.getPathInfo()); %></td><tr>
	<tr><td>request.getPathTranslated()</td><td><% out.println(request.getPathTranslated()); %></td><tr>
	<tr>
		<td>Absolute request reconstructed</td>
		<% String absUrl = request.getScheme()+"://"+
				request.getServerName()+":"+
				request.getServerPort()+
				request.getContextPath()+
				request.getServletPath()+"?"+
				request.getQueryString();
		%>
		<td><a href="<%=absUrl %>"><%=absUrl%></a></td>
	<tr>
	<tr>
		<td>Relative request reconstructed</td>
		<% String relUrl = request.getContextPath()+request.getServletPath()+"?"+request.getQueryString();%>
		<td><a href="<%=relUrl %>"><%=relUrl%></a></td>
	<tr>
	<tr>
		<td>Relative request URIBuilder</td>
		<% URIBuilder builder = new URIBuilder(request.getContextPath()+request.getServletPath());
			Enumeration params = request.getParameterNames();
			while(params.hasMoreElements()){
				String paramName = (String)params.nextElement();
				builder.addParameter(paramName, request.getParameter(paramName));
			}
		%>
		<td><a href="<%=builder.toString() %>"><%=builder.toString()%></a></td>
	<tr>
</table>
</body>
</html>