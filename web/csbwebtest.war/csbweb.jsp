<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="java.util.*,
    		java.io.*,
    		org.apache.commons.lang3.StringUtils,
    		es.bancamarch.appserverbancaonline.common.dto.gestionarchivos.envio.*, 
    		es.bancamarch.csbweb.api.*,
    		java.text.SimpleDateFormat,
    		java.text.DateFormat,
    		javax.servlet.http.HttpServletResponse"
%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%!
// 	private void obtenerFichero(int id, char tipo,  HttpServletResponse response) {
	private void obtenerFichero(int id, String tipo,  HttpServletResponse response) {
		DateFormat df = new SimpleDateFormat("yyyyMMdd_HH-mm-ss-SSS");	
// 		byte [] fileData = CuadernosUtil.generarCuaderno(id, convertir(tipo));
		byte [] fileData = CuadernosUtil.generarCuaderno(id, tipo);
		response.setContentType("text/xml");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"dl_test_%s.xml\"", df.format(Calendar.getInstance().getTime())));
		BufferedOutputStream  out1 = null;
		try {
			out1 = new BufferedOutputStream(response.getOutputStream());						
			out1.write(fileData);
			return;
			
		
		} catch(Exception e){
			System.out.println("Error: "+e);
		} finally {
			if (out1!=null) try{ out1.close();}catch(Exception e2){}							
		}
	}
	
	private String convertir(char tipoDoc) {
		switch (tipoDoc) {
			case TipoDocumento.TRANSFERENCIA_SEPA:  return ApiConstants.CUADERNO_TRANSFERENCIAS_SEPA;
			case TipoDocumento.DEBITO_DIRECTO_SEPA: return ApiConstants.CUADERNO_RECIBOS_SEPA;			
			default: System.out.println(String.format("Imposible traducir de tipo documento '%c'", tipoDoc));
			return null;
		}
	}
%>
<%	
	if(StringUtils.isNotBlank(request.getParameter("archivoId")) && StringUtils.isNotBlank(request.getParameter("tipoDoc")) ){
// 		obtenerFichero( Integer.parseInt(request.getParameter("archivoId")), request.getParameter("tipoDoc").charAt(0), response);
		obtenerFichero( Integer.parseInt(request.getParameter("archivoId")), request.getParameter("tipoDoc"), response);
		return;
		
	}else if(StringUtils.isNotBlank(request.getParameter("tipoDoc")) && StringUtils.isNotBlank(request.getParameter("ccs")) ){
// 		List<Cuaderno> lista = CuadernosUtil.consultarCuadernosPendientesContrato(request.getParameter("ccs"), convertir(request.getParameter("tipoDoc").charAt(0)));
		List<Cuaderno> lista = CuadernosUtil.consultarCuadernosPendientesContrato(request.getParameter("ccs"), request.getParameter("tipoDoc"));
		pageContext.setAttribute("lista", lista);
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>CSBWeb File Download Test</title>
</head>
<body>
	<h1>CSBWeb File Download Test</h1>
	<c:if test="${not empty lista}">
		<h2>Seleccione Archivo</h2>
		<form action="csbweb.jsp" method="get">
			<table>
				<thead>
					<tr>
						<th></th><th>idCuaderno</th><th>tipoCuaderno</th><th>descripcion</th><th>importe</th><th>fecha</th><th>ordenante</th><th>oficina</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${lista}" var="archivo">
						<tr>
							<td>
								<input type="radio" class="radio" name="archivoId" value="<c:out value='${archivo.idCuaderno}'/>" />
							</td>
						</tr>							
						<tr>
							<td><c:out value="${archivo.idCuaderno }"></c:out></td>
						</tr>							
						<tr>
							<td><c:out value="${archivo.tipoCuaderno }"></c:out></td>
						</tr>							
						<tr>
							<td><c:out value="${archivo.descripcion }"></c:out></td>
						</tr>							
						<tr>
							<td><fmt:formatNumber value="${archivo.importe}" pattern="#,##0.00"/></td>
						</tr>							
						<tr>
							<td><fmt:formatDate  value="${archivo.fecha}" pattern="dd/MM/yyyy" /></td>
						</tr>							
						<tr>
							<td><c:out value="${archivo.ordenante }"></c:out></td>
						</tr>							
						<tr>
							<td><c:out value="${archivo.oficina }"></c:out></td>
						</tr>																
					</c:forEach>								
				</tbody>
			</table>
			<input type="submit" name="submit" value="submit" />		
		</form>								
	</c:if>
	<br></br>
	<h2>Seleccione tipo de archivo y clave contrato SIP</h2>
	<form action="csbweb.jsp" method="get">
		<table>
			<thead>
				<tr>
					<td>Tipo documento</td><td>CCS</td><td>&nbsp;</td>
				</tr>
			</thead>
			<tr>
				<td>
					<select name="tipoDoc">					
						<option>debitos</option>
						<option>transferenciaSepa</option>
					</select>
				</td>
				<td><input type="text" name="ccs" value="3819"/></td>
				<td><input type="submit" name="submit" value="submit"/></td>
			</tr>		
		</table>		
	</form>
</body>
</html>