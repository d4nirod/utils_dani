<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.Map" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
Map<String, String[]> parameters = request.getParameterMap();
for(String parameter : parameters.keySet()) {
	String[] values = parameters.get(parameter);
	for(int i=0; i<values.length; i++)
		System.out.println(parameter+": "+values[i]);

}
%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dump parameters</title>
</head>
<body>
	<c:forEach var="par" items="${paramValues}">
		${par.key} =
		<c:forEach var="val" items="${par.value}">
			[${val}]
		</c:forEach>
		<br>
	</c:forEach>
</body>
</html>
