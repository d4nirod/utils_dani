<%@ page trimDirectiveWhitespaces="true" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*,javax.naming.*,javax.sql.DataSource,java.sql.*"%>
<%@ page import = "java.util.*" %>
<%@ page import = "org.apache.commons.lang3.StringUtils" %>
<%@ page import = "org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import = "org.apache.commons.io.IOUtils" %>
<%@ page import = "java.io.StringReader" %>
<%@ page import = "java.io.Reader" %>
<%@ page import = "java.io.BufferedWriter" %>
<%@ page import = "java.io.OutputStreamWriter" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.io.ByteArrayInputStream" %>
<%@ page import = "java.io.InputStreamReader" %>
<%@ page import = "java.io.BufferedReader" %>
<%@ page import = "java.text.DateFormat" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>

	<%
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		DateFormat dff = new SimpleDateFormat("yyyyMMdd_HH-mm-ss-SSS"); 
		DataSource ds = null;
		Connection con = null;
		PreparedStatement pr = null;
		ResultSet rs = null;
		InitialContext ic;
		try {
			ic = new InitialContext();
//			ds = (DataSource) ic.lookup("java:/jdbc/SepaXmlDS");
			ds = (DataSource) ic.lookup("java:comp/env/jdbc/SepaXmlDS");
			con = ds.getConnection();			
			long id = Long.parseLong(request.getParameter("id"));
			String method  = request.getParameter("readMethod");
			
			String selectQuery = "SELECT * FROM FLRFIC WHERE ID=?";
			pr = con.prepareStatement(selectQuery);
			pr.setMaxRows(1);		
			pr.setLong(1, id);
			rs = pr.executeQuery();
			if(rs.next()) {				
				response.setContentType("text/xml");
				response.setHeader("Content-Disposition", String.format("attachment; filename=\"dl_test_%s.xml\"", dff.format(Calendar.getInstance().getTime())));
				
				System.out.println(String.format("Method: %s: before reading ResultSet:  %s", method, df.format(Calendar.getInstance().getTime())));		
				if("String".equalsIgnoreCase(method)){
					String datos = rs.getString(9);
					System.out.println(String.format("Method: %s: after reading ResultSet:  %s", method, df.format(Calendar.getInstance().getTime())));
					
					BufferedWriter out1 = null;
					try {
						out1 = new BufferedWriter(new OutputStreamWriter(response.getOutputStream(),"UTF-8"));						
						out1.write(datos);
						return;
					} finally {
						if (out1!=null) out1.close();
						System.out.println(String.format("Method: %s: after writing to ouput:  %s", method, df.format(Calendar.getInstance().getTime())));						
					}
					
					
				}else if("Reader".equalsIgnoreCase(method)){
					Reader reader  = rs.getCharacterStream(9);
					System.out.println(String.format("Method: %s: after reading ResultSet:  %s", method, df.format(Calendar.getInstance().getTime())));
					BufferedWriter out2 = null;
					try {
						out2 = new BufferedWriter(new OutputStreamWriter(response.getOutputStream(),"UTF-8"));						
						IOUtils.copy(reader, out2);
						return;
					} finally {
						if (out2!=null) out2.close();
						System.out.println(String.format("Method: %s: after writing to ouput:  %s", method, df.format(Calendar.getInstance().getTime())));						
					}					
				}
			}			
		} catch (Exception e) {
			out.println("Exception thrown " + e);			
			e.printStackTrace();
		} finally {
			try { if (rs!=null ) rs.close();  } catch(Exception e) {};
			try { if (pr!=null ) pr.close();  } catch(Exception e) {};
			try { if (con!=null) con.close(); } catch(Exception e) {};
			ds = null;
		}	
%>	