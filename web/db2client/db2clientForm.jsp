<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*,javax.naming.*,javax.sql.DataSource,java.sql.*"%>
<%@ page import = "java.util.*" %>
<%@ page import = "es.bancamarch.util.ReceptorArchivo" %>
<%@ page import = "org.apache.commons.lang3.StringUtils" %>
<%@ page import = "java.io.StringReader" %>
<%@ page import = "java.text.DateFormat" %>
<%@ page import = "java.text.SimpleDateFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>DB2 CLIENT UPLOAD TEST</title>
</head>
<body>
	<h1>DB2 CLIENT UPLOAD TEST - step 2 - <%=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()) %></h1>
	<hr>
	<h2>File details</h2>
	<%
		try {
			ReceptorArchivo rcptr = new ReceptorArchivo();
			long bytesRead = rcptr.procesaArchivo(request);
			String today = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
			session.setAttribute("datos", rcptr.getFileData());
	%>
	<table border="1">
		<tr>
			<td>File name: </td><td><%=rcptr.getFileName() %></td>
		</tr>
		<tr>
			<td>Full file name: </td><td><%=rcptr.getFullFileName() %></td>
		</tr>
		<tr>
			<td>Length in bytes: </td><td><%=bytesRead %></td>
		</tr>
		<tr>
			<td>Request encoding: </td><td><%=request.getCharacterEncoding() %></td>
		</tr>
	</table>
	<br><br>
	<h2>DB record fields for Tomcat 6</h2>
	<form method="post" action="db2clientFin6.jsp">
		<table border="1">
			<tr>
				<td>FECHA_CARGA</td><td style='text-align: right;'><input type="text" name="FECHA_CARGA" value="<%=today %>" maxlength="9" /></td>
			</tr>
			<tr>
				<td>I_ORIGEN_CARGA</td><td style='text-align: right;'><input type="text" name="I_ORIGEN_CARGA" value="3" maxlength="1"/></td>
			</tr>
			<tr>
				<td>USUARIO_BT_CARGA</td><td style='text-align: right;'><input type="text" name="USUARIO_BT_CARGA" value="3819" maxlength="9" size="10"/></td>
			</tr>
			<tr>
				<td>EMPLEADO_CARGA</td><td style='text-align: right;'><input type="text" name="EMPLEADO_CARGA" value="0" maxlength="9" size="10"/></td>
			</tr>
			<tr>
				<td>TIPO_FICHERO</td><td style='text-align: right;'><input type="text" name="TIPO_FICHERO" value="1" maxlength="9" size="10"/></td>
			</tr>
			<tr>
				<td>NOMBRE_FICHERO</td><td style='text-align: right;'><input type="hidden" name="NOMBRE_FICHERO" value="<%=rcptr.getFileName() %>"/><%=rcptr.getFileName() %></td>
			</tr>
			<tr>
				<td>LONGITUD_BYTES</td><td style='text-align: right;'><input type="hidden" name="LONGITUD_BYTES" value="<%=bytesRead %>"/><%=bytesRead %></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center; padding: 9px 0 9px 0;"><input type="submit" name="insert" value="insert through tomcat 6"/></td>
			</tr>
		</table>
	</form>

	<br><br>
	<h2>DB record fields for Tomcat 7</h2>
	<form method="post" action="db2clientFin7.jsp">
		<table border="1">
			<tr>
				<td>FECHA_CARGA</td><td style='text-align: right;'><input type="text" name="FECHA_CARGA" value="<%=today %>" maxlength="9" /></td>
			</tr>
			<tr>
				<td>I_ORIGEN_CARGA</td><td style='text-align: right;'><input type="text" name="I_ORIGEN_CARGA" value="1" maxlength="1"/></td>
			</tr>
			<tr>
				<td>USUARIO_BT_CARGA</td><td style='text-align: right;'><input type="text" name="USUARIO_BT_CARGA" value="123456789" maxlength="9" size="10"/></td>
			</tr>
			<tr>
				<td>EMPLEADO_CARGA</td><td style='text-align: right;'><input type="text" name="EMPLEADO_CARGA" value="0" maxlength="9" size="10"/></td>
			</tr>
			<tr>
				<td>TIPO_FICHERO</td><td style='text-align: right;'><input type="text" name="TIPO_FICHERO" value="321" maxlength="9" size="10"/></td>
			</tr>
			<tr>
				<td>NOMBRE_FICHERO</td><td style='text-align: right;'><input type="hidden" name="NOMBRE_FICHERO" value="<%=rcptr.getFileName() %>"/><%=rcptr.getFileName() %></td>
			</tr>
			<tr>
				<td>LONGITUD_BYTES</td><td style='text-align: right;'><input type="hidden" name="LONGITUD_BYTES" value="<%=bytesRead %>"/><%=bytesRead %></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center; padding: 9px 0 9px 0;"><input type="submit" name="insert" value="insert through tomcat 6"/></td>
			</tr>
		</table>
	</form>
<%

		} catch (Exception e) {
			out.println("Exception thrown " + e);
		} finally {

		}
%>

</body>
</html>