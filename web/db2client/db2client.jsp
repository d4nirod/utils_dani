<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*,javax.naming.*,javax.sql.DataSource,java.sql.*"%>
<%@ page import = "org.apache.commons.lang3.StringUtils" %>
<%@ page import = "org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import = "java.text.DateFormat" %>
<%@ page import = "java.text.SimpleDateFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>DB2 CLIENT TEST</title>
</head>
<body>
	<h1>DB2 CLIENT TEST - step 1 - <%=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()) %></h1>
	<hr>
	<h2>DB2 Connection Details</h2>
	<%		
		DataSource ds = null;
		Connection con = null;
		PreparedStatement pr = null;
		ResultSet rs = null;
		InitialContext ic;	
		try {
			ic = new InitialContext();
//			ds = (DataSource) ic.lookup("java:/jdbc/SepaXmlDS");
			ds = (DataSource) ic.lookup("java:comp/env/jdbc/SepaXmlDS");
			con = ds.getConnection();
			DatabaseMetaData dbmd = con.getMetaData();  
	%>
	
	<h3>DB URL: <%=dbmd.getURL() %></h3>
	<h3>DB username: <%=dbmd.getUserName() %></h3>
	<table width="100%">
		<tr>
			<td>
			
<%			
			out.println("=====  Database info =====<br>");  
			out.println("DatabaseProductName: " + dbmd.getDatabaseProductName() +"<br>");  
			out.println("DatabaseProductVersion: " + dbmd.getDatabaseProductVersion() +"<br>");  
			out.println("DatabaseMajorVersion: " + dbmd.getDatabaseMajorVersion() +"<br>");  
			out.println("DatabaseMinorVersion: " + dbmd.getDatabaseMinorVersion() +"<br><br>");  
	%>
			</td>
			<td>					
	<% 
			out.println("=====  Driver info =====<br>");  
			out.println("DriverName: " + dbmd.getDriverName() +"<br>");  
			out.println("DriverVersion: " + dbmd.getDriverVersion() +"<br>");  
			out.println("DriverMajorVersion: " + dbmd.getDriverMajorVersion() +"<br>");  
			out.println("DriverMinorVersion: " + dbmd.getDriverMinorVersion() +"<br><br>");  
	%>
			</td>
			<td valign="top">
	<%
			out.println("=====  JDBC/DB attributes =====<br>");  
			out.println("JDBCDriverMajorVersion: " + dbmd.getJDBCMajorVersion() +"<br>");  
			out.println("JDBCDriverMinorVersion: " + dbmd.getJDBCMinorVersion() +"<br><br>");  
			out.print("Supports getGeneratedKeys(): " + dbmd.supportsGetGeneratedKeys());  			  
	%>
			</td>
		</tr>		
	</table>
	<%		
			//"SELECT current date FROM sysibm.sysdummy1;",
			//"SELECT * FROM SPRCON FETCH FIRST 10 ROWS ONLY;"
			//ID, FECHA_CARGA, I_ORIGEN_CARGA, USUARIO_CARGA, ESTADO, TIPO_FICHERO, NOMBRE_FICHERO, LONGITUD_BYTES, TIME_INSERT, TIME_UPDATE
			//"SELECT ID, FECHA_CARGA, I_ORIGEN_CARGA, USUARIO_CARGA, ESTADO, TIPO_FICHERO, NOMBRE_FICHERO, LONGITUD_BYTES, TIME_INSERT, TIME_UPDATE FROM GLSQL.FLRFIC ORDER BY ID DESC FETCH FIRST 10 ROWS ONLY;" 
			String [] stmts = {"SELECT * FROM FLRFIC ORDER BY ID DESC FETCH FIRST 10 ROWS ONLY;"	};
			for(int j=0; j<stmts.length; j++){
	%>
	<hr>
	<h2>DB2 Connection Query Test</h2>
	<div style="height:200px;overflow:auto;">
		<table border="1">
				<tr>			
		<%		
					pr = con.prepareStatement(stmts[j]);
					rs = pr.executeQuery();
					ResultSetMetaData md = rs.getMetaData();				
					for(int i=1; i<=md.getColumnCount(); i++){
						out.println("<th>"+md.getColumnName(i)+"</th>");
					}				
		%>
				</tr>			
		<%
					while (rs.next()) {
						out.println("<tr>");
						for(int i=1; i<=md.getColumnCount(); i++){					
							if(md.getColumnType(i) == Types.CLOB){					
								//String abrvClob = StringEscapeUtils.escapeXml(StringUtils.abbreviateMiddle(rs.getString(md.getColumnName(i)), ".", 120));
								out.println("<td style='text-align: right;'>[CLOB]</td>");							
							}else
								out.println("<td style='text-align: right;'> " + rs.getString(md.getColumnName(i))+"</td>");
						}
						out.println("</tr>");
					}
		
					for(int i=1; i<=md.getColumnCount(); i++){
						out.println("<th>"+md.getColumnTypeName(i)+"</th>");
						
					}
					rs.close();
					pr.close();
		%>
		</table>
	</div>
	<br>
	<%
			}
	%>
	<hr>
	<h2>File UPLOAD form</h2>	
	<form method="post" name="frm" id="frm" action="db2clientForm.jsp" enctype="multipart/form-data">
		<table border="1">
			<tr>
				<td>Upload file: </td><td><input type="file" name="filename" value="filenamevalue"/></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center; padding: 9px 0 9px 0;"><input type="submit" name="submit" value="submit"/></td>
			</tr>
		</table>		 		
	</form>
		<hr>
	<h2>File DONWLOAD form</h2>	
	<form method="post" name="frm" id="frm" action="db2clientDown.jsp">
		<table border="1">
			<tr>
				<td>Download file record id: </td><td><input type="text" name="id" value=""/></td>
			</tr>
			<tr>
				<td>Download read CLOB method: </td>
				<td>
					<select name="readMethod">
						<option>String</option>
						<option>Reader</option>						
					</select>					
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center; padding: 9px 0 9px 0;"><input type="submit" name="submit" value="submit"/></td>
			</tr>
		</table>		 		
	</form>
	
	<% 	
		} catch (Exception e) {
			out.println("Exception thrown " + e);
		} finally {
			try { if (rs!=null ) rs.close();  } catch(Exception e) {};
			try { if (pr!=null ) pr.close();  } catch(Exception e) {};
			try { if (con!=null) con.close(); } catch(Exception e) {};
			ds = null;			
		}	
	%>	
</body>
</html>