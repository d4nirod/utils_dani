<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*,javax.naming.*,javax.sql.DataSource,java.sql.*"%>
<%@ page import = "java.util.*" %>
<%@ page import = "org.apache.commons.lang3.StringUtils" %>
<%@ page import = "org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import = "java.io.StringReader" %>
<%@ page import = "java.io.Reader" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.io.ByteArrayInputStream" %>
<%@ page import = "java.io.InputStreamReader" %>
<%@ page import = "java.io.BufferedReader" %>
<%@ page import = "java.text.DateFormat" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>DB2 CLIENT UPLOAD TEST</title>
</head>
<body>
	<h1>DB2 CLIENT UPLOAD TEST - step 3 - <%=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime()) %></h1>
	<hr>
	<%
		DataSource ds = null;
		Connection con = null;
		PreparedStatement pr = null;
		ResultSet rs = null;
		InitialContext ic;
		InputStream is = null;
        BufferedReader bfReader = null;
		try {					
			ic = new InitialContext();
//			ds = (DataSource) ic.lookup("java:/jdbc/SepaXmlDS");
			ds = (DataSource) ic.lookup("java:comp/env/jdbc/SepaXmlDS");
			con = ds.getConnection();
			String insertQuery = "INSERT INTO FLRFIC (FECHA_CARGA, I_ORIGEN_CARGA, USUARIO_BT_CARGA, EMPLEADO_CARGA, TIPO_FICHERO, NOMBRE_FICHERO, LONGITUD_BYTES, DATOS) "
					+ "VALUES (?,?,?,?,?,?,?,?)";
// 			PreparedStatement stmt = con.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
			PreparedStatement stmt = con.prepareStatement(insertQuery, new int[]{1});
			String fechaCarga =   	request.getParameter("FECHA_CARGA");
			String iOrigenCarga = 	request.getParameter("I_ORIGEN_CARGA");
			String usuarioBtCarga = request.getParameter("USUARIO_BT_CARGA");
			String empleadoCarga = 	request.getParameter("EMPLEADO_CARGA");
			String tipoFichero = 	request.getParameter("TIPO_FICHERO");
			String nombreFichero = 	request.getParameter("NOMBRE_FICHERO");
			String longitudBytes = 	request.getParameter("LONGITUD_BYTES");
			byte[] datos = (byte[]) session.getAttribute("datos");
			
// 			String encoding = request.getParameter("encoding");
			int j=1;
			stmt.setInt(j++, Integer.parseInt(fechaCarga));
			stmt.setInt(j++, Integer.parseInt(iOrigenCarga));
			stmt.setInt(j++, Integer.parseInt(usuarioBtCarga));
			stmt.setInt(j++, Integer.parseInt(empleadoCarga));
			stmt.setInt(j++, Integer.parseInt(tipoFichero));
			stmt.setString(j++, nombreFichero);			
			stmt.setLong(j++, Long.parseLong(longitudBytes));
			

			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date a = Calendar.getInstance().getTime();
			System.out.println("\n");
			System.out.println("Before encoding to string: "+df.format(a));			
			String dataStr = new String(datos, "UTF-8");
			
			Date b = Calendar.getInstance().getTime();
			System.out.println("After  encoding to string: "+df.format(b));			
			System.out.printf("Difference: %d ms\n", (b.getTime()-a.getTime()) );
			stmt.setString(j++, dataStr);
				
			Date c = Calendar.getInstance().getTime();
			System.out.println("After  stmt.setString:     "+df.format(c));			
			System.out.printf("Difference: %d ms\n", (c.getTime()-b.getTime()) );
			
			int count = stmt.executeUpdate();			
			Date d = Calendar.getInstance().getTime();
			System.out.println("After  executing update:   "+df.format(d));
			System.out.printf("Difference: %d ms\n\n", (d.getTime()-c.getTime()) );
			
			int key = -1;				
			rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				key = rs.getInt(1);
			}			
	%>
	<h2>Insert results: </h2>
	<table border="1">
		<tr>
			<td>Rows inserted: </td><td style="text-align: right;"><%=count %></td>
		</tr>
		<tr>
			<td>Returned primary key: </td><td style="text-align: right;"><%=key %></td>
		</tr>
	</table>	
	<br><br>
<%
		try { rs.close(); }catch(Exception e) {}
		try { pr.close(); }catch(Exception e) {}
		String selectQuery = "SELECT * FROM FLRFIC WHERE ID="+key;
		pr = con.prepareStatement(selectQuery);
		pr.setMaxRows(1);		
		rs = pr.executeQuery();
		ResultSetMetaData md = rs.getMetaData();
%>	
	<h2>Inserted record</h2>
	<table border="1">
		<tr>	
<%
		for(int i=1; i<=md.getColumnCount(); i++){
			out.println("<th>"+md.getColumnName(i)+"</th>");			
		}
%>
		</tr>			
<%	
		if(rs.next()) {
			out.println("<tr>");
			for(int i=1; i<=md.getColumnCount(); i++){
				if(md.getColumnType(i) == Types.CLOB){					
					String abrvClob = StringEscapeUtils.escapeXml(StringUtils.abbreviateMiddle(rs.getString(md.getColumnName(i)), ".", 120));
					out.println("<td style='text-align: right;'>"+abrvClob+"</td>");							
				}else
					out.println("<td style='text-align: right;'> " + rs.getString(md.getColumnName(i))+"</td>");
					
			}
			out.println("</tr>");
		}
		
		for(int i=1; i<=md.getColumnCount(); i++){
			out.println("<th>"+md.getColumnTypeName(i)+"</th>");
			
		}
%>	
		</table>
<%
		} catch (Exception e) {
			out.println("Exception thrown " + e);			
			e.printStackTrace();
		} finally {
			try { if (is != null) is.close();  } catch(Exception e) {};
			try { if (rs!=null ) rs.close();  } catch(Exception e) {};
			try { if (pr!=null ) pr.close();  } catch(Exception e) {};
			try { if (con!=null) con.close(); } catch(Exception e) {};
			ds = null;			
		}	
%>	
</body>
</html>