package birt.pojo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CustomerDataSet {

	private List<Customer> customers;
	private Iterator<Customer> itr;
	
	public CustomerDataSet() {
		super();
		this.customers = new ArrayList<Customer>();
	}

	public List<Customer> getCustomers() {		
		return this.customers;
	}
	
	public void setCustomers(List<Customer> customers){
		this.customers = customers;
	}

	public void open(Object appContext, Map<String, Object> dataSetParamValues) {
		itr = getCustomers().iterator();
	}

	public Object next() {		
		if (itr.hasNext())
			return itr.next();
		else
			return null;
	}

	public void close() {
	}
}