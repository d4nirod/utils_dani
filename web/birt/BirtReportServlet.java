package birt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.birt.report.engine.api.HTMLRenderOption;
import org.eclipse.birt.report.engine.api.HTMLServerImageHandler;
import org.eclipse.birt.report.engine.api.IPDFRenderOption;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.eclipse.birt.report.engine.api.RenderOption;

import test.birt.pojo.Customer;
import test.birt.pojo.CustomerDataSet;

public class BirtReportServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private IReportEngine birtReportEngine = null;
	protected static Logger logger = LogManager.getLogger("org.eclipse.birt");

	public BirtReportServlet() {
		super();
	}

	/**
	 * Destruction of the servlet.
	 */
	public void destroy() {
		super.destroy();
		BirtEngine.destroyBirtEngine();
	}
	
	/**
	 * Genera un data set con datos para pruebas
	 */
	public CustomerDataSet buildCustomerDataSet(){
		CustomerDataSet dataset = new CustomerDataSet();
		
		Customer c = new Customer(103);
		c.setCity("Barcelona");
		c.setCountry("Spain");
		c.setCreditLimit(100);
		c.setName("Mortadelo");
		c.setState("Catalunya");
		dataset.getCustomers().add(c);

		c = new Customer(104);
		c.setCity("Madrid");
		c.setCountry("Spain");
		c.setCreditLimit(200);
		c.setName("Pepe Gotera");
		c.setState("Madrid");
		dataset.getCustomers().add(c);

		c = new Customer(105);
		c.setCity("Palma de Mallorca");
		c.setCountry("Spain");
		c.setCreditLimit(300);
		c.setName("Carpanta");
		c.setState("Baleares");
		dataset.getCustomers().add(c);
		return dataset;
	}

	/**
	 * The doGet method of the servlet.
	 * 
	 * 
	 */
	@SuppressWarnings("unchecked")
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		// get report name and launch the engine
		String reportName = req.getParameter("reportName");
		String type = req.getParameter("type");
		ServletContext sc = req.getSession().getServletContext();
		this.birtReportEngine = BirtEngine.getBirtEngine(sc);
		
		IReportRunnable design;
		try {
			// Open report design
			design = birtReportEngine.openReportDesign(sc.getRealPath("/test/birt/reports") + "/" + reportName);
			
			// set output options
			RenderOption options;
			
			if(RenderOption.OUTPUT_FORMAT_PDF.equalsIgnoreCase(type)){
				resp.setContentType( "application/pdf" );
				 resp.setHeader ("Content-Disposition","inline; filename=report.pdf");
				PDFRenderOption pdfOpt = new PDFRenderOption();
				pdfOpt.setOption(IPDFRenderOption.PAGE_OVERFLOW, IPDFRenderOption.FIT_TO_PAGE_SIZE);
				pdfOpt.setOption(IPDFRenderOption.PAGE_OVERFLOW, IPDFRenderOption.OUTPUT_TO_MULTIPLE_PAGES);
				options = pdfOpt;
				options.setOutputFormat(HTMLRenderOption.OUTPUT_FORMAT_PDF);
				
			}else {				
				resp.setContentType("text/html");
				HTMLRenderOption htmlOpt = new HTMLRenderOption();
//				String imgDir = sc.getRealPath("/home/img");
				String imgDir = sc.getContextPath()+"/home/img";
				
				htmlOpt.setImageDirectory(imgDir);
				logger.log(Level.FINE, "image directory " +imgDir);
				System.out.println("stdout image directory " +imgDir);
				htmlOpt.setBaseImageURL(imgDir);
				//Setting this to true removes html and body tags
//			htmlOpt.setEmbeddable(false);
				options = htmlOpt;
				options.setOutputFormat(HTMLRenderOption.OUTPUT_FORMAT_HTML);				
			}
			
			options.setImageHandler(new HTMLServerImageHandler());
			options.setOutputStream(resp.getOutputStream());
			
			// create task to run and render report
			IRunAndRenderTask task = birtReportEngine.createRunAndRenderTask(design);

//			task.setAppContext(contextMap);
			//Set parent classloader for engine   ---------------------> ESTO LO HACEMOS EN LA CLASE QUE INICIALIZA EL REPORT ENGINE			
//			task.getAppContext().put(EngineConstants.APPCONTEXT_CLASSLOADER_KEY, this.getClass().getClassLoader());
			
			task.getAppContext().put("APP_CONTEXT_KEY_POJODATASET", buildCustomerDataSet().getCustomers());

			task.setRenderOption(options);

			//run and render report
			task.run();
			task.close();     //*************************   METER EN FINALLY{
			
		} catch (Exception e) {

			e.printStackTrace();
			throw new ServletException(e);
		}
	}

	/**
	 * The doPost method of the servlet.
	 * 
	 * 
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.println(" Post does nothing");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

	/**
	 * Initialization of the servlet.
	 * 
	 * 
	 * @throws ServletException
	 *             if an error occure
	 */
	public void init() throws ServletException {
		BirtEngine.initBirtConfig();

	}

}