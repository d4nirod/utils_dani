#!/usr/bin/bash
/usr/bin/echo -n "rsnapshot HOURLY script started at " 
/usr/bin/date
/usr/bin/rsnapshot hourly
/usr/bin/chmod -R u=rwX,go=rX /cygdrive/u/backup/rsnapshot/hourly.*
/usr/bin/echo -n "rsnapshot HOURLY script completed at "
/usr/bin/date
/usr/bin/echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
