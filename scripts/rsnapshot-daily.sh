#!/usr/bin/bash
/usr/bin/echo -n "rsnapshot DAILY script started at " 
/usr/bin/date
/usr/bin/rsnapshot daily
/usr/bin/chmod -R u=rwX,go=rX /cygdrive/u/backup/rsnapshot/daily.*
/usr/bin/echo -n "rsnapshot DAILY script completed at "
/usr/bin/date
/usr/bin/echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
