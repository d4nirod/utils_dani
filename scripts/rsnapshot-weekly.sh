#!/usr/bin/bash
/usr/bin/echo -n "rsnapshot WEEKLY script started at " 
/usr/bin/date
/usr/bin/rsnapshot weekly
/usr/bin/chmod -R u=rwX,go=rX /cygdrive/u/backup/rsnapshot/weekly.*
/usr/bin/echo -n "rsnapshot WEEKLY script completed at "
/usr/bin/date
/usr/bin/echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
