package com.vogella.junit.first;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class RuleExceptionTesterExample {

	@Rule
	  public ExpectedException exception = ExpectedException.none();

	@Test
	public void throwsIllegalArgumentExceptionIfDivideByZero() {
		exception.expect(IllegalArgumentException.class);
		//This would fail as the string argument is not a substring of the exception message
//		exception.expectMessage("afdadsfasdf");
		exception.expectMessage("Divisor cannot be equal to 0");
		MyClass testSubject = new MyClass();
		testSubject.divide(10, 0);
	}

}
